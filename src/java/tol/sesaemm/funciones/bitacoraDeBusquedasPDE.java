package tol.sesaemm.funciones;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.integracion.DAOBase;
import tol.sesaemm.ing.jidv.javabeans.tbitacoraBusquedasPDE;
import com.mongodb.client.MongoCursor;
import org.bson.Document;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class bitacoraDeBusquedasPDE
{

    public static String spic = "SPIC";
    public static String ssancionados = "SS";
    public static String psancionados = "PS";

    /**
     * Metodo para registrar en base de datos una bitacora de las busquedas
     * realizadas en los sistemas de la PDE
     *
     * @param bitacora
     *
     * @return blnSeRegistro
     *
     * @throws java.lang.Exception
     */
    public static boolean registrarBitacoraDeBusquedasPDE(tbitacoraBusquedasPDE bitacora) throws Exception
    {

        MongoClient conectarBaseDatos = null;
        MongoDatabase database;
        MongoCollection<tbitacoraBusquedasPDE> collection;
        boolean blnSeRegistro = false;
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        try
        {

            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            {

                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tbitacora_busquedas_pde", tbitacoraBusquedasPDE.class);
                collection.insertOne(bitacora);
                blnSeRegistro = true;

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al registrar bitacora de busqueda de la PDE: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSeRegistro;

    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { 

        if (cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 

        if (conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } 

    }

}
