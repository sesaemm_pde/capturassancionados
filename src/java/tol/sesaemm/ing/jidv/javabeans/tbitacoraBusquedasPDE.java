package tol.sesaemm.ing.jidv.javabeans;

import java.util.ArrayList;

/**
 *
 * @author Ismael Ortiz Pulido isamel.ortiz@sesaemm.org.mx
 */
 
public class tbitacoraBusquedasPDE 
{

    private String fechaConsulta;                                               
    private String sistema;                                                     
    private String privacidadSistema;                                           
    private ArrayList<filtrosPDE> filtrosDeBusqueda;                            
    private filtrosPDE filtroDeOrdenamiento;                                    

    public tbitacoraBusquedasPDE(){}

    public tbitacoraBusquedasPDE(String fechaConsulta, String sistema, String privacidadSistema, ArrayList<filtrosPDE> filtrosDeBusqueda, filtrosPDE filtroDeOrdenamiento) {
        this.fechaConsulta = fechaConsulta;
        this.sistema = sistema;
        this.privacidadSistema = privacidadSistema;
        this.filtrosDeBusqueda = filtrosDeBusqueda;
        this.filtroDeOrdenamiento = filtroDeOrdenamiento;
    }
    
    public String getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(String fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public ArrayList<filtrosPDE> getFiltrosDeBusqueda() {
        return filtrosDeBusqueda;
    }

    public void setFiltrosDeBusqueda(ArrayList<filtrosPDE> filtrosDeBusqueda) {
        this.filtrosDeBusqueda = filtrosDeBusqueda;
    }

    public filtrosPDE getFiltroDeOrdenamiento() {
        return filtroDeOrdenamiento;
    }

    public void setFiltroDeOrdenamiento(filtrosPDE filtroDeOrdenamiento) {
        this.filtroDeOrdenamiento = filtroDeOrdenamiento;
    }

    public String getPrivacidadSistema() {
        return privacidadSistema;
    }

    public void setPrivacidadSistema(String privacidadSistema) {
        this.privacidadSistema = privacidadSistema;
    }
    
}
