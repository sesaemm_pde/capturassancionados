/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s3Sancionados;

import java.util.ArrayList;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 */

public class PsancionadosConPaginacion
  {

    PAGINACION paginacion;                                                      
    ArrayList<ParticularSancionado> servidores;                     

    public PsancionadosConPaginacion()
      {
      }

    public PsancionadosConPaginacion(PAGINACION paginacion, ArrayList<ParticularSancionado> servidores)
      {
        this.paginacion = paginacion;
        this.servidores = servidores;
      }

    public PAGINACION getPaginacion()
      {
        return paginacion;
      }

    public void setPaginacion(PAGINACION paginacion)
      {
        this.paginacion = paginacion;
      }

    public ArrayList<ParticularSancionado> getServidores()
      {
        return servidores;
      }

    public void setServidores(ArrayList<ParticularSancionado> servidores)
      {
        this.servidores = servidores;
      }

  }
