/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans.s3Sancionados;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 */
 
public class ServidorPublicoSancionado
  {
    @BsonId
    @BsonProperty("_id")
    private ObjectId id;
    private String nombreCompleto;
    private String institucionDependenciaNombre;
    private String cargo;
    private String publicar;
    private String usuarioRegistro;
    
    public ObjectId getId()
      {
        return id;
      }

    public void setId(ObjectId id)
      {
        this.id = id;
      }

    public String getNombreCompleto()
      {
        return nombreCompleto;
      }

    public void setNombreCompleto(String nombreCompleto)
      {
        this.nombreCompleto = nombreCompleto;
      }

    public String getInstitucionDependenciaNombre()
      {
        return institucionDependenciaNombre;
      }

    public void setInstitucionDependenciaNombre(String institucionDependenciaNombre)
      {
        this.institucionDependenciaNombre = institucionDependenciaNombre;
      }

    public String getCargo()
      {
        return cargo;
      }

    public void setCargo(String cargo)
      {
        this.cargo = cargo;
      }

    public String getPublicar()
      {
        return publicar;
      }

    public void setPublicar(String publicar)
      {
        this.publicar = publicar;
      }
      
    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

  }
