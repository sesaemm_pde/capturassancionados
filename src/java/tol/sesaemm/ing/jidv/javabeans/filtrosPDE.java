package tol.sesaemm.ing.jidv.javabeans;

/**
 *
 * @author Ismael Ortiz Pulido isamel.ortiz@sesaemm.org.mx
 */

public class filtrosPDE 
{

    private String filtro;                                                      
    private String valor;                                                      

    public filtrosPDE(){}
        
    public filtrosPDE(String filtro, String valor) 
    {
        this.filtro = filtro;
        this.valor = valor;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
