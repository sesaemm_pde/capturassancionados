/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
public class Miga
  {

    String valorMiga;
    String vinculoMiga;
    String contexto;

    public Miga(String valorMiga, String vinculoMiga, String contexto)
      {
        this.valorMiga = valorMiga;
        this.vinculoMiga = vinculoMiga;
        this.contexto = contexto;
      }

    
    
    public Miga(String valorMiga, String vinculoMiga)
      {
        this.valorMiga = valorMiga;
        this.vinculoMiga = vinculoMiga;
      }

    public String getValorMiga()
      {
        return valorMiga;
      }

    public void setValorMiga(String valorMiga)
      {
        this.valorMiga = valorMiga;
      }

    public String getVinculoMiga()
      {
        return vinculoMiga;
      }

    public void setVinculoMiga(String vinculoMiga)
      {
        this.vinculoMiga = vinculoMiga;
      }

    public String getContexto()
      {
        return contexto;
      }

    public void setContexto(String contexto)
      {
        this.contexto = contexto;
      }

    
  }
