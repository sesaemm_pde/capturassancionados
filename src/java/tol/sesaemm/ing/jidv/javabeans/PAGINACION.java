/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.javabeans;

import java.io.Serializable;

/**
 *
 * @author Ivan Cornejo Quintan ivan.cornejo@sesaemm.org.mx
 */
public class PAGINACION implements Serializable
  {
    private int registrosMostrar;
    private int numeroPagina;
    private int totalPaginas;
    private int numeroSaltos;
    private int totalRegistros;
    private boolean isEmpty;

    public PAGINACION()
      {
        registrosMostrar = 10;
        numeroPagina = 1;
        totalPaginas = 0;
        numeroSaltos = 0;
        totalRegistros = 0;
        isEmpty = true;
      }

    public PAGINACION(boolean isEmpty)
      {
        this.isEmpty = isEmpty;
      }
    
    public int getRegistrosMostrar()
      {
        return registrosMostrar;
      }

    public void setRegistrosMostrar(int registrosMostrar)
      {
        this.registrosMostrar = registrosMostrar;
      }

    public int getNumeroPagina()
      {
        return numeroPagina;
      }

    public void setNumeroPagina(int numeroPagina)
      {
        this.numeroPagina = numeroPagina;
      }

    public int getTotalPaginas()
      {
        return totalPaginas;
      }

    public void setTotalPaginas(int totalPaginas)
      {
        this.totalPaginas = totalPaginas;
      }

    public int getNumeroSaltos()
      {
        return numeroSaltos;
      }

    public void setNumeroSaltos(int numeroSaltos)
      {
        this.numeroSaltos = numeroSaltos;
      }

    public int getTotalRegistros()
      {
        return totalRegistros;
      }

    public void setTotalRegistros(int totalRegistros)
      {
        this.totalRegistros = totalRegistros;
      }

    public boolean isEmpty()
      {
        return isEmpty;
      }

    public void setIsEmpty(boolean isEmpty)
      {
        this.isEmpty = isEmpty;
      }
    
  }
