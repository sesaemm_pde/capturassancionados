/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
package tol.sesaemm.ing.jidv.javabeans; 
import java.util.ArrayList; 
import tol.sesaemm.javabeans.DOMICILIO_PDE;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.SISTEMAS;

/**
 *
 * @author sesaemm
 */
public class USUARIO
  {
    private String usuario;
    private String password;
    private String nombre;
    private String primer_apellido;
    private String segundo_apellido;
    private String puesto;
    private DOMICILIO_PDE domicilio;
    private String correo_electronico;
    private String telefono;
    private ENTE_PUBLICO dependencia;
    private ArrayList<SISTEMAS> sistemas; 
    private ArrayList<ENTE_PUBLICO> dependencias;
    private String nombreCompleto;

    public String getUsuario()
      {
        return usuario;
      }

    public void setUsuario(String usuario)
      {
        this.usuario = usuario;
      }

    public String getPassword()
      {
        return password;
      }

    public void setPassword(String password)
      {
        this.password = password;
      }

    public String getNombre()
      {
        return nombre;
      }

    public void setNombre(String nombre)
      {
        this.nombre = nombre;
      }

    public String getPrimer_apellido()
      {
        return primer_apellido;
      }

    public void setPrimer_apellido(String primer_apellido)
      {
        this.primer_apellido = primer_apellido;
      }

    public String getSegundo_apellido()
      {
        return segundo_apellido;
      }

    public void setSegundo_apellido(String segundo_apellido)
      {
        this.segundo_apellido = segundo_apellido;
      }

    public ENTE_PUBLICO getDependencia()
      {
        return dependencia;
      }

    public void setDependencia(ENTE_PUBLICO dependencia)
      {
        this.dependencia = dependencia;
      }

    public ArrayList<SISTEMAS> getSistemas()
      {
        return sistemas;
      }

    public void setSistemas(ArrayList<SISTEMAS> sistemas)
      {
        this.sistemas = sistemas;
      }

    public String getCorreo_electronico()
      {
        return correo_electronico;
      }

    public void setCorreo_electronico(String correo_electronico)
      {
        this.correo_electronico = correo_electronico;
      }

    public ArrayList<ENTE_PUBLICO> getDependencias()
      {
        return dependencias;
      }

    public void setDependencias(ArrayList<ENTE_PUBLICO> dependencias)
      {
        this.dependencias = dependencias;
      }

    public String getPuesto()
      {
        return puesto;
      }

    public void setPuesto(String puesto)
      {
        this.puesto = puesto;
      }

    public DOMICILIO_PDE getDomicilio()
      {
        return domicilio;
      }

    public void setDomicilio(DOMICILIO_PDE domicilio)
      {
        this.domicilio = domicilio;
      }

    public String getTelefono()
      {
        return telefono;
      }

    public void setTelefono(String telefono)
      {
        this.telefono = telefono;
      }

    public String getNombreCompleto()
      {
        if(this.nombre != null
           || this.primer_apellido != null
           || this.segundo_apellido != null)
          {
            this.nombreCompleto = this.nombre+ " " + this.primer_apellido+ " " + this.segundo_apellido;
        }
        else
          {
            this.nombreCompleto  = "";
          }
        return this.nombreCompleto;
      }
    
  }
