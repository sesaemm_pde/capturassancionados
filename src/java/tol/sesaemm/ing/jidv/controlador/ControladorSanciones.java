/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocioS3;
import tol.sesaemm.ing.jidv.javabeans.Miga;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_SSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.ServidorPublicoSancionado;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.SsancionadosConPaginacion;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioS3V01;
import tol.sesaemm.ing.jidv.logicaDeNegocio.LogicaDeNegocioV01;
import tol.sesaemm.javabeans.ASENTAMIENTO;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.ENTIDAD_FEDERATIVA_PDE;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.MUNICIPIO_PDE;
import tol.sesaemm.javabeans.ORGANO_INTERNO_CONTROL;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.TIPO_DOCUMENTO;
import tol.sesaemm.javabeans.UNIDAD_MEDIDA_PLAZO;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_SANCION_SER;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class ControladorSanciones extends tol.sesaemm.ing.jidv.controlador.ControladorBase
{

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException
    {
        String strAccion = "";
        LogicaDeNegocio LN = new LogicaDeNegocioV01();
        LogicaDeNegocioS3 LN3 = new LogicaDeNegocioS3V01();
        HttpSession sesion = request.getSession();
        USUARIO usuario;
        NIVEL_ACCESO nivelAcceso;

        usuario = (USUARIO) sesion.getAttribute("usuario");
        strAccion = request.getParameter("accion");

        try
        {
            if (strAccion == null)
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (strAccion.equals("paginaPrincipal"))
            {
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (strAccion.equals("loguearUsuario"))
            {

                usuario = new USUARIO();
                usuario.setUsuario(request.getParameter("txtUsuario"));
                usuario.setPassword(request.getParameter("txtPassword"));

                usuario = LN.loguearUsuario(usuario);

                if (usuario != null)
                {

                    sesion = request.getSession(true);
                    sesion.setAttribute("usuario", usuario);
                    request.setAttribute("sistemas", LN.obtenerDatosActualizadosDeSistemas(usuario.getSistemas()));
                    this.fordward(request, resp, "/frmMenuSistemas.jsp");

                }
                else
                {
                    if (sesion != null)
                    {
                        sesion.invalidate();
                        sesion = null;
                    }

                    request.setAttribute("mensaje", "Usuario o contraseña incorrectos");
                    this.fordward(request, resp, "/frmPrincipal.jsp");
                }

            }
            else if (strAccion.equals("cerrarSesion"))
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
            else if (usuario != null)
            {
                nivelAcceso = LN.tipoDeUsuario(usuario, 3);
                if (strAccion.equals("ingresarSistema"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "", "sanciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("paginacion", new PAGINACION());
                    request.setAttribute("esAdmin", LN.esAdmin(usuario, 2));
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalSancionados.jsp");
                }

                else if (strAccion.equals("consultarServidoresPublicosSancionados"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ServidorPublicoSancionado> servidoresPublicosSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    PAGINACION paginacion;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;
                    SsancionadosConPaginacion listaDeObjetos;

                    paginacion = new PAGINACION();
                    servidoresPublicosSancionados = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "", "sanciones"));

                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);
                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerServidoresPublicosSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        servidoresPublicosSancionados = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("servidoresPublicos", servidoresPublicosSancionados);

                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalSancionados.jsp");
                }

                else if (strAccion.equals("detalleServidorPublicoSancionado"))
                {
                    SSANCIONADOS privacidad;
                    SSANCIONADOS servidoresPublicosSancionados;
                    String idServidorPublico;
                    ArrayList<Miga> migas;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;

                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);
                    idServidorPublico = request.getParameter("txtId");
                    servidoresPublicosSancionados = LN3.obtenerDetalleServidorPublicoSancionado(idServidorPublico);
                    privacidad = LN3.obtenerEstatusDePrivacidadCamposSSANCIONADOS();

                    migas = new ArrayList<>();
                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "ingresarSistema", "sanciones"));
                    migas.add(new Miga("Datos del servidor p&uacute;blico", "", "contrataciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("privacidadDatos", privacidad);
                    request.setAttribute("servidorPublico", servidoresPublicosSancionados);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    this.fordward(request, resp, "s3Sancionados/frmDetalleServidorSancionado.jsp");
                }

                else if (strAccion.equals("mostrarPaginaRegistroNuevoServidorPublicoSancionado"))
                {
                    ArrayList<Miga> migas = new ArrayList<>();
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<PUESTO> puestos;
                    ArrayList<GENERO> generos;
                    ArrayList<TIPO_FALTA> tipoFaltas;
                    ArrayList<TIPO_SANCION_SER> tipoSancion;
                    ArrayList<MONEDA> tipoMoneda;
                    ArrayList<UNIDAD_MEDIDA_PLAZO> unidadesMedidasPlazos;
                    ArrayList<ORGANO_INTERNO_CONTROL> organosInternos;
                    ArrayList<TIPO_DOCUMENTO> tiposDeDocumento;

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "ingresarSistema", "sanciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "sanciones"));

                    puestos = LN3.obtenerListaDePuestos();
                    generos = LN3.obtenerListaDeGeneros();
                    entesPublicos = LN3.obtenerEntesPublicos(usuario);
                    tipoFaltas = LN3.obtenerListaTiposDeFalta();
                    tipoSancion = LN3.obtenerListaTiposDeSancion();
                    tipoMoneda = LN3.obtenerListaDeTiposDeMoneda();
                    unidadesMedidasPlazos = LN3.obtenerListaDeUnidadMedidaPlazo();
                    organosInternos = LN3.obtenerListaDeOrganoInternoDeControl();
                    tiposDeDocumento = LN3.obtenerTiposDeDocumentos();

                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("migas", migas);
                    request.setAttribute("generos", generos);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("tipoFaltas", tipoFaltas);
                    request.setAttribute("tipoSancion", tipoSancion);
                    request.setAttribute("tipoMoneda", tipoMoneda);
                    request.setAttribute("unidadesMedidasPlazos", unidadesMedidasPlazos);
                    request.setAttribute("organosInternos", organosInternos);
                    request.setAttribute("tiposDeDocumento", tiposDeDocumento);
                    this.fordward(request, resp, "s3Sancionados/frmRegistraServidorPublicoSancionado.jsp");
                }

                else if (strAccion.equals("mostrarVistaPreviaServidorPublicoSancionado"))
                {
                    SSANCIONADOS servidorPublicosSancionado;
                    ArrayList<Miga> migas;
                    String[] arregloDePlazo;

                    migas = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "ingresarSistema", "sanciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "sanciones"));

                    servidorPublicosSancionado = LN3.modelarRequestServidorPublicosSancionado(request);
                    arregloDePlazo = (servidorPublicosSancionado.getInhabilitacion().getPlazo() == null) ? new String[2] : servidorPublicosSancionado.getInhabilitacion().getPlazo().split(" ");

                    request.setAttribute("migas", migas);
                    request.setAttribute("servidorPublico", servidorPublicosSancionado);
                    request.setAttribute("arregloDePlazo", arregloDePlazo);
                    this.fordward(request, resp, "s3Sancionados/frmVistaPreviaServidorPublicoSancionado.jsp");
                }

                else if (strAccion.equals("modificarDatosServidorPublicoSancionado"))
                {
                    SSANCIONADOS servidorPublicosSancionado;
                    ArrayList<Miga> migas;
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<PUESTO> puestos;
                    ArrayList<GENERO> generos;
                    ArrayList<TIPO_FALTA> tipoFaltas;
                    ArrayList<TIPO_SANCION_SER> tipoSancion;
                    ArrayList<MONEDA> tipoMoneda;
                    ArrayList<ORGANO_INTERNO_CONTROL> organosInternos;
                    String[] arregloDePlazo;
                    ArrayList<UNIDAD_MEDIDA_PLAZO> unidadesMedidasPlazos;
                    ArrayList<TIPO_DOCUMENTO> tiposDeDocumento;

                    migas = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "ingresarSistema", "sanciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "sanciones"));

                    servidorPublicosSancionado = LN3.modelarRequestServidorPublicosSancionado(request);
                    puestos = LN3.obtenerListaDePuestos();
                    generos = LN3.obtenerListaDeGeneros();
                    entesPublicos = LN3.obtenerEntesPublicos(usuario);
                    tipoFaltas = LN3.obtenerListaTiposDeFalta();
                    tipoSancion = LN3.obtenerListaTiposDeSancion();
                    tipoMoneda = LN3.obtenerListaDeTiposDeMoneda();
                    unidadesMedidasPlazos = LN3.obtenerListaDeUnidadMedidaPlazo();
                    organosInternos = LN3.obtenerListaDeOrganoInternoDeControl();
                    tiposDeDocumento = LN3.obtenerTiposDeDocumentos();
                    arregloDePlazo = (servidorPublicosSancionado.getInhabilitacion().getPlazo() == null) ? new String[2] : servidorPublicosSancionado.getInhabilitacion().getPlazo().split(" ");

                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("migas", migas);
                    request.setAttribute("generos", generos);
                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("tipoFaltas", tipoFaltas);
                    request.setAttribute("tipoSancion", tipoSancion);
                    request.setAttribute("tipoMoneda", tipoMoneda);
                    request.setAttribute("migas", migas);
                    request.setAttribute("servidorPublico", servidorPublicosSancionado);
                    request.setAttribute("arregloDePlazo", arregloDePlazo);
                    request.setAttribute("unidadesMedidasPlazos", unidadesMedidasPlazos);
                    request.setAttribute("organosInternos", organosInternos);
                    request.setAttribute("tiposDeDocumento", tiposDeDocumento);
                    this.fordward(request, resp, "s3Sancionados/frmRegistraServidorPublicoSancionado.jsp");
                }

                else if (strAccion.equals("registrarServidorPublicoSancionado"))
                {
                    SSANCIONADOS servidorPublico;
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ServidorPublicoSancionado> servidoresPublicosSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    PAGINACION paginacion;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;
                    SsancionadosConPaginacion listaDeObjetos;
                    String mensaje;

                    paginacion = new PAGINACION();
                    servidoresPublicosSancionados = new ArrayList<>();

                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);

                    servidorPublico = LN3.modelarRequestServidorPublicosSancionado(request);
                    mensaje = LN3.registrarServidorPublicoSancionado(servidorPublico, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerServidoresPublicosSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        servidoresPublicosSancionados = listaDeObjetos.getServidores();
                    }

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "", "sanciones"));

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    request.setAttribute("servidoresPublicos", servidoresPublicosSancionados);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalSancionados.jsp");
                }

                else if (strAccion.equals("mostrarPaginaEditarServidorPublicoSancionado"))
                {
                    SSANCIONADOS servidoresPublicosSancionados;
                    String idServidorPublico;
                    ArrayList<Miga> migas;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;
                    ArrayList<ENTE_PUBLICO> entesPublicos;
                    ArrayList<PUESTO> puestos;
                    ArrayList<GENERO> generos;
                    ArrayList<TIPO_FALTA> tipoFaltas;
                    ArrayList<TIPO_SANCION_SER> tipoSancion;
                    ArrayList<MONEDA> tipoMoneda;
                    ArrayList<ORGANO_INTERNO_CONTROL> organosInternos;
                    String[] arregloDePlazo;
                    ArrayList<UNIDAD_MEDIDA_PLAZO> unidadesMedidasPlazos;
                    ArrayList<TIPO_DOCUMENTO> tiposDeDocumento;

                    migas = new ArrayList<>();

                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);
                    idServidorPublico = request.getParameter("txtId");
                    servidoresPublicosSancionados = LN3.obtenerDetalleServidorPublicoSancionado(idServidorPublico);
                    puestos = LN3.obtenerListaDePuestos();
                    generos = LN3.obtenerListaDeGeneros();
                    entesPublicos = LN3.obtenerEntesPublicos(usuario);
                    tipoFaltas = LN3.obtenerListaTiposDeFalta();
                    tipoSancion = LN3.obtenerListaTiposDeSancion();
                    tipoMoneda = LN3.obtenerListaDeTiposDeMoneda();
                    unidadesMedidasPlazos = LN3.obtenerListaDeUnidadMedidaPlazo();
                    organosInternos = LN3.obtenerListaDeOrganoInternoDeControl();
                    arregloDePlazo = (servidoresPublicosSancionados.getInhabilitacion().getPlazo() != null && !servidoresPublicosSancionados.getInhabilitacion().getPlazo().isEmpty()) ? servidoresPublicosSancionados.getInhabilitacion().getPlazo().split(" ") : null;
                    tiposDeDocumento = LN3.obtenerTiposDeDocumentos();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "ingresarSistema", "sanciones"));
                    migas.add(new Miga("Edici&oacute;n de servidor p&uacute;blico", "", ""));

                    request.setAttribute("entesPublicos", entesPublicos);
                    request.setAttribute("puestos", puestos);
                    request.setAttribute("migas", migas);
                    request.setAttribute("generos", generos);
                    request.setAttribute("tipoFaltas", tipoFaltas);
                    request.setAttribute("tipoSancion", tipoSancion);
                    request.setAttribute("tipoMoneda", tipoMoneda);
                    request.setAttribute("servidorPublico", servidoresPublicosSancionados);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("arregloDePlazo", arregloDePlazo);
                    request.setAttribute("unidadesMedidasPlazos", unidadesMedidasPlazos);
                    request.setAttribute("organosInternos", organosInternos);
                    request.setAttribute("tiposDeDocumento", tiposDeDocumento);
                    this.fordward(request, resp, "s3Sancionados/frmEditarServidorPublicoSancionado.jsp");
                }

                else if (strAccion.equals("editarDatosServidorPublicoSancionado"))
                {
                    SSANCIONADOS servidorPublico;
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ServidorPublicoSancionado> servidoresPublicosSancionados;
                    ArrayList<Miga> migas = new ArrayList<>();
                    PAGINACION paginacion;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;
                    SsancionadosConPaginacion listaDeObjetos;
                    String mensaje;

                    paginacion = new PAGINACION();
                    servidoresPublicosSancionados = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores p&uacute;blicos y particulares sancionados", "ingresarSistema", "sanciones"));
                    migas.add(new Miga("Registro de servidor p&uacute;blico", "", "sanciones"));

                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);

                    servidorPublico = LN3.obtenerDetalleServidorPublicoSancionado(filtroBusqueda.getID());
                    servidorPublico = LN3.modelarRequestServidorPublicosSancionado(request, servidorPublico);

                    mensaje = LN3.editarServidorPublicoSancionado(filtroBusqueda.getID(), servidorPublico, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerServidoresPublicosSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        servidoresPublicosSancionados = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    request.setAttribute("servidoresPublicos", servidoresPublicosSancionados);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalSancionados.jsp");
                }

                else if (strAccion.equals("publicarServidorPublicoSancionado"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    SSANCIONADOS servidorPublicos;
                    ArrayList<ServidorPublicoSancionado> listaDeServidorPublicos;
                    ArrayList<Miga> migas = new ArrayList<>();
                    String id;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;
                    String mensaje;
                    PAGINACION paginacion;
                    SsancionadosConPaginacion listaDeObjetos;

                    paginacion = new PAGINACION();
                    listaDeServidorPublicos = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores públicos que intervienen en contrataciones", "", "contrataciones"));

                    id = request.getParameter("txtId");
                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);
                    servidorPublicos = LN3.obtenerDetalleServidorPublicoSancionado(id);

                    mensaje = LN3.publicarServidorPublicoSancionado(id, servidorPublicos, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerServidoresPublicosSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        listaDeServidorPublicos = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("servidoresPublicos", listaDeServidorPublicos);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalSancionados.jsp");
                }

                else if (strAccion.equals("despublicarServidorPublicoSancionado"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    SSANCIONADOS servidorPublicos;
                    ArrayList<ServidorPublicoSancionado> listaDeServidorPublicos;
                    ArrayList<Miga> migas = new ArrayList<>();
                    String id;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;
                    String mensaje;
                    PAGINACION paginacion;
                    SsancionadosConPaginacion listaDeObjetos;

                    paginacion = new PAGINACION();
                    listaDeServidorPublicos = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores públicos que intervienen en contrataciones", "", "contrataciones"));

                    id = request.getParameter("txtId");
                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);
                    servidorPublicos = LN3.obtenerDetalleServidorPublicoSancionado(id);

                    mensaje = LN3.desPublicarServidorPublicoSancionado(id, servidorPublicos, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerServidoresPublicosSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        listaDeServidorPublicos = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("servidoresPublicos", listaDeServidorPublicos);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalSancionados.jsp");
                }

                else if (strAccion.equals("eliminarServidorPublicoSancionado"))
                {
                    ArrayList<ENTE_PUBLICO> dependenciasDelUsuario;
                    ArrayList<ServidorPublicoSancionado> listaDeServidorPublicos;
                    ArrayList<Miga> migas = new ArrayList<>();
                    String id;
                    FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda;
                    String mensaje;
                    PAGINACION paginacion;
                    SsancionadosConPaginacion listaDeObjetos;
                    SSANCIONADOS servidorPublico;

                    paginacion = new PAGINACION();
                    listaDeServidorPublicos = new ArrayList<>();

                    migas.add(new Miga("Plataforma digital estatal", "mostrarMenuSistemas", "intranet"));
                    migas.add(new Miga("Servidores públicos que intervienen en contrataciones", "", "contrataciones"));

                    id = request.getParameter("txtId");
                    filtroBusqueda = LN3.modelarFiltroServidorPublicoSancionado(request);

                    servidorPublico = LN3.obtenerDetalleServidorPublicoSancionado(id);
                    mensaje = LN3.eliminarServidorPublicoSancionado(id, servidorPublico, usuario, nivelAcceso);

                    dependenciasDelUsuario = LN3.obtenerDependenciasDelUsuario(usuario);
                    listaDeObjetos = LN3.obtenerServidoresPublicosSancionados(usuario, nivelAcceso, dependenciasDelUsuario, filtroBusqueda);
                    if (listaDeObjetos != null)
                    {
                        paginacion = listaDeObjetos.getPaginacion();
                        listaDeServidorPublicos = listaDeObjetos.getServidores();
                    }

                    request.setAttribute("migas", migas);
                    request.setAttribute("filtro", filtroBusqueda);
                    request.setAttribute("paginacion", paginacion);
                    request.setAttribute("nivelDeAcceso", nivelAcceso);
                    request.setAttribute("irASeccion", true);
                    request.setAttribute("servidoresPublicos", listaDeServidorPublicos);
                    request.setAttribute("correcto", true);
                    request.setAttribute("mensaje", mensaje);
                    this.fordward(request, resp, "s3Sancionados/frmPaginaPrincipalSancionados.jsp");
                }
                else if (strAccion.equals("obtenerDatosDelDomicilioEnMexico"))
                {
                    ArrayList<ASENTAMIENTO> asentamientos;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    asentamientos = LN3.obtenerAsentamientoPorCP(parameter);
                    String json = gson.toJson(asentamientos);
                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }
                else if (strAccion.equals("obtenerListaDeMunicipios"))
                {
                    ArrayList<MUNICIPIO_PDE> municipios;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    municipios = LN3.obtenerListaDeMunicipiosporEstado(parameter);
                    String json = gson.toJson(municipios);

                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }

                else if (strAccion.equals("obtenerListaDeEntidadFederativa"))
                {
                    ArrayList<ENTIDAD_FEDERATIVA_PDE> entidades;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    entidades = LN3.obtenerListaDeEntidadesFederativa(parameter);
                    String json = gson.toJson(entidades);

                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }

                else if (strAccion.equals("obtenerListaDeLocalidad"))
                {
                    ArrayList<ASENTAMIENTO> localidades;
                    Gson gson;

                    gson = new Gson();

                    String parameter = request.getParameter("txtId");
                    localidades = LN3.obtenerAsentamiento(parameter);
                    String json = gson.toJson(localidades);

                    resp.setContentType("application/json");
                    resp.setCharacterEncoding("ISO-8859-1");
                    resp.getWriter().write(json);

                }

            }
            else
            {
                if (sesion != null)
                {
                    sesion.invalidate();
                    sesion = null;
                }

                request.setAttribute("mensaje", "Su sesi&oacute;n ha expirado ");
                this.fordward(request, resp, "/frmPrincipal.jsp");
            }
        }
        catch (ServletException | IOException ex)
        {
            request.setAttribute("mensaje", "Error : " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
        catch (Exception ex)
        {
            request.setAttribute("mensaje", "Error " + ex.toString());
            this.fordward(request, resp, "/error.jsp");
        }
    }

    public void fordward(HttpServletRequest req, HttpServletResponse resp, String ruta) throws ServletException, IOException
    {
        req.setAttribute("datosDeLaUltimaActualizacion", ConfVariables.getDATOS_ACTUALIZACION());
        RequestDispatcher despachador = req.getRequestDispatcher(ruta);
        despachador.forward(req, resp);
    }
}
