/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaDeNegocio;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.integracion.DAOAdministrador;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocio;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.javabeans.SISTEMAS;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */

public class LogicaDeNegocioV01 implements LogicaDeNegocio
{

    /**
     * Ingresar al usuario en sesión
     *
     * @param usuario Usuario y contraseña de acceso
     * @return Usuario Datos del usuario logeado
     * @throws Exception
     */
    @Override
    public USUARIO loguearUsuario(USUARIO usuario) throws Exception
    {
        return DAOAdministrador.loguearUsuario(usuario);
    }

    @Override
    public boolean registrarAcceso(String usuario) throws Exception
    {
        return DAOAdministrador.registrarAcceso(usuario);
    }

    /**
     *
     * Modelar datos del usuario para registro
     *
     * @param request Request del formulario
     * @return Usuario Datos del usuario logeado
     */
    @Override
    public USUARIO modelarUsuario(HttpServletRequest request)
    {
        USUARIO usuario = new USUARIO();
        usuario.setUsuario(request.getParameter("txtCURP"));
        usuario.setCorreo_electronico(request.getParameter("txtCorreoElectronico"));
        usuario.setNombre(request.getParameter("txtNombre"));

        return usuario;
    }

    /**
     * Verificar si el usuario es administrador en un sistema
     *
     * @param usuario Usuario en sesión
     * @param idSistema Número de sistema que se quiere validar
     * @return boolean Devuelve un valor verdadero si el usuario es a
     * ministrador del sistema y falso si no lo es
     */
    @Override
    public boolean esAdmin(USUARIO usuario, int idSistema)
    {
        ArrayList<SISTEMAS> sistemas = usuario.getSistemas();
        for (SISTEMAS sis : sistemas)
        {
            if (sis.getSistema().getId() == idSistema)
            {
                if (sis.getNivel_acceso().getId() == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Obtiene la lista de sistemas asignados al usuario
     *
     * @param sistemasUsuario Lista de los sistemas asignados
     * @return ArrayList
     * @throws Exception
     */
    @Override
    public ArrayList<SISTEMAS> obtenerDatosActualizadosDeSistemas(ArrayList<SISTEMAS> sistemasUsuario) throws Exception
    {
        return DAOAdministrador.obtenerDatosActualizadosDeSistemas(sistemasUsuario);
    }

    /**
     * Devuelve el nivel de acceso del usuario en sesi&oacute;n
     *
     * @param usuario Usuario en sesión
     * @param idSistema Número de sistema que se quiere validar
     *
     * @return NIVEL_ACCESO
     * @throws java.lang.Exception
     */
    @Override
    public NIVEL_ACCESO tipoDeUsuario(USUARIO usuario, int idSistema) throws Exception
    {
        NIVEL_ACCESO nivelAcceso;

        nivelAcceso = new NIVEL_ACCESO();
        ArrayList<SISTEMAS> sistemas = usuario.getSistemas();
        for (SISTEMAS sis : sistemas)
        {
            if (sis.getSistema().getId() == idSistema)
            {
                switch (sis.getNivel_acceso().getId())
                {
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                        nivelAcceso.setId(sis.getNivel_acceso().getId());
                        nivelAcceso.setDescripcion(sis.getNivel_acceso().getDescripcion());
                        break;
                    default:
                        throw new Exception("Error no se reconocen los permisos del usuario");
                }
            }
        }
        return nivelAcceso;
    }

    /**
     * Convierte una cadena, en una expresión regular que contiene o no, acentos
     *
     * @param palabra
     * @return
     */
    @Override
    public String crearExpresionRegularDePalabra(String palabra)
    {

        String expReg = "";
        char[] arregloDePalabras = palabra.toLowerCase().toCharArray();

        for (char letra : arregloDePalabras)
        {
            switch (letra)
            {
                case 'a':
                    expReg += "[aáAÁ]";
                    break;
                case 'á':
                    expReg += "[aáAÁ]";
                    break;
                case 'e':
                    expReg += "[eéEÉ]";
                    break;
                case 'é':
                    expReg += "[eéEÉ]";
                    break;
                case 'i':
                    expReg += "[iíIÍ]";
                    break;
                case 'í':
                    expReg += "[iíIÍ]";
                    break;
                case 'o':
                    expReg += "[oóOÓ]";
                    break;
                case 'ó':
                    expReg += "[oóOÓ]";
                    break;
                case 'u':
                    expReg += "[uúUÚ]";
                    break;
                case 'ú':
                    expReg += "[uúUÚ]";
                    break;
                default:
                    expReg += letra;
                    break;
            }
        }

        return expReg;
    }

}
