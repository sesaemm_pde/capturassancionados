/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.logicaDeNegocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.bson.Document;
import tol.sesaemm.funciones.ExpresionesRegulares;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.funciones.ObtenerRfc;
import tol.sesaemm.ing.jidv.integracion.DAOSanciones;
import tol.sesaemm.ing.jidv.interfaces.LogicaDeNegocioS3;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_SSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.SsancionadosConPaginacion;
import tol.sesaemm.javabeans.ASENTAMIENTO;
import tol.sesaemm.javabeans.DOCUMENTO;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.ENTIDAD_FEDERATIVA_PDE;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.INHABILITACION;
import tol.sesaemm.javabeans.INSTITUCION_DEPENDENCIA;
import tol.sesaemm.javabeans.METADATA;
import tol.sesaemm.javabeans.METADATOS;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.MUNICIPIO_PDE;
import tol.sesaemm.javabeans.ORGANO_INTERNO_CONTROL;
import tol.sesaemm.javabeans.PAIS;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.TIPO_DOCUMENTO;
import tol.sesaemm.javabeans.TIPO_PERSONA;
import tol.sesaemm.javabeans.UNIDAD_MEDIDA_PLAZO;
import tol.sesaemm.javabeans.s3ssancionados.MULTA_SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.RESOLUCION_SER;
import tol.sesaemm.javabeans.s3ssancionados.SERVIDOR_PUBLICO_SANCIONADO;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_SANCION_SER;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */

public class LogicaDeNegocioS3V01 implements LogicaDeNegocioS3
{

    /**
     * Modela los par&aacute;metros que son utilizados para la b&uacute;squeda
     *
     * @param request HttpServletRequest
     *
     * @return
     */
    @Override
    public FILTRO_BUSQUEDA_SSANCIONADOS modelarFiltroServidorPublicoSancionado(HttpServletRequest request)
    {
        FILTRO_BUSQUEDA_SSANCIONADOS filtro;
        filtro = new FILTRO_BUSQUEDA_SSANCIONADOS();
        int numeroPagina;                               
        int registrosMostrar;                           
        String orden;                                   
        String id;                                      
        String nombre;                                  
        String primerApellido;                          
        String segundoApellido;                         
        String oficina;                                 
        String curp;                                    

        registrosMostrar = (request.getParameter("cmbPaginacion") == null) ? 10 : Integer.parseInt(request.getParameter("cmbPaginacion"));
        numeroPagina = (request.getParameter("txtNumeroPagina") == null ? 1 : Integer.parseInt(request.getParameter("txtNumeroPagina")));
        orden = (request.getParameter("cmbOrdenacion") == null) ? "nombres" : request.getParameter("cmbOrdenacion");
        id = (request.getParameter("txtId") == null) ? "" : request.getParameter("txtId");

        nombre = (request.getParameter("txtNombre") == null) ? "" : request.getParameter("txtNombre");
        primerApellido = (request.getParameter("txtPrimerApellido") == null) ? "" : request.getParameter("txtPrimerApellido");
        segundoApellido = (request.getParameter("txtSegundoApellido") == null) ? "" : request.getParameter("txtSegundoApellido");
        oficina = (request.getParameter("txtInstitucion") == null) ? "" : request.getParameter("txtInstitucion");
        curp = (request.getParameter("txtCurp") == null) ? "" : request.getParameter("txtCurp");

        filtro.setID(id);
        filtro.setNOMBRES(nombre.trim());
        filtro.setPRIMER_APELLIDO(primerApellido.trim());
        filtro.setSEGUNDO_APELLIDO(segundoApellido.trim());
        filtro.setINSTITUCION_DEPENDENCIA(oficina.trim());
        filtro.setCURP(curp.trim());
        filtro.setNumeroPagina(numeroPagina);
        filtro.setRegistrosMostrar(registrosMostrar);
        filtro.setOrden(orden);

        return filtro;
    }

    /**
     * Obtener listado de las dependencias que el usuario puede modificar o
     * consultar
     *
     * @param usuario Usuario en sesión
     *
     * @return ArrayList Listado de dependencias
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerDependenciasDelUsuario(USUARIO usuario) throws Exception
    {
        ArrayList<ENTE_PUBLICO> dependencias = usuario.getDependencias();
        return dependencias;
    }

    /**
     * Devuelve la lista de los servidores publico sancionados
     *
     * @param usuario Informaci&oacute;n del usuario
     * @param nivelAcceso
     * @param dependencias Dependencias asignadas al usuario
     * @param filtroBusqueda filtro de b&uacute;squeda
     *
     * @return ArrayList Object con la paginaci&oacute;n y la lista de los
     * servidores sancionado
     *
     * @throws Exception
     */
    @Override
    public SsancionadosConPaginacion obtenerServidoresPublicosSancionados(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependencias, FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda) throws Exception
    {
        return DAOSanciones.obtenerServidoresPublicosSancionados(usuario, nivelAcceso, dependencias, filtroBusqueda);
    }

    /**
     * Devuelve el detalle de un servidor sancionado por su id
     *
     * @param idServidorPublico identificado del servidor p&uacute;blico
     *
     * @return SSANCIONADOS
     *
     * @throws Exception
     */
    @Override
    public SSANCIONADOS obtenerDetalleServidorPublicoSancionado(String idServidorPublico) throws Exception
    {
        return DAOSanciones.obtenerDetalleServidorPublicoSancionado(idServidorPublico);
    }

    /**
     * Devuelve el modelo de SSANCIONADOS con los campos que son publico o
     * privados de la colecci&oacute;n de servidores sancionados
     *
     * @return SSANCIONADOS con campos publico y privados
     *
     * @throws Exception
     */
    @Override
    public SSANCIONADOS obtenerEstatusDePrivacidadCamposSSANCIONADOS() throws Exception
    {
        return DAOSanciones.obtenerEstatusDePrivacidadCamposSSANCIONADOS("ssancionados");
    }

    /**
     * Devuelve la lista de puestos registrado en la base de datos
     *
     * @return ArrayList PUESTO
     *
     * @throws Exception
     */
    @Override
    public ArrayList<PUESTO> obtenerListaDePuestos() throws Exception
    {
        return DAOSanciones.obtenerListaDePuestos();
    }

    /**
     * Devuelve la lista de g&eacute;neros almacenados en la base de datos
     *
     * @return ArrayList GENERO
     */
    @Override
    public ArrayList<GENERO> obtenerListaDeGeneros() throws Exception
    {
        return DAOSanciones.obtenerListaDeGeneros();
    }

    /**
     * Devuelve el listado de las diferentes dependencias que est&aacute;n
     * registradas en la base de datos
     *
     * @param usuario
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(USUARIO usuario) throws Exception
    {
        return DAOSanciones.obtenerEntesPublicos(usuario);
    }

    /**
     * Devuelve la lista de las diferentes faltas registradas en la base de
     * datos
     *
     * @return ArrayList TIPO_FALTA
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_FALTA> obtenerListaTiposDeFalta() throws Exception
    {
        return DAOSanciones.obtenerListaTiposDeFalta();
    }

    /**
     * Devuelve la lista de tipos de sanciones registradas en la base de datos
     *
     * @return ArrayList TIPO_SANCION_SER
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_SANCION_SER> obtenerListaTiposDeSancion() throws Exception
    {
        return DAOSanciones.obtenerListaTiposDeSancion();
    }

    /**
     * Devuelve la lista de los tipos de monedas registrados en la base de datos
     *
     * @return ArrayList MONEDA
     */
    @Override
    public ArrayList<MONEDA> obtenerListaDeTiposDeMoneda() throws Exception
    {
        return DAOSanciones.obtenerListaDeTiposDeMoneda();
    }

    /**
     * Devuelve el tipo de sanci&oacute;n y su descripci&oacute;n
     *
     * @param request HttpServletRequest
     *
     * @return ArrayList TIPO_SANCION_SER
     *
     * @throws Exception
     */
    private ArrayList<TIPO_SANCION_SER> obtenerTiposDeSancion(HttpServletRequest request) throws Exception
    {
        ArrayList<TIPO_SANCION_SER> tipoSancion;        
        List<String> codSancion;                        

        codSancion = Arrays.asList(request.getParameterValues("chckTIPO_SANCION"));
        tipoSancion = DAOSanciones.obtenerListaTiposDeSancion(codSancion);

        for (int i = 0; i < tipoSancion.size(); i++)
        {
            if (request.getParameter("txtTIPO_SANCION_" + tipoSancion.get(i).getClave() + "_DESCRIPCION") == null || request.getParameter("txtTIPO_SANCION_" + tipoSancion.get(i).getClave() + "_DESCRIPCION").isEmpty())
            {
                tipoSancion.get(i).setDescripcion(null);
            }
            else
            {
                tipoSancion.get(i).setDescripcion(request.getParameter("txtTIPO_SANCION_" + tipoSancion.get(i).getClave() + "_DESCRIPCION"));
            }
        } 

        return tipoSancion;
    }

    /**
     * Modela los datos del servidor p&uacute;blico
     *
     * @param request HttpServletRequest
     *
     * @return SSANCIONADOS
     */
    @Override
    public SSANCIONADOS modelarRequestServidorPublicosSancionado(HttpServletRequest request) throws Exception
    {
        SSANCIONADOS servidorPublico;                 
        INSTITUCION_DEPENDENCIA institucionDependencia;
        SERVIDOR_PUBLICO_SANCIONADO servidorPublicoSancionado;
        PUESTO puesto;                                        
        RESOLUCION_SER resolucion;                            
        MULTA_SSANCIONADOS multa;                             
        INHABILITACION inhabilitacion;                        
        Date dateInicial;                                     
        Date dateFinal;                                       
        ManejoDeFechas formatoFechas;                         
        String fechaInicial;                                  
        String fechaFinal;                                    
        UNIDAD_MEDIDA_PLAZO medidaPlazo;                      

        servidorPublico = new SSANCIONADOS();
        formatoFechas = new ManejoDeFechas("yyyy-MM-dd");
        institucionDependencia = new INSTITUCION_DEPENDENCIA();
        servidorPublicoSancionado = new SERVIDOR_PUBLICO_SANCIONADO();
        resolucion = new RESOLUCION_SER();
        multa = new MULTA_SSANCIONADOS();
        inhabilitacion = new INHABILITACION();

        servidorPublico.setFechaCaptura(new ManejoDeFechas().getDateNowAsString());
        servidorPublico.setExpediente((request.getParameter("txtNUMERO_EXPEDIENTE") == null || request.getParameter("txtNUMERO_EXPEDIENTE").isEmpty()) ? null : request.getParameter("txtNUMERO_EXPEDIENTE"));

        institucionDependencia = DAOSanciones.obtenerEntePublico(request.getParameter("cmbDEPENDENCIA"));

        servidorPublico.setInstitucionDependencia(institucionDependencia);

        servidorPublicoSancionado.setNombres(request.getParameter("txtNOMBRES"));
        servidorPublicoSancionado.setPrimerApellido(request.getParameter("txtPRIMER_APELLIDO"));
        servidorPublicoSancionado.setSegundoApellido((request.getParameter("txtSEGUNDO_APELLIDO") == null || request.getParameter("txtSEGUNDO_APELLIDO").isEmpty()) ? null : request.getParameter("txtSEGUNDO_APELLIDO"));

        servidorPublicoSancionado.setRfc(
                (request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty())
                ? (ExpresionesRegulares.validarRfcPF(request.getParameter("txtRFC")))
                ? request.getParameter("txtRFC").toUpperCase()
                : ""
                : ""
        );
        servidorPublicoSancionado.setCurp(
                (request.getParameter("txtCURP") != null && !request.getParameter("txtCURP").isEmpty())
                ? (ExpresionesRegulares.validarCurp(request.getParameter("txtCURP")))
                ? request.getParameter("txtCURP").toUpperCase()
                : ""
                : ""
        );

        if (servidorPublicoSancionado.getRfc() == null || servidorPublicoSancionado.getRfc().isEmpty())
        {
            servidorPublicoSancionado.setRfc(ObtenerRfc.obntenerRfcSPDN(servidorPublicoSancionado.getNombres(), servidorPublicoSancionado.getPrimerApellido(), servidorPublicoSancionado.getSegundoApellido()).toLowerCase());
        }
        if (servidorPublicoSancionado.getCurp() == null || servidorPublicoSancionado.getCurp().isEmpty())
        {
            servidorPublicoSancionado.setCurp(ObtenerRfc.obntenerRfcSPDN(servidorPublicoSancionado.getNombres(), servidorPublicoSancionado.getPrimerApellido(), servidorPublicoSancionado.getSegundoApellido()).toLowerCase());
        }

        servidorPublicoSancionado.setGenero((request.getParameter("radGENERO") == null || request.getParameter("radGENERO").isEmpty()) ? new GENERO() : DAOSanciones.obtenerGenero(request.getParameter("radGENERO")));

        puesto = DAOSanciones.obtenerPuesto(request.getParameter("cmbPUESTO"));
        servidorPublicoSancionado.setPuestoCompleto(puesto);
        servidorPublicoSancionado.setPuesto(puesto.getFuncional());
        servidorPublicoSancionado.setNivel(puesto.getNivel());

        servidorPublico.setServidorPublicoSancionado(servidorPublicoSancionado);

        servidorPublico.setAutoridadSancionadora((request.getParameter("txtAUTORIDAD_SANCIONADORA") == null || request.getParameter("txtAUTORIDAD_SANCIONADORA").isEmpty()) ? null : DAOSanciones.obtenerOrganoInternoDeControl(request.getParameter("txtAUTORIDAD_SANCIONADORA")).getValor());

        servidorPublico.setTipoFalta((request.getParameter("cmbTIPO_FALTAS") == null || request.getParameter("cmbTIPO_FALTAS").isEmpty()) ? new TIPO_FALTA() : DAOSanciones.obtenerTipoDeFalta(request.getParameter("cmbTIPO_FALTAS")));
        servidorPublico.getTipoFalta().setDescripcion((request.getParameter("txtTIPO_FALTAS_DESCRIPCION") == null || request.getParameter("txtTIPO_FALTAS_DESCRIPCION").isEmpty()) ? null : request.getParameter("txtTIPO_FALTAS_DESCRIPCION"));

        if (request.getParameterValues("chckTIPO_SANCION") == null || request.getParameterValues("chckTIPO_SANCION").length == 0)
        {
            servidorPublico.setTipoSancion(new ArrayList<>());
        }
        else
        {
            servidorPublico.setTipoSancion(this.obtenerTiposDeSancion(request));
        }

        servidorPublico.setCausaMotivoHechos(request.getParameter("txtCAUSA_MOTIVO"));

        resolucion.setUrl((request.getParameter("txtURL_RESOLUCION") == null || request.getParameter("txtURL_RESOLUCION").isEmpty()) ? null : request.getParameter("txtURL_RESOLUCION"));

        resolucion.setFechaResolucion((request.getParameter("txtFECHA_RESOLUCION") == null || request.getParameter("txtFECHA_RESOLUCION").isEmpty()) ? null : formatoFechas.getParseAString(request.getParameter("txtFECHA_RESOLUCION")));
        servidorPublico.setResolucion(resolucion);

        multa.setMonto((request.getParameter("txtMULTA_MONTO") == null || request.getParameter("txtMULTA_MONTO").isEmpty()) ? null : Double.parseDouble(request.getParameter("txtMULTA_MONTO")));

        multa.setMoneda((request.getParameter("cmbMONEDA") == null || request.getParameter("cmbMONEDA").isEmpty()) ? new MONEDA() : DAOSanciones.obtenerMoneda(request.getParameter("cmbMONEDA")));
        servidorPublico.setMulta(multa);

        if ((request.getParameter("txtPLAZO") == null || request.getParameter("txtPLAZO").isEmpty()) || (request.getParameter("cmbUNIDAD_MEDIDA_PLAZO") == null || request.getParameter("cmbUNIDAD_MEDIDA_PLAZO").isEmpty()))
        {
            inhabilitacion.setPlazo(null);
        }
        else
        {
            medidaPlazo = DAOSanciones.obtenerUnidadMedidaPlazo(request.getParameter("cmbUNIDAD_MEDIDA_PLAZO"));
            String plazo = (request.getParameter("txtPLAZO") != null)
                    ? request.getParameter("txtPLAZO").concat(" " + medidaPlazo.getValor())
                    : null;
            inhabilitacion.setPlazo((request.getParameter("txtPLAZO") == null || request.getParameter("txtPLAZO").isEmpty()) ? null : plazo);
        }

        if ((request.getParameter("txtFECHA_INICIAL") == null || request.getParameter("txtFECHA_INICIAL").isEmpty()) || (request.getParameter("txtFECHA_FINAL") == null || request.getParameter("txtFECHA_FINAL").isEmpty()))
        {
            inhabilitacion.setFechaInicial(null);
            inhabilitacion.setFechaFinal(null);
        }
        else
        {
            dateInicial = formatoFechas.getParse((request.getParameter("txtFECHA_INICIAL") != null) ? request.getParameter("txtFECHA_INICIAL") : null);
            dateFinal = formatoFechas.getParse((request.getParameter("txtFECHA_FINAL") != null) ? request.getParameter("txtFECHA_FINAL") : null);

            if (dateInicial.compareTo(dateFinal) == 0 || dateInicial.compareTo(dateFinal) == -1)
            { 
                fechaInicial = formatoFechas.getFormat(dateInicial);
                fechaFinal = formatoFechas.getFormat(dateFinal);
                inhabilitacion.setFechaInicial(fechaInicial);
                inhabilitacion.setFechaFinal(fechaFinal);
            } 
            else
            { 
                throw new Exception("Error: La fecha inicial de inhabilitaron debe ser mayor que la fecha final!");
            } 
        }
        servidorPublico.setInhabilitacion(inhabilitacion);

        servidorPublico.setObservaciones((request.getParameter("txtOBSERVACIONES") == null || request.getParameter("txtOBSERVACIONES").isEmpty()) ? null : request.getParameter("txtOBSERVACIONES"));

        if (request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar") == null)
        {
            servidorPublico.setDocumentos((request.getParameter("txtDOCUMENTO_NUM") == null || request.getParameter("txtDOCUMENTO_NUM").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }
        else
        {
            servidorPublico.setDocumentos((request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }

        return servidorPublico;
    }

    /**
     * Cambia el valor del campo publicar a 0 para indicar que el registro no
     * debe ser visible al p&uacute;blico
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param servidorPublicos Datos del servidor p&uacute;blico
     * @param usuario Usuario de despublicaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    @Override
    public String publicarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;   

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        {

            if (servidorPublicos.getMetadatos() != null)
            {
                servidorPublicos.getMetadatos().setUsuario_publicacion(usuario.getUsuario());
                servidorPublicos.getMetadatos().setFecha_publicacion(new ManejoDeFechas().getDateNowAsString());
            }
            else
            {
                METADATOS metadatos = new METADATOS();
                metadatos.setUsuario_publicacion(usuario.getUsuario());
                metadatos.setFecha_publicacion(new ManejoDeFechas().getDateNowAsString());
                servidorPublicos.setMetadatos(metadatos);
            }
            servidorPublicos.setPublicar("1");

            if (DAOSanciones.publicarServidorPublicoSancionado(id, servidorPublicos, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha publicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible publicar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Cambia el valor del campo publicar a 1 para indicar que el registro puede
     * ser visible al p&uacute;blico
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param servidorPublicos Datos del servidor p&uacute;blico
     * @param usuario Usuario de despublicaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    @Override
    public String desPublicarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 

            if (servidorPublicos.getMetadatos() != null)
            { 
                servidorPublicos.getMetadatos().setUsuario_despublicacion(usuario.getUsuario());
                servidorPublicos.getMetadatos().setFecha_despublicacion(new ManejoDeFechas().getDateNowAsString());
            } 
            else
            { 
                METADATOS metadatos = new METADATOS();
                metadatos.setUsuario_despublicacion(usuario.getUsuario());
                metadatos.setFecha_despublicacion(new ManejoDeFechas().getDateNowAsString());
                servidorPublicos.setMetadatos(metadatos);
            } 
            servidorPublicos.setPublicar("0");

            if (DAOSanciones.desPublicarServidorPublicoSancionado(id, servidorPublicos, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha despublicado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible despublicar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Borra de la base de datos el registro seleccionado por su id
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param servidorPublicos Datos del servidor p&uacute;blico
     * @param usuario Usuario de despublicaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    @Override
    public String eliminarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 
            if (DAOSanciones.eliminarServidorPublicoSancionado(id, servidorPublicos, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha eliminado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible eliminar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Devuelve un arreglo que contiene los atributos de los Documentos
     *
     * @param request HttpServletRequest
     *
     * @return ArrayList DOCUMENTO
     *
     * @throws Exception
     */
    private ArrayList<DOCUMENTO> obtenerLaCantidadDeValoresCadaDocumento(HttpServletRequest request) throws Exception
    {
        ArrayList<DOCUMENTO> listaDocumentos;                                                   
        DOCUMENTO documento;                                                                    
        ManejoDeFechas formatoFechas;                                                           
        listaDocumentos = new ArrayList<>();
        formatoFechas = new ManejoDeFechas("yyyy-MM-dd");
        String[] listaDeIdentificadoresDeDocumentosARegistrar;          

        try
        {
            if (request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar") != null)
            { 
                listaDeIdentificadoresDeDocumentosARegistrar = request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar").split(",");

                for (String identificadorDocumento : listaDeIdentificadoresDeDocumentosARegistrar)
                { 
                    documento = new DOCUMENTO();
                    documento.setId((request.getParameter("txtDOCUMENTO_ID_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_ID_" + identificadorDocumento));
                    documento.setTipo((request.getParameter("txtDOCUMENTO_TIPO_" + identificadorDocumento) == null || request.getParameter("txtDOCUMENTO_TIPO_" + identificadorDocumento).equals("")) ? "" : DAOSanciones.obtenerDocumento(request.getParameter("txtDOCUMENTO_TIPO_" + identificadorDocumento)).getValor());
                    documento.setTitulo((request.getParameter("txtDOCUMENTO_TITULO_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_TITULO_" + identificadorDocumento));
                    documento.setDescripcion((request.getParameter("txtDOCUMENTO_DESCRIPCION_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_DESCRIPCION_" + identificadorDocumento));
                    documento.setUrl((request.getParameter("txtDOCUMENTO_URL_" + identificadorDocumento) == null) ? "" : request.getParameter("txtDOCUMENTO_URL_" + identificadorDocumento));
                    documento.setFecha((request.getParameter("txtDOCUMENTO_FECHA_" + identificadorDocumento) == null) ? "" : formatoFechas.getParseAString(request.getParameter("txtDOCUMENTO_FECHA_" + identificadorDocumento)));
                    if (documento.getId().isEmpty() == false)
                    { 
                        listaDocumentos.add(documento);
                    } 
                } 
            } 
            else
            { 
                for (int i = 0; i <= (Integer.parseInt(request.getParameter("txtDOCUMENTO_NUM"))); i++)
                { 
                    documento = new DOCUMENTO();
                    documento.setId((request.getParameter("txtDOCUMENTO_ID_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_ID_" + i));
                    documento.setTipo((request.getParameter("txtDOCUMENTO_TIPO_" + i) == null || request.getParameter("txtDOCUMENTO_TIPO_" + i).equals("")) ? "" : DAOSanciones.obtenerDocumento(request.getParameter("txtDOCUMENTO_TIPO_" + i)).getValor());
                    documento.setTitulo((request.getParameter("txtDOCUMENTO_TITULO_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_TITULO_" + i));
                    documento.setDescripcion((request.getParameter("txtDOCUMENTO_DESCRIPCION_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_DESCRIPCION_" + i));
                    documento.setUrl((request.getParameter("txtDOCUMENTO_URL_" + i) == null) ? "" : request.getParameter("txtDOCUMENTO_URL_" + i));
                    documento.setFecha((request.getParameter("txtDOCUMENTO_FECHA_" + i) == null) ? "" : formatoFechas.getParseAString(request.getParameter("txtDOCUMENTO_FECHA_" + i)));
                    if (documento.getId().isEmpty() == false)
                    { 
                        listaDocumentos.add(documento);
                    } 
                } 
            } 
        }
        catch (Exception e)
        {
            throw new Exception("Error al intentar leer los datos de los documentos del servidor publico sancionado: " + e.toString());
        }
        return listaDocumentos;
    }

    /**
     * Devuelve un nuevo modelo agregando las nuevas modificaciones realizadas
     * por el usuario y con los valores que ya se encontraban almacenados en la
     * base de datos
     *
     * @param request HttpServletRequest
     * @param servidorPublico Resultado de la b&uacute;squeda del servidor
     * publico
     *
     * @return SSANCIONADOS
     *
     * @throws Exception
     */
    @Override
    public SSANCIONADOS modelarRequestServidorPublicosSancionado(HttpServletRequest request, SSANCIONADOS servidorPublico) throws Exception
    {
        INSTITUCION_DEPENDENCIA institucionDependencia;                                     
        SERVIDOR_PUBLICO_SANCIONADO servidorPublicoSancionado;                              
        PUESTO puesto;                                                                      
        RESOLUCION_SER resolucion;                                                          
        MULTA_SSANCIONADOS multa;                                                           
        INHABILITACION inhabilitacion;                                                      
        Date dateInicial;                                                                   
        Date dateFinal;                                                                     
        ManejoDeFechas formatoFechas;                                                       
        String fechaInicial;                                                                
        String fechaFinal;                                                                  
        UNIDAD_MEDIDA_PLAZO medidaPlazo;                                                    

        formatoFechas = new ManejoDeFechas("yyyy-MM-dd");
        institucionDependencia = new INSTITUCION_DEPENDENCIA();
        servidorPublicoSancionado = new SERVIDOR_PUBLICO_SANCIONADO();
        resolucion = new RESOLUCION_SER();
        multa = new MULTA_SSANCIONADOS();
        inhabilitacion = new INHABILITACION();

        servidorPublico.setFechaCaptura(new ManejoDeFechas().getDateNowAsString());
        servidorPublico.setExpediente((request.getParameter("txtNUMERO_EXPEDIENTE") == null || request.getParameter("txtNUMERO_EXPEDIENTE").isEmpty()) ? null : request.getParameter("txtNUMERO_EXPEDIENTE"));

        institucionDependencia = DAOSanciones.obtenerEntePublico(request.getParameter("cmbDEPENDENCIA"));

        servidorPublico.setInstitucionDependencia(institucionDependencia);

        servidorPublicoSancionado.setNombres(request.getParameter("txtNOMBRES"));
        servidorPublicoSancionado.setPrimerApellido(request.getParameter("txtPRIMER_APELLIDO"));
        servidorPublicoSancionado.setSegundoApellido((request.getParameter("txtSEGUNDO_APELLIDO") == null || request.getParameter("txtSEGUNDO_APELLIDO").isEmpty()) ? null : request.getParameter("txtSEGUNDO_APELLIDO"));

        servidorPublicoSancionado.setRfc(
                (request.getParameter("txtRFC") != null && !request.getParameter("txtRFC").isEmpty())
                ? (ExpresionesRegulares.validarRfcPF(request.getParameter("txtRFC")))
                ? request.getParameter("txtRFC").toUpperCase()
                : ""
                : ""
        );
        servidorPublicoSancionado.setCurp(
                (request.getParameter("txtCURP") != null && !request.getParameter("txtCURP").isEmpty())
                ? (ExpresionesRegulares.validarCurp(request.getParameter("txtCURP")))
                ? request.getParameter("txtCURP").toUpperCase()
                : ""
                : ""
        );

        if (servidorPublicoSancionado.getRfc() == null || servidorPublicoSancionado.getRfc().isEmpty())
        { 
            servidorPublicoSancionado.setRfc(ObtenerRfc.obntenerRfcSPDN(servidorPublicoSancionado.getNombres(), servidorPublicoSancionado.getPrimerApellido(), servidorPublicoSancionado.getSegundoApellido()).toLowerCase());
        } 
        if (servidorPublicoSancionado.getCurp() == null || servidorPublicoSancionado.getCurp().isEmpty())
        { 
            servidorPublicoSancionado.setCurp(ObtenerRfc.obntenerRfcSPDN(servidorPublicoSancionado.getNombres(), servidorPublicoSancionado.getPrimerApellido(), servidorPublicoSancionado.getSegundoApellido()).toLowerCase());
        } 

        servidorPublicoSancionado.setGenero((request.getParameter("radGENERO") == null || request.getParameter("radGENERO").isEmpty()) ? new GENERO() : DAOSanciones.obtenerGenero(request.getParameter("radGENERO")));

        puesto = DAOSanciones.obtenerPuesto(request.getParameter("cmbPUESTO"));
        servidorPublicoSancionado.setPuestoCompleto(puesto);
        servidorPublicoSancionado.setPuesto(puesto.getFuncional());
        servidorPublicoSancionado.setNivel(puesto.getNivel());

        servidorPublico.setServidorPublicoSancionado(servidorPublicoSancionado);

        servidorPublico.setAutoridadSancionadora((request.getParameter("txtAUTORIDAD_SANCIONADORA") == null || request.getParameter("txtAUTORIDAD_SANCIONADORA").isEmpty()) ? null : DAOSanciones.obtenerOrganoInternoDeControl(request.getParameter("txtAUTORIDAD_SANCIONADORA")).getValor());

        servidorPublico.setTipoFalta((request.getParameter("cmbTIPO_FALTAS") == null || request.getParameter("cmbTIPO_FALTAS").isEmpty()) ? new TIPO_FALTA() : DAOSanciones.obtenerTipoDeFalta(request.getParameter("cmbTIPO_FALTAS")));
        servidorPublico.getTipoFalta().setDescripcion((request.getParameter("txtTIPO_FALTAS_DESCRIPCION") == null || request.getParameter("txtTIPO_FALTAS_DESCRIPCION").isEmpty()) ? null : request.getParameter("txtTIPO_FALTAS_DESCRIPCION"));

        if (request.getParameterValues("chckTIPO_SANCION") == null || request.getParameterValues("chckTIPO_SANCION").length == 0)
        { 
            servidorPublico.setTipoSancion(new ArrayList<>());
        } 
        else
        { 
            servidorPublico.setTipoSancion(this.obtenerTiposDeSancion(request));
        } 

        servidorPublico.setCausaMotivoHechos(request.getParameter("txtCAUSA_MOTIVO"));

        resolucion.setUrl((request.getParameter("txtURL_RESOLUCION") == null || request.getParameter("txtURL_RESOLUCION").isEmpty()) ? null : request.getParameter("txtURL_RESOLUCION"));

        resolucion.setFechaResolucion((request.getParameter("txtFECHA_RESOLUCION") == null || request.getParameter("txtFECHA_RESOLUCION").isEmpty()) ? null : formatoFechas.getParseAString(request.getParameter("txtFECHA_RESOLUCION")));
        servidorPublico.setResolucion(resolucion);

        multa.setMonto((request.getParameter("txtMULTA_MONTO") == null || request.getParameter("txtMULTA_MONTO").isEmpty()) ? null : Double.parseDouble(request.getParameter("txtMULTA_MONTO")));

        multa.setMoneda((request.getParameter("cmbMONEDA") == null || request.getParameter("cmbMONEDA").isEmpty()) ? new MONEDA() : DAOSanciones.obtenerMoneda(request.getParameter("cmbMONEDA")));
        servidorPublico.setMulta(multa);

        if ((request.getParameter("txtPLAZO") == null || request.getParameter("txtPLAZO").isEmpty()) || (request.getParameter("cmbUNIDAD_MEDIDA_PLAZO") == null || request.getParameter("cmbUNIDAD_MEDIDA_PLAZO").isEmpty()))
        { 
            inhabilitacion.setPlazo(null);
        } 
        else
        { 
            medidaPlazo = DAOSanciones.obtenerUnidadMedidaPlazo(request.getParameter("cmbUNIDAD_MEDIDA_PLAZO"));
            String plazo = (request.getParameter("txtPLAZO") != null)
                    ? request.getParameter("txtPLAZO").concat(" " + medidaPlazo.getValor())
                    : null;
            inhabilitacion.setPlazo((request.getParameter("txtPLAZO") == null || request.getParameter("txtPLAZO").isEmpty()) ? null : plazo);
        } 

        if ((request.getParameter("txtFECHA_INICIAL") == null || request.getParameter("txtFECHA_INICIAL").isEmpty()) || (request.getParameter("txtFECHA_FINAL") == null || request.getParameter("txtFECHA_FINAL").isEmpty()))
        { 
            inhabilitacion.setFechaInicial(null);
            inhabilitacion.setFechaFinal(null);
        } 
        else
        { 
            dateInicial = formatoFechas.getParse((request.getParameter("txtFECHA_INICIAL") != null) ? request.getParameter("txtFECHA_INICIAL") : null);
            dateFinal = formatoFechas.getParse((request.getParameter("txtFECHA_FINAL") != null) ? request.getParameter("txtFECHA_FINAL") : null);

            if (dateInicial.compareTo(dateFinal) == 0 || dateInicial.compareTo(dateFinal) == -1)
            { 
                fechaInicial = formatoFechas.getFormat(dateInicial);
                fechaFinal = formatoFechas.getFormat(dateFinal);
                inhabilitacion.setFechaInicial(fechaInicial);
                inhabilitacion.setFechaFinal(fechaFinal);
            } 
            else
            { 
                throw new Exception("Error: La fecha inicial de inhabilitaron debe ser mayor que la fecha final!");
            } 
        } 
        servidorPublico.setInhabilitacion(inhabilitacion);

        servidorPublico.setObservaciones((request.getParameter("txtOBSERVACIONES") == null || request.getParameter("txtOBSERVACIONES").isEmpty()) ? null : request.getParameter("txtOBSERVACIONES"));

        if (request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar") == null)
        {
            servidorPublico.setDocumentos((request.getParameter("txtDOCUMENTO_NUM") == null || request.getParameter("txtDOCUMENTO_NUM").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }
        else
        {
            servidorPublico.setDocumentos((request.getParameter("listaDeIdentificadoresDeDocumentosARegistrar").isEmpty()) ? new ArrayList<>() : this.obtenerLaCantidadDeValoresCadaDocumento(request));
        }
        return servidorPublico;
    }

    /**
     * Registra los cambios realizados a un servidor p&uacute;blico usando como
     * filtro el nivel del usuario y el id del registro
     *
     * @param id Id del registro que se va a editar
     * @param servidorPublico Objeto que contiene los valores del servidor
     * publico
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Nivel que es otorgado a cada usuario
     *
     * @return boolean
     */
    @Override
    public String editarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       

        mensaje = new Document();
        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 

            if (servidorPublico.getMetadatos() != null)
            { 
                servidorPublico.getMetadatos().setUsuario_modificacion(usuario.getUsuario());
                servidorPublico.getMetadatos().setFecha_modificacion(new ManejoDeFechas().getDateNowAsString());
            } 
            else
            { 
                METADATOS metadatos = new METADATOS();
                metadatos.setUsuario_modificacion(usuario.getUsuario());
                metadatos.setFecha_modificacion(new ManejoDeFechas().getDateNowAsString());
                servidorPublico.setMetadatos(metadatos);
            } 

            if (servidorPublico.getMetadata() != null)
            { 
                servidorPublico.getMetadata().setActualizacion(new ManejoDeFechas().getDateNowAsString());
                servidorPublico.getMetadata().setInstitucion(usuario.getDependencia().getValor());
                servidorPublico.getMetadata().setContacto(usuario.getCorreo_electronico());
                servidorPublico.getMetadata().setPersonaContacto(usuario.getNombreCompleto());
            } 
            else
            { 
                METADATA metadata = new METADATA();
                metadata.setActualizacion(new ManejoDeFechas().getDateNowAsString());
                metadata.setInstitucion(usuario.getDependencia().getValor());
                metadata.setContacto(usuario.getCorreo_electronico());
                metadata.setPersonaContacto(usuario.getNombreCompleto());
                servidorPublico.setMetadata(metadata);
            } 

            servidorPublico.setPublicar("0");

            if (DAOSanciones.editarServidorPublicoSancionado(id, servidorPublico, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha modificado correctamente el registro");
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible modificar el registro!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\\u00F3n!");
        } 

        return mensaje.toJson();
    }

    /**
     * Agrega a la base de datos el registro del servidor p&uacute;blico
     * sancionado
     *
     * @param servidorPublico datos del servidor p&uacute;blico sancionado
     * @param usuario usuario en sesi&oacute;n
     * @param nivelAcceso permisos para el usuario dentro del sistema
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    @Override
    public String registrarServidorPublicoSancionado(SSANCIONADOS servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        Document mensaje;                                                       
        METADATA metadata;                                                      
        METADATOS metadatos;                                                    

        metadata = new METADATA();
        metadatos = new METADATOS();
        mensaje = new Document();

        if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
        { 
            metadata.setActualizacion(new ManejoDeFechas().getDateNowAsString());
            metadata.setInstitucion(usuario.getDependencia().getValor());
            metadata.setContacto(usuario.getCorreo_electronico());
            metadata.setPersonaContacto(usuario.getNombreCompleto());
            servidorPublico.setMetadata(metadata);

            servidorPublico.setId(UUID.randomUUID().toString());
            servidorPublico.setFechaCaptura(new ManejoDeFechas().getDateNowAsString());

            servidorPublico.setDependencia("SAEMM");
            metadatos.setUsuario_registro(usuario.getUsuario());
            metadatos.setFecha_registro(new ManejoDeFechas().getDateNowAsString());
            servidorPublico.setMetadatos(metadatos);
            servidorPublico.setPublicar("0");

            if (DAOSanciones.registrarServidorPublicoSancionado(servidorPublico, usuario, nivelAcceso))
            {
                mensaje.append("tipo", "success");
                mensaje.append("mensaje", "Se ha registrado correctamente al servidor p\u00FAblico: " + servidorPublico.getServidorPublicoSancionado().getNombres() + " " + servidorPublico.getServidorPublicoSancionado().getPrimerApellido() + (servidorPublico.getServidorPublicoSancionado().getSegundoApellido() == null ? "" : " " + servidorPublico.getServidorPublicoSancionado().getSegundoApellido()));
            }
            else
            {
                mensaje.append("tipo", "error");
                mensaje.append("mensaje", "No fue posible registrar los datos del servidor p\u00FAblico, intente de nuevo!");
            }
        } 
        else
        { 
            mensaje.append("tipo", "error");
            mensaje.append("mensaje", "No cuentas con los permisos necesarios para realizar esta acci\u00F3n!");
        } 
        return mensaje.toJson();
    }

    /**
     * Devuelve el listado de las unidades de medida para el plazo de
     * inhabilitaci&oacute;n
     *
     * @return ArrayList UNIDAD_MEDIDA_PLAZO
     *
     * @throws Exception
     */
    @Override
    public ArrayList<UNIDAD_MEDIDA_PLAZO> obtenerListaDeUnidadMedidaPlazo() throws Exception
    {
        return DAOSanciones.obtenerListaDeUnidadMedidaPlazo();
    }

    /**
     * Devuelve el listado de los &oacute;rgano internos de control de la base
     * de datos
     *
     * @return ArrayList ORGANO_INTERNO_CONTROL
     *
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<ORGANO_INTERNO_CONTROL> obtenerListaDeOrganoInternoDeControl() throws Exception
    {
        return DAOSanciones.obtenerListaDeOrganoInternoDeControl();
    }

    /**
     * Devuelve el valor correspondiente al &oacute;rgano interno de control por
     * el filtro de id
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ORGANO_INTERNO_CONTROL obtenerOrganoInternoDeControl(String parameter) throws Exception
    {
        return DAOSanciones.obtenerOrganoInternoDeControl(parameter);
    }

    /**
     *
     * @param servidorPublico
     */
    private void validarCamposDeServidoresPublicosSancionados(SSANCIONADOS servidorPublico)
    {
    }

    /**
     * Devuelve el tipo de persona que es particular sancionado
     *
     * @param tipoPersona clave del tipo de persona
     *
     * @return TIPO_PERSONA
     *
     * @throws Exception
     */
    @Override
    public TIPO_PERSONA obtenerTipoPersona(String tipoPersona) throws Exception
    {
        return DAOSanciones.obtenerTipoPersona(tipoPersona);
    }

    /**
     * Devuelve el listado de los tipos de personas registrados en la base de
     * datos
     *
     * @return ArrayList TIPO_PERSONA
     *
     * @throws Exception
     */
    @Override
    public ArrayList<TIPO_PERSONA> obtenerListaTipoPersona() throws Exception
    {
        return DAOSanciones.obtenerListaTipoPersona();
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    @Override
    public ArrayList<MUNICIPIO_PDE> obtenerListaDeMunicipiosporEstado(String parameter) throws Exception
    {
        return DAOSanciones.obtenerListaDeMunicipiosporEstado(parameter);
    }

    /**
     *
     * @return
     */
    @Override
    public ArrayList<PAIS> obtenerListaDePaises() throws Exception
    {
        return DAOSanciones.obtenerListaDePaises();
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ASENTAMIENTO> obtenerAsentamiento(String parameter) throws Exception
    {
        return DAOSanciones.obtenerAsentamiento(parameter);
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ArrayList<ASENTAMIENTO> obtenerAsentamientoPorCP(String parameter) throws Exception
    {
        return DAOSanciones.obtenerAsentamientoPorCP(parameter);
    }

    /**
     *
     * @param parameter
     *
     * @return
     */
    @Override
    public ArrayList<ENTIDAD_FEDERATIVA_PDE> obtenerListaDeEntidadesFederativa(String parameter) throws Exception
    {
        return DAOSanciones.obtenerListaDeEntidadesFederativas(parameter);
    }

    /**
     *
     * @return
     */
    @Override
    public ArrayList<TIPO_DOCUMENTO> obtenerTiposDeDocumentos() throws Exception
    {
        return DAOSanciones.obtenerTiposDeDocumentos();
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    public ArrayList<TIPO_FALTA> obtenerListaTiposDeFaltaPsancionados() throws Exception
    {
        return DAOSanciones.obtenerListaTiposDeFaltaPsancionados();
    }

}
