/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import org.bson.Document;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.javabeans.SISTEMA;
import tol.sesaemm.javabeans.SISTEMAS;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class DAOAdministrador
{

    /**
     * Verificar usuario y contraseña para inicio de sesi&oacute;n
     *
     * @param usuario Datos del usuario que inicia sesi&oacute;n
     *
     * @return Datos completos del usuario
     *
     * @throws Exception
     */
    public static USUARIO loguearUsuario(USUARIO usuario) throws Exception
    {
        USUARIO usuarioLogeado = null;              
        MongoClient conectarBaseDatos = null;  
        MongoCursor<Document> cursor = null;            
        MongoDatabase database;                     
        MongoCollection<Document> collection;       
        FindIterable<Document> aIUsuario;            
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = (MongoClient) DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("usuarios");
                Document match = new Document();

                match.append("usuario", usuario.getUsuario());
                match.append("password", usuario.getPassword());
                match.append("activo", 1);

                aIUsuario = collection.find(match);
                cursor = aIUsuario.iterator();

                Gson gson = new Gson();
                if (cursor.hasNext())
                {
                    usuarioLogeado = new USUARIO();
                    String jsonUsuarios = cursor.next().toJson();
                    usuarioLogeado = gson.fromJson(jsonUsuarios, USUARIO.class);
                }

            } 
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener sesi&oacute;n: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return usuarioLogeado;
    
    }

    /**
     * Obtiene la lista de sistemas asignados al usuario
     *
     * @param sistemasUsuario Lista de los sistemas asignados
     *
     * @return ArrayList
     *
     * @throws Exception
     */
    public static ArrayList<SISTEMAS> obtenerDatosActualizadosDeSistemas(ArrayList<SISTEMAS> sistemasUsuario) throws Exception
    {
        MongoClient conectarBaseDatos = null;       
        MongoDatabase database;              
        MongoCollection<Document> collection;
        FindIterable<Document> aIPuesto;    
        ArrayList<SISTEMAS> listadoSistemas;
        SISTEMAS sistema = new SISTEMAS();
        MongoCursor<Document> cursor = null;  
        Gson gson;
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("sistemas");

                for (SISTEMAS sistemaUsuario : sistemasUsuario)
                {
                    aIPuesto = collection.find(new Document("$and", Arrays.asList(new Document("id", sistemaUsuario.getSistema().getId()), new Document("activo", 1))));
                    cursor = aIPuesto.iterator();

                    gson = new Gson();
                    listadoSistemas = new ArrayList<>();

                    while (cursor.hasNext())
                    { 
                        String jsonProcedimientos = cursor.next().toJson();
                        sistemaUsuario.setSistema(gson.fromJson(jsonProcedimientos, SISTEMA.class));
                    } 
                    
                }

            }
            else
            {
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener estatus de sistemas:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return sistemasUsuario;

    }

    /**
     * Obtiene la lista de sistemas
     *
     * @return ArrayList Lista de los sistemas
     *
     * @throws Exception
     */
    public static ArrayList<SISTEMA> obtenerSistemas() throws Exception
    {
        MongoClient conectarBaseDatos = null;       
        MongoDatabase database;              
        MongoCollection<Document> collection;
        FindIterable<Document> iSistema;   
        ArrayList<SISTEMA> listadoSistemas;
        SISTEMA sistema = new SISTEMA(); 
        MongoCursor<Document> cursor = null;    
        Gson gson;         
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("sistemas");

                iSistema = collection.find(new Document("id", new Document("$ne", 9))).sort(new Document("id", 1));
                cursor = iSistema.iterator();

                gson = new Gson();
                listadoSistemas = new ArrayList<>();

                while (cursor.hasNext())
                {
                    sistema = gson.fromJson(cursor.next().toJson(), SISTEMA.class);
                    listadoSistemas.add(sistema);
                }

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener sistemas:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return listadoSistemas;
    }

    /**
     *
     * @param usuario
     *
     * @return
     *
     * @throws Exception
     */
    public static boolean registrarAcceso(String usuario) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                          
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                  
        Date dateHoy;                                                          
        SimpleDateFormat formato;                                              
        boolean seRegistra;
        dateHoy = new Date();
        formato = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        seRegistra = false;
        ConfVariables confVariables;

        confVariables = new ConfVariables();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("log_inicio");

                collection.insertOne(new Document("fecha_ingreso", formato.format(dateHoy))
                        .append("usuario", usuario));
                seRegistra = true;

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al iniciar sesión : " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seRegistra;

    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { 

        if (cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 

        if (conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } 

    }

}
