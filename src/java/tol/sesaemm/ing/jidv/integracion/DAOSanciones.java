/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Collation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bson.Document;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;
import tol.sesaemm.annotations.Exclude;
import tol.sesaemm.funciones.BitacoraPde;
import tol.sesaemm.funciones.ExpresionesRegulares;
import tol.sesaemm.funciones.ManejoDeFechas;
import tol.sesaemm.funciones.TBITACORAS_PDE;
import tol.sesaemm.ing.jidv.config.ConfVariables;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.PAGINACION;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_SSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.ServidorPublicoSancionado;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.SsancionadosConPaginacion;
import tol.sesaemm.javabeans.*;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_SANCION_SER;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class DAOSanciones
{

    private static final Collation collation = Collation.builder().locale("es").caseLevel(true).build();

    /**
     * Devuelve la lista de los servidores publico sancionados
     *
     * @param usuario Informaci&oacute;n del usuario
     * @param nivelAcceso
     * @param dependencias Dependencias asignadas al usuario
     * @param filtroBusqueda filtro de b&uacute;squeda
     *
     * @see
     * https://mongodb.github.io/mongo-java-driver/4.0/apidocs/bson/org/bson/json/JsonWriterSettings.html
     *
     * @return ArrayList Object con la paginaci&oacute;n y la lista de los
     * servidores sancionado
     *
     * @throws Exception
     */
    public static SsancionadosConPaginacion obtenerServidoresPublicosSancionados(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependencias, FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        AggregateIterable<Document> aIServidoresPublicos;                       
        ArrayList<ServidorPublicoSancionado> servidoresPublicos;                
        MongoCursor<Document> cursor = null;                                    
        List<Document> where_dependencia;                                       
        Document or;                                                            
        Document where;                                                         
        Document order_by;                                                      
        Gson gson;                                                              
        PAGINACION filtroPaginacion;                                            
        int totalRegFiltro = 0;                                                 
        int totalPaginas = 0;                                                   
        int saltos = 0;                                                         
        SsancionadosConPaginacion listaDeObjetos;                               
        ArrayList<Document> query;                                              
        SSANCIONADOS servidorPublico;   
        ConfVariables confVariables = new ConfVariables();                                        

        gson = new Gson();
        filtroPaginacion = new PAGINACION();
        listaDeObjetos = new SsancionadosConPaginacion();
        query = new ArrayList<>();
        servidoresPublicos = new ArrayList<>();
        where_dependencia = new ArrayList<>();
        or = new Document();
        where = new Document();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ssancionados");

                order_by = new Document();
                if (filtroBusqueda.getOrden().equals("nombres"))
                { 
                    order_by.append("nombreCompleto", 1);
                } 
                else if (filtroBusqueda.getOrden().equals("dependencia"))
                { 
                    order_by.append("institucionDependenciaNombre", 1);
                } 
                else if (filtroBusqueda.getOrden().equals("puesto"))
                { 
                    order_by.append("cargo", 1);
                } 

                if (nivelAcceso.getId() == 2 || nivelAcceso.getId() == 3)
                { 
                    for (ENTE_PUBLICO dependencia : dependencias)
                    { 
                        where_dependencia.add(new Document("institucionDependenciaCodigo", dependencia.getClave()));
                    } 
                    or.append("$or", where_dependencia);
                } 

                if (nivelAcceso.getId() == 2)
                { 
                    where.append("usuarioRegistro", new Document("$eq", usuario.getUsuario()));
                } 
                else if (nivelAcceso.getId() == 3)
                { 
                    where.append("publicar", new Document("$ne", "0"));
                } 
                else if (nivelAcceso.getId() == 5)
                { 
                    where.append("publicar", new Document("$ne", "0"));
                } 

                boolean tieneCondicion = false;
                if (filtroBusqueda.getNOMBRES() != null && !filtroBusqueda.getNOMBRES().isEmpty())
                { 
                    where.append("servidorPublicoSancionado.nombres",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getNOMBRES())).append("$options", "i"));
                    tieneCondicion = true;
                } 
                if (filtroBusqueda.getPRIMER_APELLIDO() != null && !filtroBusqueda.getPRIMER_APELLIDO().isEmpty())
                { 
                    where.append("servidorPublicoSancionado.primerApellido",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getPRIMER_APELLIDO())).append("$options", "i"));
                    tieneCondicion = true;
                } 
                if (filtroBusqueda.getINSTITUCION_DEPENDENCIA() != null && !filtroBusqueda.getINSTITUCION_DEPENDENCIA().isEmpty())
                { 
                    where.append("institucionDependenciaNombre",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(filtroBusqueda.getINSTITUCION_DEPENDENCIA())).append("$options", "i"));
                    tieneCondicion = true;
                } 

                //<editor-fold defaultstate="collapsed" desc="Consulta">
                query.add(
                        new Document("$addFields",
                                new Document("institucionDependenciaNombre",
                                        new Document("$cond",
                                                Arrays.asList(
                                                        new Document(
                                                                "$or",
                                                                Arrays.asList(
                                                                        new Document("$eq",
                                                                                Arrays.asList("$institucionDependencia.nombre", "")
                                                                        ),
                                                                        new Document("$eq",
                                                                                Arrays.asList("$institucionDependencia.nombre", null)
                                                                        )
                                                                )
                                                        ),
                                                        "$institucionDependencia.siglas",
                                                        "$institucionDependencia.nombre"
                                                )
                                        )
                                )
                                        .append("nombreCompleto",
                                                new Document("$trim",
                                                        new Document("input",
                                                                new Document("$concat",
                                                                        Arrays.asList(
                                                                                new Document("$cond",
                                                                                        Arrays.asList(
                                                                                                new Document(
                                                                                                        "$or",
                                                                                                        Arrays.asList(
                                                                                                                new Document("$eq",
                                                                                                                        Arrays.asList("$servidorPublicoSancionado.nombres", "")
                                                                                                                ),
                                                                                                                new Document("$eq",
                                                                                                                        Arrays.asList("$servidorPublicoSancionado.nombres", null)
                                                                                                                )
                                                                                                        )
                                                                                                ),
                                                                                                "",
                                                                                                "$servidorPublicoSancionado.nombres"
                                                                                        )
                                                                                ),
                                                                                " ",
                                                                                new Document("$cond",
                                                                                        Arrays.asList(
                                                                                                new Document(
                                                                                                        "$or",
                                                                                                        Arrays.asList(
                                                                                                                new Document("$eq",
                                                                                                                        Arrays.asList("$servidorPublicoSancionado.primerApellido", "")
                                                                                                                ),
                                                                                                                new Document("$eq",
                                                                                                                        Arrays.asList("$servidorPublicoSancionado.primerApellido", null)
                                                                                                                )
                                                                                                        )
                                                                                                ),
                                                                                                "",
                                                                                                "$servidorPublicoSancionado.primerApellido"
                                                                                        )
                                                                                ),
                                                                                " ",
                                                                                new Document("$cond",
                                                                                        Arrays.asList(
                                                                                                new Document(
                                                                                                        "$or",
                                                                                                        Arrays.asList(
                                                                                                                new Document("$eq",
                                                                                                                        Arrays.asList("$servidorPublicoSancionado.segundoApellido", "")
                                                                                                                ),
                                                                                                                new Document("$eq",
                                                                                                                        Arrays.asList("$servidorPublicoSancionado.segundoApellido", null)
                                                                                                                )
                                                                                                        )
                                                                                                ),
                                                                                                "",
                                                                                                "$servidorPublicoSancionado.segundoApellido"
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                        .append("cargo", "$servidorPublicoSancionado.puesto")
                                        .append("institucionDependenciaCodigo", "$institucionDependencia.clave")
                                        .append("usuarioRegistro", "$metadatos.usuario_registro")
                        )
                );
                //</editor-fold>

                query.add(new Document("$match", new Document("$and", Arrays.asList(where, or))));
                
                query.add(new Document("$count", "totalCount"));

                aIServidoresPublicos = collection.aggregate(query).collation(collation).allowDiskUse(true);
                cursor = aIServidoresPublicos.iterator();
                if (cursor.hasNext())
                { 
                    totalRegFiltro = cursor.next().get("totalCount", Integer.class);
                    totalPaginas = (int) Math.ceil((float) totalRegFiltro / filtroBusqueda.getRegistrosMostrar());

                    if (filtroBusqueda.getNumeroPagina() > 1)
                    { 
                        saltos = (filtroBusqueda.getNumeroPagina() - 1) * filtroBusqueda.getRegistrosMostrar();
                    } 

                    filtroPaginacion.setRegistrosMostrar(filtroBusqueda.getRegistrosMostrar());
                    filtroPaginacion.setNumeroPagina(filtroBusqueda.getNumeroPagina());
                    filtroPaginacion.setTotalPaginas((totalPaginas < 1) ? 1 : totalPaginas);
                    filtroPaginacion.setNumeroSaltos(saltos);
                    filtroPaginacion.setTotalRegistros(totalRegFiltro);
                    filtroPaginacion.setIsEmpty(false);
                } 
                query.remove(2);

                query.add(new Document("$sort",
                        order_by
                ));

                if (!filtroPaginacion.isEmpty())
                { 
                    query.add(new Document("$skip", filtroPaginacion.getNumeroSaltos()));
                    query.add(new Document("$limit", filtroPaginacion.getRegistrosMostrar()));
                } 

                servidoresPublicos = collection.aggregate(query, ServidorPublicoSancionado.class).collation(collation).allowDiskUse(true).into(new ArrayList<>());

                listaDeObjetos.setPaginacion(filtroPaginacion);
                listaDeObjetos.setServidores(servidoresPublicos);

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener datos del servidor p&uacute;blico sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return listaDeObjetos;
    }

    /**
     * Devuelve el detalle de un servidor sancionado por su id
     *
     * @param idServidorPublico identificado del servidor p&uacute;blico
     *
     * @see
     * https://mongodb.github.io/mongo-java-driver/4.0/apidocs/bson/org/bson/json/JsonWriterSettings.html
     *
     * @return SSANCIONADOS
     *
     * @throws Exception
     */
    public static SSANCIONADOS obtenerDetalleServidorPublicoSancionado(String idServidorPublico) throws Exception
    {
        SSANCIONADOS ssancionados;                                              
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<SSANCIONADOS> collection;                               
        ArrayList<Document> query;
        ConfVariables confVariables = new ConfVariables(); 

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();
            ssancionados = new SSANCIONADOS();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ssancionados", SSANCIONADOS.class);

                query = new ArrayList<>();

                query.add(new Document("$match", new Document("_id", new ObjectId(idServidorPublico))));
                ssancionados = collection.aggregate(query).collation(collation).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener datos del servidor público sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return ssancionados;
    }

    /**
     * Devuelve el modelo de SSANCIONADOS con los campos que son publico o
     * privados de la colecci&oacute;n de servidores sancionados
     *
     * @param coleccion
     * @see
     * https://mongodb.github.io/mongo-java-driver/4.0/apidocs/bson/org/bson/json/JsonWriterSettings.html
     *
     * @return SSANCIONADOS con campos publico y privados
     *
     * @throws Exception
     */
    public static SSANCIONADOS obtenerEstatusDePrivacidadCamposSSANCIONADOS(String coleccion) throws Exception
    {
        SSANCIONADOS servidorpublico;                                           
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;    
        ConfVariables confVariables = new ConfVariables();                                

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();
            servidorpublico = new SSANCIONADOS();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("campos_publicos");
                Document match = new Document();

                match.append("coleccion", coleccion);   

                servidorpublico = collection.find(match, SSANCIONADOS.class).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener privacidad de campos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        return servidorpublico;
    }

    /**
     * Devuelve la lista de puestos registrado en la base de datos
     *
     * @return ArrayList PUESTO
     *
     * @throws Exception
     */
    public static ArrayList<PUESTO> obtenerListaDePuestos() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> aIPuesto;                                        
        ArrayList<PUESTO> listadoPuestos;                                       
        MongoCursor<Document> cursor = null;                                    
        PUESTO puesto;
        JsonWriterSettings settings;                                            
        Gson gson;
        ConfVariables confVariables = new ConfVariables(); 

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto");

                aIPuesto = collection.find().sort(new Document("funcional", 1));
                cursor = aIPuesto.iterator();

                listadoPuestos = new ArrayList<>();
                gson = new Gson();
                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                while (cursor.hasNext())
                { 
                    Document next = cursor.next();
                    puesto = gson.fromJson(next.toJson(settings), PUESTO.class);
                    puesto.setNombre(puesto.getFuncional().trim());
                    puesto.setId(next.get("_id", ObjectId.class));
                    listadoPuestos.add(puesto);
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener puestos funcionales:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return listadoPuestos;
    }

    /**
     * Devuelve la lista de g&eacute;neros almacenados en la base de datos
     *
     * @return ArrayList GENERO
     * @throws java.lang.Exception
     */
    public static ArrayList<GENERO> obtenerListaDeGeneros() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<GENERO> collection;                                     
        ArrayList<GENERO> listadoGeneros;     
        ConfVariables confVariables = new ConfVariables();                                   

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("genero", GENERO.class);

                listadoGeneros = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener generos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoGeneros;
    }

    /**
     * Obtener listado de entes p&uacute;blicos asignados al usuario, si es
     * administrador obtendrá toda la lista
     *
     * @param usuario Usuario en sesi&oacute;n
     *
     * @return Lista de entes p&uacute;blicos asignados al usuario
     *
     * @throws Exception
     */
    public static ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(USUARIO usuario) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        FindIterable<Document> findIterable;                                    
        ArrayList<ENTE_PUBLICO> listadoDependencias;                            
        MongoCursor<Document> cursor = null;                                    
        JsonWriterSettings settings;                                            
        Gson gson;
        ENTE_PUBLICO ente_publico;
        ConfVariables confVariables = new ConfVariables(); 

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ente_publico");
                List<Document> where = new ArrayList<>();

                Document or = new Document();

                for (SISTEMAS sistema : usuario.getSistemas())
                { 
                    if (sistema.getSistema().getId() == 3)
                    { 
                        if (sistema.getNivel_acceso().getId() != 1)
                        { 
                            for (ENTE_PUBLICO itemDependencia : usuario.getDependencias())
                            { 
                                where.add(new Document("clave", itemDependencia.getClave()));
                            } 
                            or.append("$or", where);
                        } 
                    } 
                } 

                findIterable = collection.find(or).sort(new Document("valor", 1));
                cursor = findIterable.iterator();

                listadoDependencias = new ArrayList<>();
                gson = new Gson();
                settings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build();

                while (cursor.hasNext())
                { 
                    Document next = cursor.next();
                    ente_publico = gson.fromJson(next.toJson(settings), ENTE_PUBLICO.class);
                    ente_publico.setNombre(ente_publico.getValor());
                    ente_publico.setId(next.get("_id", ObjectId.class));
                    listadoDependencias.add(ente_publico);
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener entes p&uacute;blicos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return listadoDependencias;
    }

    /**
     * Devuelve la lista de las diferentes faltas registradas en la base de
     * datos
     *
     * @return ArrayList TIPO_FALTA
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_FALTA> obtenerListaTiposDeFalta() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_FALTA> collection;                                 
        ArrayList<TIPO_FALTA> listadoDeFaltas;   
        ConfVariables confVariables = new ConfVariables();                                

        listadoDeFaltas = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_falta", TIPO_FALTA.class);

                listadoDeFaltas = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de faltas:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoDeFaltas;
    }

    /**
     * Devuelve la lista de tipos de sanciones registradas en la base de datos
     *
     * @return ArrayList TIPO_SANCION_SER
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_SANCION_SER> obtenerListaTiposDeSancion() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_SANCION_SER> collection;                           
        ArrayList<TIPO_SANCION_SER> listadoDeSanciones;   
        ConfVariables confVariables = new ConfVariables();                       

        listadoDeSanciones = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_sancion_ser", TIPO_SANCION_SER.class);

                listadoDeSanciones = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de sanci&oacute;n:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoDeSanciones;
    }

    /**
     * Devuelve la lista de los tipos de monedas registrados en la base de datos
     *
     * @return ArrayList MONEDA
     * @throws java.lang.Exception
     */
    public static ArrayList<MONEDA> obtenerListaDeTiposDeMoneda() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<MONEDA> collection;                                     
        ArrayList<MONEDA> listadoDeMonedas;     
        ConfVariables confVariables = new ConfVariables();                                 

        listadoDeMonedas = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("moneda", MONEDA.class);

                listadoDeMonedas = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de monedas:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }
        return listadoDeMonedas;
    }

    /**
     * Devuelve el ente publico que le pertenece al servidor p&uacute;blicos
     *
     * @param clave
     *
     * @return INSTITUCION_DEPENDENCIA
     *
     * @throws java.lang.Exception
     */
    public static INSTITUCION_DEPENDENCIA obtenerEntePublico(String clave) throws Exception
    {
        INSTITUCION_DEPENDENCIA institucioDependencia;                          
        ENTE_PUBLICO entePublico;                                               
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ENTE_PUBLICO> collection;   
        ConfVariables confVariables = new ConfVariables();                             

        institucioDependencia = new INSTITUCION_DEPENDENCIA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ente_publico", ENTE_PUBLICO.class);

                entePublico = collection.find(new Document("clave", clave)).sort(new Document("valor", 1)).first();

                if (entePublico != null)
                { 
                    institucioDependencia.setClave(entePublico.getClave());
                    institucioDependencia.setNombre(entePublico.getValor());
                    institucioDependencia.setSiglas(entePublico.getSiglas());
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado entes p&uacute;blicos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return institucioDependencia;
    }

    /**
     * Devuelve un genero en especifico usando como filtro de b&uacute;squeda el
     * c&oacute;digo del genero
     *
     * @param parameter c&oacute;digo del genero que se buscara
     *
     * @return GENERO
     *
     * @throws java.lang.Exception
     */
    public static GENERO obtenerGenero(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<GENERO> collection;                                     
        GENERO genero;        
        ConfVariables confVariables = new ConfVariables();                                                   

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("genero", GENERO.class);

                genero = collection.find(new Document("clave", new Document("$eq", parameter))).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el genero de la base de datos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return genero;
    }

    /**
     * Devuelve un puesto en especifico usando como filtro de b&uacute;squeda el
     * valor funcional del puesto
     *
     * @param parameter valor id del puesto que se buscara
     *
     * @return PUESTO
     *
     * @throws Exception
     */
    public static PUESTO obtenerPuesto(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PUESTO> collection;                                     
        FindIterable<Document> aIGenero;                                        
        PUESTO puesto;                                                          
        MongoCursor<Document> cursor = null;                                    
        Document query;
        Gson gson;                                                              
        puesto = new PUESTO();
        gson = new Gson();
        ConfVariables confVariables = new ConfVariables(); 

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto", PUESTO.class);

                query = new Document("_id", new ObjectId(parameter));

                puesto = collection.find(query).collation(collation).first();

                if (puesto == null)
                {
                    puesto = DAOSanciones.obtenerPuestoPorNombre(parameter);
                }

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el puesto de la base de datos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, cursor);
        }

        return puesto;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static PUESTO obtenerPuestoPorNombre(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PUESTO> collection;                                     
        PUESTO puesto;                                                          
        Document query;
        ConfVariables confVariables = new ConfVariables(); 

        puesto = new PUESTO();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("puesto", PUESTO.class);

                query = new Document("funcional", new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(parameter)).append("$options", "i"));

                puesto = collection.find(query).collation(collation).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el puesto de la base de datos:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return puesto;
    }

    /**
     * Devuelve un tipo de falta en especifico usando como filtro de
     * b&uacute;squeda la clave de la falta
     *
     * @param parameter clave de la falta
     *
     * @return TIPO_FALTA
     *
     * @throws Exception
     */
    public static TIPO_FALTA obtenerTipoDeFalta(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_FALTA> collection;                                 
        TIPO_FALTA falta;   
        ConfVariables confVariables = new ConfVariables();                                                     

        falta = new TIPO_FALTA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_falta", TIPO_FALTA.class);

                falta = collection.find(new Document("clave", new Document("$eq", parameter))).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener tipo de falta de la base de datos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return falta;
    }

    /**
     * Devuelve una lista de objetos que contiene los valores de cada
     * sanci&oacute;n aplicada al servidor publico
     *
     * @param codigoTiposSanciones
     *
     * @return ArrayList TIPO_SANCION_SER
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_SANCION_SER> obtenerListaTiposDeSancion(List<String> codigoTiposSanciones) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_SANCION_SER> collection;                           
        ArrayList<TIPO_SANCION_SER> listadodeSanciones;                         
        ArrayList<Document> where;
        ConfVariables confVariables = new ConfVariables(); 

        where = new ArrayList<>();
        listadodeSanciones = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_sancion_ser", TIPO_SANCION_SER.class);
                if (codigoTiposSanciones.size() > 0)
                { 
                    for (String calve : codigoTiposSanciones)
                    { 
                        where.add(new Document().append("clave", calve));
                    } 

                    listadodeSanciones = collection.find(new Document("$or", where)).sort(new Document("valor", 1)).into(new ArrayList<>());
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener los tipos de sanciones del servidor p&uacute;blico sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadodeSanciones;
    }

    /**
     * Devuelve los par&aacute;metros correspondientes de un tipo de moneda
     * usando como filtro la clave de la moneda
     *
     * @param parameter clave del tipo de moneda
     *
     * @return MONEDA
     *
     * @throws Exception
     */
    public static MONEDA obtenerMoneda(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<MONEDA> collection;                                     
        MONEDA moneda;        
        ConfVariables confVariables = new ConfVariables();                                                   

        moneda = new MONEDA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("moneda", MONEDA.class);

                moneda = collection.find(new Document("clave", new Document("$eq", parameter))).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener tipo de moneda de la base de datos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return moneda;
    }

    /**
     * Cambia el valor del campo publicar a 1 para indicar que el registro puede
     * ser visible al p&uacute;blico
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param servidorPublicos Datos del servidor p&uacute;blico
     * @param usuario Usuario de despublicaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean publicarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean blnSePublica;                                                   
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document ssancionados;   
        ConfVariables confVariables = new ConfVariables();                                                

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();
        where = new Document();
        blnSePublica = false;
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ssancionados");

                if (nivelAcceso.getId() == 1)
                { 
                    where.append("_id", new ObjectId(id));
                    blnSePublica = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    blnSePublica = true;
                } 

                if (blnSePublica)
                { 
                    ssancionados = Document.parse(gson.toJson(servidorPublicos));
                    collection.updateOne(where, new Document("$set", ssancionados));
                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.ssancionados, "SAEMM", servidorPublicos.getId(), ssancionados.toJson(), BitacoraPde.publicacion));
                    //</editor-fold>
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al publicar servidor p&uacute;blico sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSePublica;
    }

    /**
     * Cambia el valor del campo publicar a 0 para indicar que el registro no
     * debe ser visible al p&uacute;blico
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param servidorPublicos Datos del servidor p&uacute;blico
     * @param usuario Usuario de despublicaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean desPublicarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean blnSeDesPublica;                                                
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document ssancionados;     
        ConfVariables confVariables = new ConfVariables();                                              

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();
        where = new Document();
        blnSeDesPublica = false;
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ssancionados");

                if (nivelAcceso.getId() == 1)
                { 
                    where.append("_id", new ObjectId(id));
                    blnSeDesPublica = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    blnSeDesPublica = true;
                } 
                if (blnSeDesPublica)
                { 
                    ssancionados = Document.parse(gson.toJson(servidorPublicos));
                    collection.updateOne(where, new Document("$set", ssancionados));
                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.ssancionados, "SAEMM", servidorPublicos.getId(), ssancionados.toJson(), BitacoraPde.despublicacion));
                    //</editor-fold>
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al despublicar servidor p&uacute;blico sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSeDesPublica;
    }

    /**
     * Borra de la base de datos el registro seleccionado por su id
     *
     * @param id Identificador del servidor p&uacute;blico
     * @param servidorPublicos Datos del servidor p&uacute;blico
     * @param usuario Usuario de despublicaci&oacute;n
     * @param nivelAcceso Nivel que tiene el usuario para interactuar en el
     * sistema
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static boolean eliminarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document query;                                                         
        boolean seBorra;                                                        
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document ssancionados;  
        ConfVariables confVariables = new ConfVariables();                                                 

        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };

        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        query = new Document();
        seBorra = false;

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ssancionados");

                if (nivelAcceso.getId() == 1)
                { 
                    query.append("_id", new ObjectId(id));
                    seBorra = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    query.append("_id", new ObjectId(id));
                    query.append("metadatos.usuario_registro", usuario.getUsuario());
                    seBorra = true;
                } 
                if (seBorra)
                { 
                    collection.deleteOne(query);

                    ssancionados = Document.parse(gson.toJson(servidorPublicos));
                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.ssancionados, "SAEMM", servidorPublicos.getId(), ssancionados.toJson(), BitacoraPde.borrado));
                    //</editor-fold>
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar servidor p&uacute;blico sancionado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return seBorra;
    }

    /**
     * Registra los cambios realizados a un servidor p&uacute;blico usando como
     * filtro el nivel del usuario y el id del registro
     *
     * @param id Id del registro que se va a editar
     * @param servidorPublico Objeto que contiene los valores del servidor
     * publico
     * @param usuario Usuario en sesi&oacute;n
     * @param nivelAcceso Nivel que es otorgado a cada usuario
     *
     * @return boolean
     */
    public static boolean editarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        Document where;                                                         
        boolean blnSeEdita;                                                     
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document ssancionados;  
        ConfVariables confVariables = new ConfVariables();                                                 

        blnSeEdita = false;
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };
        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        where = new Document();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ssancionados");

                if (nivelAcceso.getId() == 1)
                { 
                    where.append("_id", new ObjectId(id));
                    blnSeEdita = true;
                } 
                else if (nivelAcceso.getId() == 2)
                { 
                    where.append("_id", new ObjectId(id));
                    where.append("metadatos.usuario_registro", usuario.getUsuario());
                    blnSeEdita = true;
                } 

                if (blnSeEdita)
                { 
                    ssancionados = Document.parse(gson.toJson(servidorPublico));
                    collection.updateOne(where, new Document("$set", ssancionados));
                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.ssancionados, "SAEMM", servidorPublico.getId(), ssancionados.toJson(), BitacoraPde.modificacion));
                    //</editor-fold>
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al modificar servidor p&uacute;blico sanconado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSeEdita;
    }

    /**
     * Agrega a la base de datos el registro del servidor p&uacute;blico
     * sancionado
     *
     * @param servidorPublico datos del servidor p&uacute;blico sancionado
     * @param usuario usuario en sesi&oacute;n
     * @param nivelAcceso permisos para el usuario dentro del sistema
     *
     * @throws java.lang.Exception
     */
    public static boolean registrarServidorPublicoSancionado(SSANCIONADOS servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<Document> collection;                                   
        boolean blnSeRegistra;                                                  
        ExclusionStrategy strategy;                                             
        Gson gson;                                                              
        Document ssancionados;   
        ConfVariables confVariables = new ConfVariables();                                                

        blnSeRegistra = false;
        strategy = new ExclusionStrategy()
        {
            @Override
            public boolean shouldSkipClass(Class<?> clazz)
            {
                return false;
            }

            @Override
            public boolean shouldSkipField(FieldAttributes field)
            {
                return field.getAnnotation(Exclude.class) != null;
            }
        };
        gson = new GsonBuilder()
                .serializeNulls()
                .addSerializationExclusionStrategy(strategy)
                .create();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("ssancionados");

                if (nivelAcceso.getId() == 1 || nivelAcceso.getId() == 2)
                { 
                    ssancionados = Document.parse(gson.toJson(servidorPublico));
                    collection.insertOne(ssancionados);

                    //<editor-fold defaultstate="collapsed" desc="asignar y registrar informacion en la bitacora">
                    BitacoraPde.registrarBitacora(new TBITACORAS_PDE(new ManejoDeFechas().getDateNowAsString(), usuario.getUsuario(), BitacoraPde.ssancionados, "SAEMM", servidorPublico.getId(), ssancionados.toJson(), BitacoraPde.registro));
                    //</editor-fold>

                    blnSeRegistra = true;
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al modificar servidor p&uacute;blico sanconado: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return blnSeRegistra;
    }

    /**
     * Devuelve el listado de las unidades de medida para el plazo de
     * inhabilitaci&oacute;n
     *
     *
     * @return ArrayList UNIDAD_MEDIDA_PLAZO
     *
     * @throws Exception
     */
    public static ArrayList<UNIDAD_MEDIDA_PLAZO> obtenerListaDeUnidadMedidaPlazo() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<UNIDAD_MEDIDA_PLAZO> collection;                        
        ArrayList<UNIDAD_MEDIDA_PLAZO> listaDePlazos; 
        ConfVariables confVariables = new ConfVariables();                           

        listaDePlazos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("unidad_medida_plazo", UNIDAD_MEDIDA_PLAZO.class);

                listaDePlazos = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener las unidades de medida para el plazo: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaDePlazos;
    }

    /**
     * Devuelve la unidad de de medida para el plazo de inhabilitaci&oacute;n
     *
     * @param parameter valor de b&uacute;squeda
     *
     * @return ArrayList UNIDAD_MEDIDA_PLAZO
     *
     * @throws Exception
     */
    public static UNIDAD_MEDIDA_PLAZO obtenerUnidadMedidaPlazo(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<UNIDAD_MEDIDA_PLAZO> collection;                        
        UNIDAD_MEDIDA_PLAZO plazo;     
        ConfVariables confVariables = new ConfVariables();                                          

        plazo = new UNIDAD_MEDIDA_PLAZO();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("unidad_medida_plazo", UNIDAD_MEDIDA_PLAZO.class);
                plazo = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

                if (plazo == null)
                { 
                    plazo = collection.find(new Document("valor",
                            new Document("$regex", ExpresionesRegulares.crearExpresionRegularDePalabra(parameter)).append("$options", "i")
                    )).collation(collation).sort(new Document("valor", 1)).first();
                }

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener la unidad de medida para el plazo: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return plazo;
    }

    /**
     * Devuelve el listado de los &oacute;rgano internos de control de la base
     * de datos
     *
     * @return ArrayList ORGANO_INTERNO_CONTROL
     *
     * @throws java.lang.Exception
     */
    public static ArrayList<ORGANO_INTERNO_CONTROL> obtenerListaDeOrganoInternoDeControl() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ORGANO_INTERNO_CONTROL> collection;                     
        ArrayList<ORGANO_INTERNO_CONTROL> listaDeOrganos;     
        ConfVariables confVariables = new ConfVariables();                   

        listaDeOrganos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("organo_interno_control", ORGANO_INTERNO_CONTROL.class);

                listaDeOrganos = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de los organos internos de control: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaDeOrganos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static ORGANO_INTERNO_CONTROL obtenerOrganoInternoDeControl(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ORGANO_INTERNO_CONTROL> collection;                     
        ORGANO_INTERNO_CONTROL organo;         
        ConfVariables confVariables = new ConfVariables();                                  

        organo = new ORGANO_INTERNO_CONTROL();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("organo_interno_control", ORGANO_INTERNO_CONTROL.class);

                organo = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

                if (organo == null)
                {  
                    organo = collection.find(new Document("valor", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el organo interno de control: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return organo;
    }

    /**
     * Devuelve el tipo de persona que es particular sancionado
     *
     * @param tipoPersona clave del tipo de persona
     *
     * @return TIPO_PERSONA
     *
     * @throws Exception
     */
    public static TIPO_PERSONA obtenerTipoPersona(String tipoPersona) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_PERSONA> collection;                               
        TIPO_PERSONA persona;  
        ConfVariables confVariables = new ConfVariables();                                                  

        persona = new TIPO_PERSONA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_persona", TIPO_PERSONA.class);

                persona = collection.find(new Document("clave", new Document("$eq", tipoPersona))).collation(collation).sort(new Document("valor", 1)).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de persona: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return persona;
    }

    /**
     * Devuelve el listado de los tipos de personas registrados en la base de
     * datos
     *
     * @return ArrayList TIPO_PERSONA
     *
     * @throws Exception
     */
    public static ArrayList<TIPO_PERSONA> obtenerListaTipoPersona() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_PERSONA> collection;                               
        ArrayList<TIPO_PERSONA> listaTipoPersona;    
        ConfVariables confVariables = new ConfVariables();                            

        listaTipoPersona = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_persona", TIPO_PERSONA.class);

                listaTipoPersona = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de tipo de persona: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listaTipoPersona;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static ArrayList<ENTIDAD_FEDERATIVA_PDE> obtenerListaDeEntidadesFederativas(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ENTIDAD_FEDERATIVA_PDE> collection;                     
        ArrayList<ENTIDAD_FEDERATIVA_PDE> entidadesFederativas;   
        ConfVariables confVariables = new ConfVariables();               

        entidadesFederativas = new ArrayList<>();

        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("entidades", ENTIDAD_FEDERATIVA_PDE.class);

                entidadesFederativas = collection.find().collation(collation).sort(new Document("nom_agee", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de tipo de entidades federativas: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return entidadesFederativas;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws Exception
     */
    public static ArrayList<MUNICIPIO_PDE> obtenerListaDeMunicipiosporEstado(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                          
        MongoDatabase database;                                                 
        MongoCollection<MUNICIPIO_PDE> collection;                              
        ArrayList<MUNICIPIO_PDE> municipios;    
        ConfVariables confVariables = new ConfVariables();                                 

        municipios = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (parameter != null && !parameter.isEmpty())
            { 
                if (conectarBaseDatos != null)
                { 
                    database = conectarBaseDatos.getDatabase(confVariables.getBD());
                    collection = database.getCollection("municipios", MUNICIPIO_PDE.class);

                    municipios = collection.find(
                            new Document("cve_agee",
                                    new Document("$eq", parameter)
                            )
                    ).collation(collation).sort(new Document("nom_agem", 1)).into(new ArrayList<>());

                } 
                else
                { 
                    throw new Exception("Conexion no establecida.");
                } 
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de municipios: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return municipios;
    }

    /**
     *
     * @return @throws Exception
     */
    public static ArrayList<PAIS> obtenerListaDePaises() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PAIS> collection;                                       
        ArrayList<PAIS> paises;         
        ConfVariables confVariables = new ConfVariables();                                         

        paises = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("paises", PAIS.class);

                paises = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de vialidades: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return paises;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    public static ArrayList<ASENTAMIENTO> obtenerAsentamiento(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ASENTAMIENTO> collection;                               
        ArrayList<ASENTAMIENTO> asentamientos;   
        ConfVariables confVariables = new ConfVariables();                                

        asentamientos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("asentamientos", ASENTAMIENTO.class);

                asentamientos = collection.find(new Document("cve_agem", new Document("$eq", parameter))).collation(collation).sort(new Document("nom_asenta", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de asentamientos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return asentamientos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    public static ArrayList<ASENTAMIENTO> obtenerAsentamientoPorCP(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<ASENTAMIENTO> collection;                               
        ArrayList<ASENTAMIENTO> asentamientos;     
        ConfVariables confVariables = new ConfVariables();                              

        asentamientos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("asentamientos", ASENTAMIENTO.class);

                asentamientos = collection.find(new Document("nom_clave", new Document("$eq", parameter))).collation(collation).sort(new Document("nom_asenta", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de asentamientos por codigo postal: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return asentamientos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     *
     * @throws java.lang.Exception
     */
    public static PAIS obtenerPais(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<PAIS> collection;                                       
        PAIS pais;        
        ConfVariables confVariables = new ConfVariables();                                                       

        pais = new PAIS();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("paises", PAIS.class);

                pais = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el pais: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return pais;
    }

    /**
     *
     * @return @throws Exception
     */
    public static ArrayList<TIPO_DOCUMENTO> obtenerTiposDeDocumentos() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_DOCUMENTO> collection;                             
        ArrayList<TIPO_DOCUMENTO> documentos;      
        ConfVariables confVariables = new ConfVariables();                              

        documentos = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_documento", TIPO_DOCUMENTO.class);

                documentos = collection.find().collation(collation).sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 

        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el listado de tipos de documentos: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return documentos;
    }

    /**
     *
     * @param parameter
     *
     * @return
     * @throws java.lang.Exception
     */
    public static TIPO_DOCUMENTO obtenerDocumento(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_DOCUMENTO> collection;                             
        TIPO_DOCUMENTO documento;            
        ConfVariables confVariables = new ConfVariables();                                    

        documento = new TIPO_DOCUMENTO();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_documento", TIPO_DOCUMENTO.class);

                documento = collection.find(new Document("clave", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();

                if (documento == null)
                { 
                    documento = collection.find(new Document("valor", new Document("$eq", parameter))).collation(collation).sort(new Document("valor", 1)).first();
                } 

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 

        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de documento: " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return documento;
    }

    public static ArrayList<TIPO_FALTA> obtenerListaTiposDeFaltaPsancionados() throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_FALTA> collection;                                 
        ArrayList<TIPO_FALTA> listadoDeFaltas;     
        ConfVariables confVariables = new ConfVariables();                              

        listadoDeFaltas = new ArrayList<>();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_falta", TIPO_FALTA.class);

                listadoDeFaltas = collection.find().sort(new Document("valor", 1)).into(new ArrayList<>());

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener listado de tipos de faltas para particulares sancionados:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return listadoDeFaltas;
    }

    /**
     *
     * @param parameter
     *
     * @return
     * @throws java.lang.Exception
     */
    public static TIPO_FALTA obtenerTipoDeFaltaPsancionados(String parameter) throws Exception
    {
        MongoClient conectarBaseDatos = null;                                   
        MongoDatabase database;                                                 
        MongoCollection<TIPO_FALTA> collection;                                 
        TIPO_FALTA falta;            
        ConfVariables confVariables = new ConfVariables();                                            

        falta = new TIPO_FALTA();
        try
        {
            conectarBaseDatos = DAOBase.conectarBaseDatos();

            if (conectarBaseDatos != null)
            { 
                database = conectarBaseDatos.getDatabase(confVariables.getBD());
                collection = database.getCollection("tipo_falta", TIPO_FALTA.class);

                falta = collection.find().sort(new Document("valor", 1)).first();

            } 
            else
            { 
                throw new Exception("Conexion no establecida.");
            } 
        }
        catch (Exception ex)
        {
            throw new Exception("Error al obtener el tipo de falta para particulares sancionados:  " + ex.toString());
        }
        finally
        {
            cerrarRecursos(conectarBaseDatos, null);
        }

        return falta;
    }

    /**
     * Metodo de ayuda para cerrar las conexiones activas
     *
     *
     * @param conectarBaseDatos
     * @param cursor
     */
    public static void cerrarRecursos(MongoClient conectarBaseDatos, MongoCursor<Document> cursor)
    { 

        if (cursor != null)
        { 
            cursor.close();
            cursor = null;
        } 

        if (conectarBaseDatos != null)
        { 
            conectarBaseDatos.close();
            conectarBaseDatos = null;
        } 

    } 

}
