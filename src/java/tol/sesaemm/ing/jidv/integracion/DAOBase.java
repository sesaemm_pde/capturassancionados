/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.integracion;

import com.mongodb.Block;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.ClusterSettings;
import java.util.Arrays;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import tol.sesaemm.ing.jidv.config.ConfVariables;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx 
 * Colaboracion: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
 */
 
public class DAOBase
{

    /**
     * Conexión a la base de datos sin opciones de cifrado
     *
     * @return MongoClient
     *
     * @throws Exception
     */
    public static MongoClient conectarBaseDatos() throws Exception
    {
        MongoCredential credential;
        MongoClient mongoClient = null;
        String tipoConexion = "CADENA_CONEXION";
        CodecRegistry pojoCodecRegistry;
        ConfVariables confVariables;

        confVariables = new ConfVariables();
        pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        if (tipoConexion.equals("CADENA_CONEXION"))
        {
            credential = MongoCredential.createCredential(confVariables.getUSUARIO(), confVariables.getBD(), confVariables.getCONTRASENIA().toCharArray());
            mongoClient = MongoClients.create(
                    MongoClientSettings.builder()
                            .applyToClusterSettings(new Block<ClusterSettings.Builder>()
                            {
                                @Override
                                public void apply(ClusterSettings.Builder builder)
                                {
                                    builder.hosts(Arrays.asList(new ServerAddress(confVariables.getHOST(), confVariables.getPUERTO())));
                                }
                            })
                            .codecRegistry(pojoCodecRegistry)
                            .credential(credential)
                            .build());
        }

        return mongoClient;
    }

}
