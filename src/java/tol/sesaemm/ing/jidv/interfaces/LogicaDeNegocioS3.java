/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tol.sesaemm.ing.jidv.interfaces;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import tol.sesaemm.ing.jidv.javabeans.NIVEL_ACCESO;
import tol.sesaemm.ing.jidv.javabeans.USUARIO;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.FILTRO_BUSQUEDA_SSANCIONADOS;
import tol.sesaemm.ing.jidv.javabeans.s3Sancionados.SsancionadosConPaginacion;
import tol.sesaemm.javabeans.ASENTAMIENTO;
import tol.sesaemm.javabeans.ENTE_PUBLICO;
import tol.sesaemm.javabeans.ENTIDAD_FEDERATIVA_PDE;
import tol.sesaemm.javabeans.GENERO;
import tol.sesaemm.javabeans.MONEDA;
import tol.sesaemm.javabeans.MUNICIPIO_PDE;
import tol.sesaemm.javabeans.ORGANO_INTERNO_CONTROL;
import tol.sesaemm.javabeans.PAIS;
import tol.sesaemm.javabeans.PUESTO;
import tol.sesaemm.javabeans.TIPO_DOCUMENTO;
import tol.sesaemm.javabeans.TIPO_PERSONA;
import tol.sesaemm.javabeans.UNIDAD_MEDIDA_PLAZO;
import tol.sesaemm.javabeans.s3ssancionados.SSANCIONADOS;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_FALTA;
import tol.sesaemm.javabeans.s3ssancionados.TIPO_SANCION_SER;

/**
 *
 * @author Jessica Díaz jessica.diaz@sesaemm.org.mx
 */
 
public interface LogicaDeNegocioS3
{

    public FILTRO_BUSQUEDA_SSANCIONADOS modelarFiltroServidorPublicoSancionado(HttpServletRequest request);

    public SsancionadosConPaginacion obtenerServidoresPublicosSancionados(USUARIO usuario, NIVEL_ACCESO nivelAcceso, ArrayList<ENTE_PUBLICO> dependencias, FILTRO_BUSQUEDA_SSANCIONADOS filtroBusqueda) throws Exception;

    public SSANCIONADOS obtenerDetalleServidorPublicoSancionado(String idServidorPublico) throws Exception;

    public SSANCIONADOS obtenerEstatusDePrivacidadCamposSSANCIONADOS() throws Exception;

    public SSANCIONADOS modelarRequestServidorPublicosSancionado(HttpServletRequest request) throws Exception;

    public SSANCIONADOS modelarRequestServidorPublicosSancionado(HttpServletRequest request, SSANCIONADOS servidorPublico) throws Exception;

    public String registrarServidorPublicoSancionado(SSANCIONADOS servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public String editarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublico, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public String publicarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public String desPublicarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public String eliminarServidorPublicoSancionado(String id, SSANCIONADOS servidorPublicos, USUARIO usuario, NIVEL_ACCESO nivelAcceso) throws Exception;

    public ArrayList<ENTE_PUBLICO> obtenerDependenciasDelUsuario(USUARIO usuario) throws Exception;

    public ArrayList<PUESTO> obtenerListaDePuestos() throws Exception;

    public ArrayList<GENERO> obtenerListaDeGeneros() throws Exception;

    public ArrayList<ENTE_PUBLICO> obtenerEntesPublicos(USUARIO usuario) throws Exception;

    public ArrayList<TIPO_FALTA> obtenerListaTiposDeFalta() throws Exception;

    public ArrayList<TIPO_SANCION_SER> obtenerListaTiposDeSancion() throws Exception;

    public ArrayList<MONEDA> obtenerListaDeTiposDeMoneda() throws Exception;

    public ArrayList<UNIDAD_MEDIDA_PLAZO> obtenerListaDeUnidadMedidaPlazo() throws Exception;

    public ArrayList<ORGANO_INTERNO_CONTROL> obtenerListaDeOrganoInternoDeControl() throws Exception;

    public ORGANO_INTERNO_CONTROL obtenerOrganoInternoDeControl(String parameter) throws Exception;

    public TIPO_PERSONA obtenerTipoPersona(String tipoPersona) throws Exception;

    public ArrayList<TIPO_PERSONA> obtenerListaTipoPersona() throws Exception;

    public ArrayList<MUNICIPIO_PDE> obtenerListaDeMunicipiosporEstado(String parameter) throws Exception;

    public ArrayList<PAIS> obtenerListaDePaises() throws Exception;

    public ArrayList<ASENTAMIENTO> obtenerAsentamiento(String parameter) throws Exception;

    public ArrayList<ASENTAMIENTO> obtenerAsentamientoPorCP(String parameter) throws Exception;

    public ArrayList<ENTIDAD_FEDERATIVA_PDE> obtenerListaDeEntidadesFederativa(String parameter) throws Exception;

    public ArrayList<TIPO_DOCUMENTO> obtenerTiposDeDocumentos() throws Exception;

    public ArrayList<TIPO_FALTA> obtenerListaTiposDeFaltaPsancionados() throws Exception;

}
