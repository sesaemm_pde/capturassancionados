<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<!DOCTYPE html>
<html lang="es-Es">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>Login para los sistemas de la SESAEMM</title>

        <meta charset="ISO-8859-1" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <link rel="shortcut icon" href="framework/img/png/favicon.png" />
        <link rel="icon" href="framework/img/png/favicon_32.png" sizes="32x32" />
        <link rel="icon" href="framework/img/png/favicon_192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="framework/img/png/favicon_180.png">
        <meta name="msapplication-TileImage" content="framework/img/png/favicon_270.png">
        <link rel="stylesheet" href="style.min.css" />
        <link rel="stylesheet" href="framework/css/login.min.css" />

    </head>

    <body class="login">
        <div id="login">
            <h1><a href="https://saemm.gob.mx/" title="Bienvenidos" tabindex="-1">Login para los sistemas de la SESAEMM</a></h1>
            <form name="loginform" id="loginform" action="/sistemaCaptura${initParam.versionIntranet}/intranet" method="post" data-abide novalidate>
                <input type="hidden" name="accion" value="loguearUsuario"/>
                <label for="user_login">Nombre de usuario<br />
                    <input type="text" name="txtUsuario" id="txtUsuario" class="input" value="" size="20" required />
                    <span class="form-error" data-form-error-for="txtUsuario">
                        Campo requerido
                    </span>
                </label>
                <label for="user_pass">Contrase&ntilde;a<br />
                    <input type="password" name="txtPassword" id="txtPassword" class="input" value="" size="20" required />
                    <span class="form-error" data-form-error-for="txtPassword">
                        Campo requerido
                    </span>
                </label>
                <span class="form-error" style="display: block;">${mensaje}</span>
                <input type="submit" class="button button-primary button-large display-top" value="Acceder" />
            </form>
        </div>
        <div class="clear"></div>

        <script src="framework/js/vendor/jquery.js"></script>
        <script src="framework/js/vendor/what-input.js"></script>
        <script src="framework/js/vendor/foundation.js"></script>
        <script src="framework/js/app.js"></script>

    </body>

</html>
