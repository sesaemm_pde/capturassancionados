<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri = "http://saemm.gob.mx" prefix="patterns"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema III</h2>
            <h4>Servidores p&uacute;blicos sancionados.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de servidores p&uacute;blicos</a>
                <a class="button"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo servidor p&uacute;blico</a>
            </div>
            <form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                <input type="hidden" name="accion" value="consultarServidoresPublicosSancionados"/>
            </form>
            <h2>Datos del servidor p&uacute;blico </h2>
            <fieldset class="fieldset">
                <legend>Servidor p&uacute;blico</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>RFC</b>
                        <label>${patterns:validarRfcPF(servidorPublico.servidorPublicoSancionado.rfc) ? servidorPublico.servidorPublicoSancionado.rfc : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>CURP</b>
                        <label>${patterns:validarCurp(servidorPublico.servidorPublicoSancionado.curp) ? servidorPublico.servidorPublicoSancionado.curp : ""}</label>
                    </label>
                    <label class="cell medium-4"><b>G&eacute;nero</b>
                        <label>${servidorPublico.servidorPublicoSancionado.genero.clave} - ${servidorPublico.servidorPublicoSancionado.genero.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Nombres</b>
                        <label>${servidorPublico.servidorPublicoSancionado.nombres}</label>
                    </label>
                    <label class="cell medium-4"><b>Primer apellido</b>
                        <label>${servidorPublico.servidorPublicoSancionado.primerApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Segundo apellido</b>
                        <label>${servidorPublico.servidorPublicoSancionado.segundoApellido}</label>
                    </label>
                    <label class="cell medium-4"><b>Puesto</b>
                        <label>${servidorPublico.servidorPublicoSancionado.puesto}</label>
                    </label>
                    <label class="cell medium-4"><b>Nivel</b>
                        <label>${servidorPublico.servidorPublicoSancionado.nivel}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Datos de la sanci&oacute;n</legend>
                <div class="grid-margin-x grid-x">
                    <label class="cell medium-4"><b>N&uacute;mero de expediente</b>
                        <label>${servidorPublico.expediente}</label>
                    </label>
                    <label class="cell medium-4"><b>Instituci&oacute;n de dependencia</b>
                        <label>${servidorPublico.institucionDependencia.nombre}</label>
                    </label>
                    <label class="cell medium-4"><b>Autoridad sancionadora</b>
                        <label>${servidorPublico.autoridadSancionadora}</label>
                    </label>
                    <label class="cell medium-4"><b>Tipo de falta</b>
                        <label>${servidorPublico.tipoFalta.clave} - ${servidorPublico.tipoFalta.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Descripci&oacute;n de la falta</b>
                        <label>${servidorPublico.tipoFalta.descripcion}</label>
                    </label>
                    <label class="cell medium-12">
                        <fieldset class="fieldset">
                            <legend><b>Tipo de sanci&oacute;n</b></legend>
                            <div class="grid-margin-x grid-x">
                                <c:forEach  items="${servidorPublico.tipoSancion}" var="tipoSancion">
                                    <label class="cell medium-4">
                                        <label><b>Tipo:</b> ${tipoSancion.valor}</label>
                                        <label><b>Descripci&oacute;n:</b> ${tipoSancion.descripcion}</label>
                                    </label>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </label>
                    <label class="cell medium-12"><b>Causa o motivo</b>
                        <label>${servidorPublico.causaMotivoHechos}</label>
                    </label>
                    <label class="cell medium-4"><b>URL de la resoluci&oacute;n</b>
                        <c:if test="${servidorPublico.resolucion.url != null && servidorPublico.resolucion.url.isEmpty() == false}">
                            <label><a href="${(fn:contains(servidorPublico.resolucion.url, 'http')) ? "" : "//" }${servidorPublico.resolucion.url}" target="_blank">${servidorPublico.resolucion.url}</a></label>
                            </c:if>
                    </label>
                    <label class="cell medium-4"><b>Fecha de la resoluci&oacute;n</b>
                        <label>${servidorPublico.resolucion.fechaResolucion}</label>
                    </label>
                    <label class="cell medium-4"><b>Sanci&oacute;n econ&oacute;mica</b>
                        <label>${servidorPublico.multa.monto} ${servidorPublico.multa.moneda.valor}</label>
                    </label>
                    <label class="cell medium-4"><b>Plazo de inhabilitaci&oacute;n</b>
                        <label>${servidorPublico.inhabilitacion.plazo}</label>
                    </label>
                    <label class="cell medium-4"><b>Fecha inicial</b>
                        <label>${servidorPublico.inhabilitacion.fechaInicial}</label>
                    </label>
                    <label class="cell medium-4"><b>Fecha final</b>
                        <label>${servidorPublico.inhabilitacion.fechaFinal}</label>
                    </label>
                    <label class="cell medium-12"><b>Observaciones</b>
                        <label>${servidorPublico.observaciones}</label>
                    </label>
                </div>
            </fieldset>
            <fieldset class="fieldset">
                <legend>Documentos</legend>
                <c:choose>
                    <c:when test="${fn:length(servidorPublico.documentos) > 0}">
                        <div class="grid-margin-x grid-x">
                            <c:forEach items="${servidorPublico.documentos}" var="documento">
                                <label class="cell medium-4"><b>Identificador &uacute;nico</b>
                                    <label>${documento.id}</label>
                                </label>
                                <label class="cell medium-4"><b>Tipo</b>
                                    <label>${documento.tipo}</label>
                                </label>
                                <label class="cell medium-4"><b>T&iacute;tulo</b>
                                    <label>${documento.titulo}</label>
                                </label>
                                <label class="cell medium-4"><b>Descripci&oacute;n</b>
                                    <label>${documento.descripcion}</label>
                                </label>
                                <label class="cell medium-4"><b>URL</b>
                                    <c:if test="${documento.url != null && documento.url.isEmpty() == false}">
                                        <label><a href="${(fn:contains(documento.url, 'http')) ? "" : "//" }${documento.url}" target="_blank">${documento.url}</a></label>
                                        </c:if>
                                </label>
                                <label class="cell medium-4"><b>Fecha</b>
                                    <label>${documento.fecha}</label>
                                </label>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        Sin documentos.
                    </c:otherwise>
                </c:choose>
            </fieldset>
            <div class="grid-x grid-margin-x">
                <fieldset class="cell medium-6">
                    <form name="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" data-abide novalidate>
                        <input type="hidden" name="accion" value="modificarDatosServidorPublicoSancionado"/>
                        <input type="hidden" name="txtNOMBRES" value="${servidorPublico.servidorPublicoSancionado.nombres}"/>
                        <input type="hidden" name="txtPRIMER_APELLIDO" value="${servidorPublico.servidorPublicoSancionado.primerApellido}"/>
                        <input type="hidden" name="txtSEGUNDO_APELLIDO" value="${servidorPublico.servidorPublicoSancionado.segundoApellido}"/>
                        <input type="hidden" name="txtRFC" value="${servidorPublico.servidorPublicoSancionado.rfc}"/>
                        <input type="hidden" name="txtCURP" value="${servidorPublico.servidorPublicoSancionado.curp}"/>
                        <input type="hidden" name="radGENERO" value="${servidorPublico.servidorPublicoSancionado.genero.clave}"/>
                        <input type="hidden" name="cmbPUESTO" value="${servidorPublico.servidorPublicoSancionado.puestoCompleto.id}"/>
                        <input type="hidden" name="txtNUMERO_EXPEDIENTE" value="${servidorPublico.expediente}"/>
                        <input type="hidden" name="cmbDEPENDENCIA" value="${servidorPublico.institucionDependencia.clave}"/>
                        <input type="hidden" name="txtAUTORIDAD_SANCIONADORA" value="${servidorPublico.autoridadSancionadora}"/>
                        <input type="hidden" name="cmbTIPO_FALTAS" value="${servidorPublico.tipoFalta.clave}"/>
                        <input type="hidden" name="txtTIPO_FALTAS_DESCRIPCION" value="${servidorPublico.tipoFalta.descripcion}"/>
                        <c:forEach items="${servidorPublico.tipoSancion}" var="sancion">
                            <input type="hidden" name="chckTIPO_SANCION"  value="${sancion.clave}"/>
                            <input type="hidden" name="txtTIPO_SANCION_${sancion.clave}_DESCRIPCION"  value="${sancion.descripcion}"/>
                        </c:forEach>
                        <input type="hidden" name="txtCAUSA_MOTIVO" value="${servidorPublico.causaMotivoHechos}"/>
                        <input type="hidden" name="txtURL_RESOLUCION" value="${servidorPublico.resolucion.url}"/>
                        <input type="hidden" name="txtFECHA_RESOLUCION" value="${servidorPublico.resolucion.fechaResolucion}" />
                        <input type="hidden" name="txtMULTA_MONTO" value="${servidorPublico.multa.monto}"/>
                        <input type="hidden" name="cmbMONEDA" value="${servidorPublico.multa.moneda.clave}"/>
                        <input type="hidden" name="txtPLAZO" value="${arregloDePlazo[0]}"/>
                        <input type="hidden" name="cmbUNIDAD_MEDIDA_PLAZO" value="${arregloDePlazo[1]}"/>
                        <input type="hidden" name="txtFECHA_INICIAL" value="${servidorPublico.inhabilitacion.fechaInicial}" />
                        <input type="hidden" name="txtFECHA_FINAL" value="${servidorPublico.inhabilitacion.fechaFinal}"/>
                        <input type="hidden" name="txtOBSERVACIONES" value="${servidorPublico.observaciones}">
                        <c:set var="contadorDocs" value="0"></c:set>
                        <c:forEach items="${servidorPublico.documentos}" var="listaDocumentos">
                            <input type="hidden" name="txtDOCUMENTO_ID_${contadorDocs}" value="${listaDocumentos.id}" />
                            <input type="hidden" name="txtDOCUMENTO_TIPO_${contadorDocs}" value="${listaDocumentos.tipo}"/>
                            <input type="hidden" name="txtDOCUMENTO_TITULO_${contadorDocs}" value="${listaDocumentos.titulo}"/>
                            <input type="hidden" name="txtDOCUMENTO_DESCRIPCION_${contadorDocs}" value="${listaDocumentos.descripcion}"/>
                            <input type="hidden" name="txtDOCUMENTO_URL_${contadorDocs}" value="${listaDocumentos.url}"/>
                            <input type="hidden" name="txtDOCUMENTO_FECHA_${contadorDocs}" value="${listaDocumentos.fecha}"/>
                            <c:set var="contadorDocs" value="${contadorDocs+1}"></c:set>
                        </c:forEach>
                        <input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="${fn:length(servidorPublico.documentos)}">
                        <input type="submit" class="button expanded secondary" name="btnReset" value="Modificar datos"/>
                    </form>
                </fieldset>
                <fieldset class="cell medium-6">
                    <form name="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" data-abide novalidate>
                        <input type="hidden" name="accion" value="registrarServidorPublicoSancionado"/>
                        <input type="hidden" name="txtNOMBRES" value="${servidorPublico.servidorPublicoSancionado.nombres}"/>
                        <input type="hidden" name="txtPRIMER_APELLIDO" value="${servidorPublico.servidorPublicoSancionado.primerApellido}"/>
                        <input type="hidden" name="txtSEGUNDO_APELLIDO" value="${servidorPublico.servidorPublicoSancionado.segundoApellido}"/>
                        <input type="hidden" name="txtRFC" value="${servidorPublico.servidorPublicoSancionado.rfc}"/>
                        <input type="hidden" name="txtCURP" value="${servidorPublico.servidorPublicoSancionado.curp}"/>
                        <input type="hidden" name="radGENERO" value="${servidorPublico.servidorPublicoSancionado.genero.clave}"/>
                        <input type="hidden" name="cmbPUESTO" value="${servidorPublico.servidorPublicoSancionado.puestoCompleto.id}"/>
                        <input type="hidden" name="txtNUMERO_EXPEDIENTE" value="${servidorPublico.expediente}"/>
                        <input type="hidden" name="cmbDEPENDENCIA" value="${servidorPublico.institucionDependencia.clave}"/>
                        <input type="hidden" name="txtAUTORIDAD_SANCIONADORA" value="${servidorPublico.autoridadSancionadora}"/>
                        <input type="hidden" name="cmbTIPO_FALTAS" value="${servidorPublico.tipoFalta.clave}"/>
                        <input type="hidden" name="txtTIPO_FALTAS_DESCRIPCION" value="${servidorPublico.tipoFalta.descripcion}"/>
                        <c:forEach items="${servidorPublico.tipoSancion}" var="sancion">
                            <input type="hidden" name="chckTIPO_SANCION"  value="${sancion.clave}"/>
                            <input type="hidden" name="txtTIPO_SANCION_${sancion.clave}_DESCRIPCION"  value="${sancion.descripcion}"/>
                        </c:forEach>
                        <input type="hidden" name="txtCAUSA_MOTIVO" value="${servidorPublico.causaMotivoHechos}"/>
                        <input type="hidden" name="txtURL_RESOLUCION" value="${servidorPublico.resolucion.url}"/>
                        <input type="hidden" name="txtFECHA_RESOLUCION" value="${servidorPublico.resolucion.fechaResolucion}" />
                        <input type="hidden" name="txtMULTA_MONTO" value="${servidorPublico.multa.monto}"/>
                        <input type="hidden" name="cmbMONEDA" value="${servidorPublico.multa.moneda.clave}"/>
                        <input type="hidden" name="txtPLAZO" value="${arregloDePlazo[0]}"/>
                        <input type="hidden" name="cmbUNIDAD_MEDIDA_PLAZO" value="${arregloDePlazo[1]}"/>
                        <input type="hidden" name="txtFECHA_INICIAL" value="${servidorPublico.inhabilitacion.fechaInicial}" />
                        <input type="hidden" name="txtFECHA_FINAL" value="${servidorPublico.inhabilitacion.fechaFinal}"/>
                        <input type="hidden" name="txtOBSERVACIONES" value="${servidorPublico.observaciones}">
                        <c:set var="contadorDocs" value="0"></c:set>
                        <c:forEach items="${servidorPublico.documentos}" var="listaDocumentos">
                            <input type="hidden" name="txtDOCUMENTO_ID_${contadorDocs}" value="${listaDocumentos.id}" />
                            <input type="hidden" name="txtDOCUMENTO_TIPO_${contadorDocs}" value="${listaDocumentos.tipo}"/>
                            <input type="hidden" name="txtDOCUMENTO_TITULO_${contadorDocs}" value="${listaDocumentos.titulo}"/>
                            <input type="hidden" name="txtDOCUMENTO_DESCRIPCION_${contadorDocs}" value="${listaDocumentos.descripcion}"/>
                            <input type="hidden" name="txtDOCUMENTO_URL_${contadorDocs}" value="${listaDocumentos.url}"/>
                            <input type="hidden" name="txtDOCUMENTO_FECHA_${contadorDocs}" value="${listaDocumentos.fecha}"/>
                            <c:set var="contadorDocs" value="${contadorDocs+1}"></c:set>
                        </c:forEach>
                        <input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="${fn:length(servidorPublico.documentos)}">
                        <input type="submit" class="button expanded" name="btnRegistrar" value="Registrar"/>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
</div>

<%@include file="/piePagina.jsp" %>