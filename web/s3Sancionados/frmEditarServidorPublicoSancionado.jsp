<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://saemm.gob.mx" prefix="patterns"%>

<%@include file="/encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="/migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Plataforma Digital Estatal</h1>
            <h2>Sistema III</h2>
            <h4>Servidores p&uacute;blicos sancionados.</h4>
            <div id="cargaDB" class="reveal-overlay" style="display: none;">
                <div id="loader"></div>
                <div id="textDB">
                    Actualizando base de datos...
                </div>
            </div>
            <div class="stacked-for-small expanded large button-group">
                <a class="button hollow" onclick="document.frmListado.submit()"><i class="material-icons md-4t">list</i> Listado de servidores p&uacute;blicos</a>
                <a class="button hollow" onclick="document.frmNuevo.submit()"><i class="material-icons md-4t">playlist_add</i> Agregar nuevo servidor p&uacute;blico</a>
                <form name="frmNuevo" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                    <input type="hidden" name="accion" value="mostrarPaginaRegistroNuevoServidorPublicoSancionado"/>
                </form>
            </div>
            <form name="frmListado" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones">
                <input type="hidden" name="accion" value="consultarServidoresPublicosSancionados"/>
            </form>
            <h2>Modificar servidor p&uacute;blico</h2>
            <p><span style="color:red;">*</span> Campo obligatorio</p>
            <form name="frmRegistrar" id="frmRegistrar" method="post" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" data-abide novalidate onSubmit="event.preventDefault(); validarSeleccionChecks(this.id);" autocomplete="off">
                <input type="hidden" name="accion" value="editarDatosServidorPublicoSancionado"/>
                <input type="hidden" name="txtId" value="${servidorPublico.idSpdn}">
                <input type="hidden" name="listaDeIdentificadoresDeDocumentosPorDefecto" id="listaDeIdentificadoresDeDocumentosPorDefecto" value="">
                <input type="hidden" name="listaDeIdentificadoresDeDocumentosARegistrar" id="listaDeIdentificadoresDeDocumentosARegistrar" value="">
                <div data-abide-error="data-abide-error" class="alert callout" style="display: none;">
                    <p>
                        <i class="material-icons">warning</i>
                        Existen algunos errores en tu registro. Verifica nuevamente.
                    </p>
                </div>
                <fieldset class="fieldset">
                    <legend>Servidor p&uacute;blico</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">Nombres <span style="color:red;">*</span>
                            <input type="text" name="txtNOMBRES" id="txtNOMBRES" placeholder="p. ej. Carlos" required value="${servidorPublico.servidorPublicoSancionado.nombres}"/>
                            <span class="form-error" data-form-error-for="txtNOMBRES">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Primer apellido <span style="color:red;">*</span>
                            <input type="text" name="txtPRIMER_APELLIDO" id="txtPRIMER_APELLIDO" placeholder="p. ej. P&eacute;rez" required value="${servidorPublico.servidorPublicoSancionado.primerApellido}"/>
                            <span class="form-error" data-form-error-for="txtPRIMER_APELLIDO">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Segundo apellido
                            <input type="text" name="txtSEGUNDO_APELLIDO" id="txtSEGUNDO_APELLIDO" placeholder="p. ej. P&eacute;rez" value="${servidorPublico.servidorPublicoSancionado.segundoApellido}"/>
                            <span class="form-error" data-form-error-for="txtSEGUNDO_APELLIDO">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">RFC
                            <input type="text" name="txtRFC" id="txtRFC" class="mayusculas" placeholder="p. ej. XAXX010101XX0 o XAXX010101" pattern="rfc_pf" value="${patterns:validarRfcPF(servidorPublico.servidorPublicoSancionado.rfc) ? servidorPublico.servidorPublicoSancionado.rfc : ""}"/>
                            <span class="form-error" data-form-error-for="txtRFC">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">CURP
                            <input type="text" name="txtCURP" id="txtCURP" class="mayusculas" placeholder="p. ej. XAXX010101XAXAXA01" pattern="curp" value="${patterns:validarCurp(servidorPublico.servidorPublicoSancionado.curp) ? servidorPublico.servidorPublicoSancionado.curp : ""}"/>
                            <span class="form-error" data-form-error-for="txtCURP">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <fieldset class="cell medium-4">
                            <legend>G&eacute;nero</legend>
                            <c:forEach items="${generos}" var="genero">
                                <label>
                                    <input type="radio" name="radGENERO" id="radGENERO${genero.clave}" value="${genero.clave}" ${servidorPublico.servidorPublicoSancionado.genero.clave == genero.clave ? 'checked': ''}/>
                                    <label for="radGENERO${genero.clave}">${genero.valor}</label>
                                </label>
                            </c:forEach>
                        </fieldset>
                        <label class="cell medium-4">Puesto <span style="color:red;">*</span>
                            <select name="cmbPUESTO" id="cmbPUESTO" required>
                                <option value="" disabled="disabled" selected="selected">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${puestos}" var="puesto">
                                    <option value="${puesto.id}" ${servidorPublico.servidorPublicoSancionado.puesto == puesto.funcional ? 'selected' : '' }>${puesto.funcional}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbPUESTO" id="spanPUESTO">
                                Campo requerido, elige una opci&oacute;n.
                            </span>
                        </label>
                    </div>
                </fieldset>
                <fieldset class="fieldset">
                    <legend>Datos de la sanci&oacute;n</legend>
                    <div class="grid-margin-x grid-x">
                        <label class="cell medium-4">N&uacute;mero de expediente
                            <input type="text" name="txtNUMERO_EXPEDIENTE" id="txtNUMERO_EXPEDIENTE" placeholder="p. ej. 123" value="${servidorPublico.expediente}"/>
                            <span class="form-error" data-form-error-for="txtNUMERO_EXPEDIENTE">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Instituci&oacute;n de dependencia <span style="color:red;">*</span>
                            <select name="cmbDEPENDENCIA" id="cmbDEPENDENCIA" required>
                                <option value="" disabled="disabled" selected="selected">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${entesPublicos}" var="ente_publico">
                                    <option value="${ente_publico.clave}" ${servidorPublico.institucionDependencia.clave == ente_publico.clave ? 'selected' : ''}>${ente_publico.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbDEPENDENCIA" id="spanDEPENDENCIA">
                                Campo requerido, elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">Autoridad sancionadora
                            <select name="txtAUTORIDAD_SANCIONADORA" id="txtAUTORIDAD_SANCIONADORA">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${organosInternos}" var="organo">
                                    <option value="${organo.clave}" ${organo.valor == servidorPublico.autoridadSancionadora ? 'selected' : ''}>${organo.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="txtAUTORIDAD_SANCIONADORA">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-6">Tipo de falta
                            <span style="color:red">*</span>
                            <select name="cmbTIPO_FALTAS" id="cmbTIPO_FALTAS" required>
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tipoFaltas}" var="falta">
                                    <option value="${falta.clave}" ${fn:contains(servidorPublico.tipoFalta.clave, falta.clave) ? 'selected' : ''} />${falta.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbTIPO_FALTAS" id="spanTIPO_FALTAS">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-6">Descripci&oacute;n de la falta
                            <input type="text" name="txtTIPO_FALTAS_DESCRIPCION" placeholder="p. ej. Descripci&oacute;n o nota aclaratoria" id="txtTIPO_FALTAS_DESCRIPCION" value="${servidorPublico.tipoFalta.descripcion}"/>
                            <span class="form-error" data-form-error-for="txtTIPO_FALTAS_DESCRIPCION" id="spanTIPO_FALTAS_DESCRIPCION">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-12">
                            <fieldset class="fieldset">
                                <legend>Tipo de sanci&oacute;n <span style="color:red">*</span></legend>
                                <div class="grid-margin-x grid-x">
                                    <c:forEach items="${tipoSancion}" var="sancion">
                                        <label class="cell medium-6">
                                            <c:set var="found" value="false" scope="request" />
                                            <c:set var="index" value="-1" scope="request" />
                                            <c:set var="contador" value="0" scope="request" />
                                            <c:forEach items="${servidorPublico.tipoSancion}" var="tipoSancion_sp">
                                                <c:if test="${tipoSancion_sp.clave == sancion.clave}">
                                                    <c:set var="found" value="true" scope="request" />
                                                    <c:set var="index" value="${contador}" scope="request" />
                                                </c:if>
                                                <c:set var="contador" value="${contador+1}" scope="request" />
                                            </c:forEach>
                                            <label class="text-truncate">
                                                <input data-min-required="1" type="checkbox" name="chckTIPO_SANCION" id="chckTIPO_SANCION_${sancion.clave}" onclick="validarSiFueSeleccionado(this.id, 'txtTIPO_SANCION_${sancion.clave}_DESCRIPCION');" value="${sancion.clave}" ${found ? 'checked' : ''} /> ${sancion.valor}
                                            </label>
                                            <label class="">Descripci&oacute;n
                                                <input type="text" name="txtTIPO_SANCION_${sancion.clave}_DESCRIPCION" id="txtTIPO_SANCION_${sancion.clave}_DESCRIPCION" placeholder="p. ej. Descripci&oacute;n o nota aclaratoria" value="${found ? index >= 0 ? servidorPublico.tipoSancion[index].descripcion : '' : ''}" ${found ? '' : 'disabled'}/>
                                            </label>
                                            <span class="form-error" data-form-error-for="chckTIPO_SANCION" id="spanTIPO_SANCION">
                                                Campo requerido, elige una opci&oacute;n.
                                            </span>
                                        </label>
                                    </c:forEach>
                                </div>
                            </fieldset>
                        </label>
                        <label class="cell medium-12">Causa o motivo <span style="color:red;">*</span>
                            <textarea name="txtCAUSA_MOTIVO" id="txtCAUSA_MOTIVO" placeholder="p. ej. Abuso de autoridad" required>${servidorPublico.causaMotivoHechos}</textarea>
                            <span class="form-error" data-form-error-for="txtCAUSA_MOTIVO">
                                Campo requerido, verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-8">URL de la resoluci&oacute;n
                            <input type="text" name="txtURL_RESOLUCION" id="txtURL_RESOLUCION" placeholder="p. ej. www.url.com" value="${servidorPublico.resolucion.url}" pattern="url"/>
                            <span class="form-error" data-form-error-for="txtURL_RESOLUCION" id="spanURL_RESOLUCION">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha de la resoluci&oacute;n
                            <input type="date" name="txtFECHA_RESOLUCION" id="txtFECHA_RESOLUCION" placeholder="p. ej. 2018-06-10" value="${servidorPublico.resolucion.fechaResolucion}" pattern="fecha"/>
                            <span class="form-error" data-form-error-for="txtFECHA_RESOLUCION">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Sanci&oacute;n econ&oacute;mica - Monto
                            <input type="number" min="0" name="txtMULTA_MONTO" id="txtMULTA_MONTO" placeholder="p. ej. 10" value="${servidorPublico.multa.monto}"/>
                            <span class="form-error" data-form-error-for="txtMULTA_MONTO">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-6">Sanci&oacute;n econ&oacute;mica - Moneda
                            <select name="cmbMONEDA" id="cmbMONEDA">
                                <option value="">--Elige una opci&oacute;n--</option>
                                <c:forEach items="${tipoMoneda}" var="moneda">
                                    <option value="${moneda.clave}" ${fn:contains(servidorPublico.multa.moneda.clave, moneda.clave) ? 'selected' : ''} />${moneda.valor}</option>
                                </c:forEach>
                            </select>
                            <span class="form-error" data-form-error-for="cmbMONEDA" id="spanMONEDA">
                                Elige una opci&oacute;n.
                            </span>
                        </label>
                        <label class="cell medium-4">Plazo de inhabilitaci&oacute;n
                            <div class="grid-x">
                                <label class="small-4 cell margin-right-1">
                                    <input type="number" min="0" name="txtPLAZO" id="txtPLAZO" placeholder="p. ej. 3 meses" onblur="validarPlazo();" value="${arregloDePlazo[0]}"/>
                                    <span class="form-error" data-form-error-for="txtPLAZO" id="spanPLAZO">
                                        Campo requerido.
                                    </span>
                                </label>
                                <label class="small-7 cell">
                                    <select name="cmbUNIDAD_MEDIDA_PLAZO" id="cmbUNIDAD_MEDIDA_PLAZO" onchange="validarPlazo();">
                                        <option value="">--Elige una opci&oacute;n--</option>
                                        <c:forEach items="${unidadesMedidasPlazos}" var="unidadMedidaPlazo">
                                            <option value="${unidadMedidaPlazo.clave}" ${ (arregloDePlazo[1] == unidadMedidaPlazo.valor) ? 'selected' : ''}/>${unidadMedidaPlazo.valor}</option>
                                        </c:forEach>
                                    </select>
                                    <span class="form-error" data-form-error-for="cmbUNIDAD_MEDIDA_PLAZO" id="spanUNIDAD_MEDIDA_PLAZO">
                                        Elige una opci&oacute;n.
                                    </span>
                                </label>
                            </div>
                        </label>
                        <label class="cell medium-4">Fecha inicial
                            <input type="date" name="txtFECHA_INICIAL" id="txtFECHA_INICIAL" placeholder="p. ej. 2018-06-19" value="${servidorPublico.inhabilitacion.fechaInicial}" pattern="fecha" oninput="validarPlazo();" />
                            <span class="form-error" data-form-error-for="txtFECHA_INICIAL">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-4">Fecha final
                            <input type="date" name="txtFECHA_FINAL" id="txtFECHA_FINAL" placeholder="p. ej. 2018-06-19" value="${servidorPublico.inhabilitacion.fechaFinal}" pattern="fecha" oninput="validarPlazo();" />
                            <span class="form-error" data-form-error-for="txtFECHA_FINAL">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                        <label class="cell medium-12">Observaciones
                            <textarea name="txtOBSERVACIONES" placeholder="p. ej. Cualquier observaci&oacute;n pertinente" id="txtOBSERVACIONES">${servidorPublico.observaciones}</textarea>
                            <span class="form-error" data-form-error-for="txtOBSERVACIONES">
                                Verifica el formato de tu registro.
                            </span>
                        </label>
                    </div>
                </fieldset>
                <fieldset class="fieldset">
                    <legend>Documentos</legend>
                    <div id="divContenedorDocumentos">
                        <c:choose>
                            <c:when test="${fn:length(servidorPublico.documentos) > 0}">
                                <c:set var="contadorDocs" value="0"></c:set>
                                <c:forEach items="${servidorPublico.documentos}" var="listaDocumentos">
                                    <div id="documento${contadorDocs}" >
                                        <div class="grid-margin-x grid-x">
                                            <label class="cell medium-4">Identificador &uacute;nico
                                                <span style="color:red;">**</span>
                                                <input type="text" name="txtDOCUMENTO_ID_${contadorDocs}" id="txtDOCUMENTO_ID_${contadorDocs}" placeholder="p. ej. 1" value="${listaDocumentos.id}" onfocus="desbloquearCamposDocumento(${contadorDocs})"/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_ID_${contadorDocs}" id="spanDOCUMENTO_ID_${contadorDocs}">
                                                    Verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">Tipo
                                                <select name="txtDOCUMENTO_TIPO_${contadorDocs}" id="txtDOCUMENTO_TIPO_${contadorDocs}">
                                                    <option value="">--Elige una opci&oacute;n--</option>
                                                    <c:forEach items="${tiposDeDocumento}" var="documento">
                                                        <option value="${documento.clave}" ${(listaDocumentos.tipo == documento.valor) ? 'selected' : ''} >${documento.valor}</option>
                                                    </c:forEach>
                                                </select>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_TIPO_${contadorDocs}" id="spanDOCUMENTO_TIPO_${contadorDocs}">
                                                    Elige una opci&oacute;n.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">T&iacute;tulo
                                                <span style="color: red">*</span>
                                                <input type="text" name="txtDOCUMENTO_TITULO_${contadorDocs}" id="txtDOCUMENTO_TITULO_${contadorDocs}" placeholder="p. ej. Constancia de sanci&oacute;n..." value="${listaDocumentos.titulo}" required/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_TITULO_${contadorDocs}" id="spanDOCUMENTO_TITULO_${contadorDocs}">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">Descripci&oacute;n
                                                <span style="color: red">*</span>
                                                <input type="text" name="txtDOCUMENTO_DESCRIPCION_${contadorDocs}" id="txtDOCUMENTO_DESCRIPCION_${contadorDocs}" placeholder="p. ej. Descripci&oacute;n del documento" value="${listaDocumentos.descripcion}" required/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_DESCRIPCION_${contadorDocs}" id="spanDOCUMENTO_DESCRIPCION_${contadorDocs}">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">URL
                                                <span style="color: red">*</span>
                                                <input type="text" name="txtDOCUMENTO_URL_${contadorDocs}" id="txtDOCUMENTO_URL_${contadorDocs}" placeholder="p. ej. www.url.com" value="${listaDocumentos.url}" pattern="url" required/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_URL_${contadorDocs}" id="spanDOCUMENTO_URL_${contadorDocs}">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">Fecha
                                                <span style="color: red">*</span>
                                                <input type="date" name="txtDOCUMENTO_FECHA_${contadorDocs}" id="txtDOCUMENTO_FECHA_${contadorDocs}" placeholder="p. ej. 2018-15-20" value="${listaDocumentos.fecha}" pattern="fecha" required/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_FECHA_${contadorDocs}" id="spanDOCUMENTO_FECHA_${contadorDocs}">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                        </div>
                                        <div class="grid-x grid-margin-x grid-padding-y text-right">
                                            <fieldset class="cell medium-12">
                                                <a class="" id="btnBorrarDocumentoSeleccionado"><i class="material-icons secondary" onclick="borrarDocumentoSeleccionado(${contadorDocs}, '${listaDocumentos.id}')">remove_circle</i> </a>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <c:set var="contadorDocs" value="${contadorDocs+1}"></c:set>
                                </c:forEach>
                                <input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="${servidorPublico.documentos.size()-1}">
                            </c:when>
                            <c:otherwise>
                                <div id="divContenedorSinDocumentosPorDefecto">
                                    <div id="documento0" >
                                        <div class="grid-margin-x grid-x">
                                            <label class="cell medium-4">Identificador &uacute;nico
                                                <span style="color:red;">**</span>
                                                <input type="text" name="txtDOCUMENTO_ID_0" id="txtDOCUMENTO_ID_0" placeholder="p. ej. 1" value="" onfocus="desbloquearCamposDocumento(0)" />
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_ID_0" id="spanDOCUMENTO_ID_0">
                                                    Verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">Tipo
                                                <select name="txtDOCUMENTO_TIPO_0" id="txtDOCUMENTO_TIPO_0" disabled>
                                                    <option value="">--Elige una opci&oacute;n--</option>
                                                    <c:forEach items="${tiposDeDocumento}" var="documento">
                                                        <option value="${documento.clave}" >${documento.valor}</option>
                                                    </c:forEach>
                                                </select>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_TIPO_0" id="spanDOCUMENTO_TIPO_0">
                                                    Elige una opci&oacute;n.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">T&iacute;tulo
                                                <span style="color: red">*</span>
                                                <input type="text" name="txtDOCUMENTO_TITULO_0" id="txtDOCUMENTO_TITULO_0" placeholder="p. ej. Constancia de sanci&oacute;n..." value="" disabled/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_TITULO_0" id="spanDOCUMENTO_TITULO_0">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">Descripci&oacute;n
                                                <span style="color: red">*</span>
                                                <input type="text" name="txtDOCUMENTO_DESCRIPCION_0" id="txtDOCUMENTO_DESCRIPCION_0" placeholder="p. ej. Descripci&oacute;n del documento" value="" disabled/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_DESCRIPCION_0" id="spanDOCUMENTO_DESCRIPCION_0">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">URL
                                                <span style="color: red">*</span>
                                                <input type="text" name="txtDOCUMENTO_URL_0" id="txtDOCUMENTO_URL_0" placeholder="p. ej. www.url.com" value="" pattern="url" disabled/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_URL_0" id="spanDOCUMENTO_URL_0">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                            <label class="cell medium-4">Fecha
                                                <span style="color: red">*</span>
                                                <input type="date" name="txtDOCUMENTO_FECHA_0" id="txtDOCUMENTO_FECHA_0" placeholder="p. ej. 2018-15-20" value="" pattern="fecha" disabled/>
                                                <span class="form-error" data-form-error-for="txtDOCUMENTO_FECHA_0" id="spanDOCUMENTO_FECHA_0">
                                                    Campo requerido, verifica el formato de tu registro.
                                                </span>
                                            </label>
                                        </div>
                                        <div class="grid-x grid-margin-x grid-padding-y text-right">
                                            <fieldset class="cell medium-12">
                                                <a class="" id="btnBorrarDocumentoSeleccionado"><i class="material-icons secondary" onclick="borrarDocumentoSeleccionado(0, '')">remove_circle</i> </a>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="0">
                            </c:otherwise>
                        </c:choose>

                        <div id="nuevosDocumentos"></div>
                    </div>
                </fieldset>
                <div class="grid-x grid-margin-x grid-padding-y text-right">
                    <fieldset class="cell medium-12">
                        <a class="" id="btnNuevoDocumento"><i class="material-icons secondary">add_circle</i></a>
                    </fieldset>
                </div>
                <div class="grid-x grid-margin-x">
                    <fieldset class="cell medium-6">
                        <input type="reset" class="button expanded secondary" name="btnReset" value="Restaurar"/>
                    </fieldset>
                    <fieldset class="cell medium-6">
                        <input type="submit" class="button expanded" name="btnRegistrar" value="Modificar datos"/>
                    </fieldset>
                </div>
                <div data-abide-error="data-abide-error" class="alert callout alertFooter" style="display: none;">
                    <p>
                        <i class="material-icons">warning</i>
                        Existen algunos errores en tu registro. Verifica nuevamente.
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="/piePagina.jsp" %>

<script>

    function validarSeleccionChecks(idFormulario)
    { 
        $("#frmRegistrar").on("formvalid.zf.abide", function (ev, frm)
        {
            let checkBoxTipoSancion;                                
            let bolCheckBoxTipoSancionSeleccionado;                 
            
            checkBoxTipoSancion = document.querySelectorAll('input[name="chckTIPO_SANCION"]:checked');
            bolCheckBoxTipoSancionSeleccionado = false;
            
            if (checkBoxTipoSancion.length === 0)
            { 
                $("#spanTIPO_SANCION").css("display", "block");
                $(".alertFooter").css("display", "block");
            } 
            else
            { 
                $("#spanTIPO_SANCION").css("display", "none");
                bolCheckBoxTipoSancionSeleccionado = true;
            } 
            
            if (bolCheckBoxTipoSancionSeleccionado === true)
            { 
                if (validarFechas("txtFECHA_INICIAL", "txtFECHA_FINAL"))
                { 
                    if (validarDocumento())
                    { 
                        swal({
                            title: "Confirmar",
                            text: "\u00bf Los datos son correctos?",
                            icon: "warning",
                            buttons: ["Cancelar", "Aceptar"],
                            dangerMode: true,
                            closeOnClickOutside: false,
                            closeOnEsc: false
                        })
                        .then((seAcepta) => {
                            if (seAcepta)
                            { 
                                document.getElementById(idFormulario).submit();
                            } 
                            else
                            { 
                                swal("Se ha cancelado la operaci\u00f3n", {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000
                                });
                                return false;
                            } 
                        });
                    } 
                } 
                else
                { 
                    $("#spanFECHA_INICIAL").css("display", "block");
                    $("#spanFECHA_FINAL").css("display", "block");
                    swal({
                        title: 'Oops...',
                        text: 'Verifique que la fecha final sea menor a la fecha inicial en los campos utilizados para identificar el plazo de inhabilitaci\u00f3n.',
                        icon: 'warning',
                        button: 'ok'
                    });
                    return false;
                } 
            } 
            else
            {
                return false;
            }
            
        });
    } 

    function validarDocumento()
    { 
        
        var contador = $('#txtDOCUMENTO_NUM').val();

        if (contador === '0')
        {
            var id = $.trim($('#txtDOCUMENTO_ID_0').val());
            var tipo = $.trim($('#txtDOCUMENTO_TIPO_0').val());
            var titulo = $.trim($('#txtDOCUMENTO_TITULO_0').val());
            var descripcion = $.trim($('#txtDOCUMENTO_DESCRIPCION_0').val());
            var url = $.trim($('#txtDOCUMENTO_URL_0').val());
            var fecha = $.trim($('#txtDOCUMENTO_FECHA_0').val());

            if (id.length > 0 || (tipo !== null && tipo.length > 0) || titulo.length > 0 || descripcion.length > 0 || url.length > 0 || fecha.length > 0)
            {
                document.getElementById("txtDOCUMENTO_NUM").value = '1';
            } else
            {
                document.getElementById("txtDOCUMENTO_NUM").value = '0';
            }
        }
        
        return true;
        
    } 

    function validarDatalist(comboBox, list, span)
    {
        var val = $("#" + comboBox).val();
        var obj = $("#" + list).find("option[value='" + val + "']");
        if (obj !== null && obj.length > 0)
        {
            $("#" + span).css("display", "none");
            return true;
        } else
        {
            $("#" + span).css("display", "block");
            return false;
        }
    }

    function validarFechas(fechaIni, fechaFin)
    { 
        
        let valFechaInicial;                
        let valFechaFinal;                  
        let tiempoInicial;                  
        let tiempoFinal;                    
        
        valFechaInicial = document.getElementById(fechaIni).value;
        valFechaFinal = document.getElementById(fechaFin).value;
        
        if (valFechaInicial === "" && valFechaFinal === "")
        { 
            return true;
        } 
        else
        { 
            
            tiempoInicial = new Date(valFechaInicial).getTime();
            tiempoFinal = new Date(valFechaFinal).getTime();
            
            if (tiempoInicial <= tiempoFinal)
            { 
                return true;
            } 
            else
            { 
                return false;
            } 
            
        } 
        
    } 

    function validarPlazo()
    { 
        
        let txtPlazo;                   
        let txtUnidad;                  
        let txtFechaInicial;            
        let txtFechaFinal;              
        
        txtPlazo = document.getElementById("txtPLAZO");
        txtUnidad = document.getElementById("cmbUNIDAD_MEDIDA_PLAZO");
        txtFechaInicial = document.getElementById("txtFECHA_INICIAL");
        txtFechaFinal = document.getElementById("txtFECHA_FINAL");

        if (txtPlazo.value !== null && txtPlazo.value.length > 0 && txtPlazo.value > 0)
        { 
            txtUnidad.required = true;
        } 
        else
        { 
            txtUnidad.required = false;
        } 

        if (txtUnidad.value !== null && txtUnidad.value.length > 0)
        { 
            txtPlazo.required = true;
        } 
        else
        { 
            txtPlazo.required = false;
        } 
        
        if (txtFechaInicial.value !== null && txtFechaInicial.value.length > 0)
        { 
           asignarValorMinimoFechaFinal(txtFechaInicial.value);
        } 
        else
        { 
            txtFechaFinal.required = false;
            txtFechaFinal.min = "";
        } 
        
        if(txtFechaFinal.value !== null && txtFechaFinal.value.length > 0)
        { 
            txtFechaInicial.required = true;
        } 
        else
        { 
            txtFechaInicial.required = false;
        } 

    } 
    
    function validarPlazoInformacionPrevia(plazoPrevio, unidadPrevia, fechaInicialPrevia, fechaFinalPrevia)
    {
        
        let txtPlazo;                   
        let txtUnidad;                  
        let txtFechaInicial;            
        let txtFechaFinal;              
        
        txtPlazo = document.getElementById("txtPLAZO");
        txtUnidad = document.getElementById("cmbUNIDAD_MEDIDA_PLAZO");
        txtFechaInicial = document.getElementById("txtFECHA_INICIAL");
        txtFechaFinal = document.getElementById("txtFECHA_FINAL");
        
        if (plazoPrevio !== null && plazoPrevio.length > 0 && plazoPrevio > 0)
        { 
            txtUnidad.required = true;
        } 
        else
        { 
            txtUnidad.required = false;
        } 

        if (unidadPrevia !== null && unidadPrevia.length > 0)
        { 
            txtPlazo.required = true;
        } 
        else
        { 
            txtPlazo.required = false;
        } 
        
        if(fechaFinalPrevia !== null && fechaFinalPrevia.length > 0)
        { 
            txtFechaInicial.required = true;
        } 
        else
        { 
            txtFechaInicial.required = false;
        } 
        
        if (fechaInicialPrevia !== null && fechaInicialPrevia.length > 0)
        { 
            asignarValorMinimoFechaFinal(fechaInicialPrevia);
        } 
        else
        { 
            txtFechaFinal.required = false;
            txtFechaFinal.min = "";
        } 
        
    }
    
    function validarSiFueSeleccionado(idCheckbox, idInput)
    { 
        
        let check;          
        let input;          
        
        check = document.getElementById(idCheckbox);
        input = document.getElementById(idInput);
        
        if (check.checked)
        { 
            input.disabled = false;
        } 
        else
        { 
            input.value = '';
            input.disabled = true;
        } 
        
    } 

    function estructuraDocumento(numeroDeDocumentos)
    { 
        
        let documento = '';             
        
        documento += '<div id="documento' + numeroDeDocumentos + '" >';
        documento += '<hr>';
        documento += '<div class="grid-margin-x grid-x">';
        documento += '  <label class="cell medium-4">Identificador &uacute;nico';
        documento += '      <span style="color:red;">**</span>';
        documento += '      <input type="text" name="txtDOCUMENTO_ID_' + numeroDeDocumentos + '" id="txtDOCUMENTO_ID_' + numeroDeDocumentos + '" placeholder="p. ej. 1" value="" ' + ' onfocus="desbloquearCamposDocumento('+numeroDeDocumentos+')"/>';
        documento += '       <span class="form-error" data-form-error-for="txtDOCUMENTO_ID_' + numeroDeDocumentos + '" id="spanDOCUMENTO_ID_' + numeroDeDocumentos + '">';
        documento += '         Campo requerido, verifica el formato de tu registro.';
        documento += '       </span>';
        documento += '   </label>';
        documento += '  <label class="cell medium-4">Tipo';
        documento += '      <select name="txtDOCUMENTO_TIPO_' + numeroDeDocumentos + '" id="txtDOCUMENTO_TIPO_' + numeroDeDocumentos + '" disabled >';
        documento += '          <option value="">--Elige una opci&oacute;n--</option>';
    <c:forEach items="${tiposDeDocumento}" var="documento">
        documento += '<option value="${documento.clave}">${documento.valor}</option>';
    </c:forEach>
        documento += '      </select>';
        documento += '      <span class="form-error" data-form-error-for="txtDOCUMENTO_TIPO_' + numeroDeDocumentos + '" id="spanDOCUMENTO_TIPO_' + numeroDeDocumentos + '">';
        documento += '         Campo requerido, elige una opci&oacute;n.';
        documento += '      </span>';
        documento += '  </label>';
        documento += '  <label class="cell medium-4">T&iacute;tulo';
        documento += '      <span style="color: red">*</span>';
        documento += '      <input type="text" name="txtDOCUMENTO_TITULO_' + numeroDeDocumentos + '" id="txtDOCUMENTO_TITULO_' + numeroDeDocumentos + '" placeholder="p. ej. Constancia de sanci&oacute;n..." value="" disabled/>';
        documento += '       <span class="form-error" data-form-error-for="txtDOCUMENTO_TITULO_' + numeroDeDocumentos + '" id="spanDOCUMENTO_TITULO_' + numeroDeDocumentos + '">';
        documento += '         Campo requerido, verifica el formato de tu registro.';
        documento += '       </span>';
        documento += '  </label>';
        documento += '  <label class="cell medium-4">Descripci&oacute;n';
        documento += '      <span style="color: red">*</span>';
        documento += '      <input type="text" name="txtDOCUMENTO_DESCRIPCION_' + numeroDeDocumentos + '" id="txtDOCUMENTO_DESCRIPCION_' + numeroDeDocumentos + '" placeholder="p. ej. Descripci&oacute;n del documento" value=""/ disabled>';
        documento += '       <span class="form-error" data-form-error-for="txtDOCUMENTO_DESCRIPCION_' + numeroDeDocumentos + '" id="spanDOCUMENTO_DESCRIPCION_' + numeroDeDocumentos + '">';
        documento += '         Campo requerido, verifica el formato de tu registro.';
        documento += '       </span>';
        documento += '  </label>';
        documento += '  <label class="cell medium-4">URL';
        documento += '      <span style="color: red">*</span>';
        documento += '      <input type="text" name="txtDOCUMENTO_URL_' + numeroDeDocumentos + '" id="txtDOCUMENTO_URL_' + numeroDeDocumentos + '" placeholder="p. ej. www.url.com" value="" pattern="url" disabled/>';
        documento += '       <span class="form-error" data-form-error-for="txtDOCUMENTO_URL_' + numeroDeDocumentos + '" id="spanDOCUMENTO_URL_' + numeroDeDocumentos + '">';
        documento += '           Campo requerido, verifica el formato de tu registro.';
        documento += '       </span>';
        documento += '  </label>';
        documento += '  <label class="cell medium-4">Fecha';
        documento += '      <span style="color: red">*</span>';
        documento += '      <input type="date" name="txtDOCUMENTO_FECHA_' + numeroDeDocumentos + '" id="txtDOCUMENTO_FECHA_' + numeroDeDocumentos + '" placeholder="p. ej. 2018-15-20" value="" pattern="fecha" disabled/>';
        documento += '       <span class="form-error" data-form-error-for="txtDOCUMENTO_FECHA_' + numeroDeDocumentos + '" id="spanDOCUMENTO_FECHA_' + numeroDeDocumentos + '">';
        documento += '         Campo requerido, verifica el formato de tu registro.';
        documento += '       </span>';
        documento += '  </label>';
        documento += '</div>';
        documento += '<div class="grid-x grid-margin-x grid-padding-y text-right">';
        documento += '  <fieldset class="cell medium-12">';
        documento += '      <span data-tooltip class="top" tabindex="2" title="Borrar los campos del ultimo documento" style="border: none;">';
        documento += '          <a class="" id="btnBorrarDocumentoSeleccionado"><i class="material-icons secondary" onclick="borrarDocumentoSeleccionado(' + numeroDeDocumentos + ', '+ "''" +')">remove_circle</i> </a>';
        documento += '      </span>';
        documento += '  </fieldset>';
        documento += '</div>' ;
        documento += '</div>';
        
        return documento;
        
    }

    $(document).ready(function () {
        
        var informacionPreviaFechaInicial = document.getElementById("txtFECHA_INICIAL").value;
        var informacionPreviaFechaFinal = document.getElementById("txtFECHA_FINAL").value;
        var informacionPreviaPlazo = document.getElementById("txtPLAZO").value;
        var informacionPreviaUnidadPlazo = document.getElementById("cmbUNIDAD_MEDIDA_PLAZO").value;
        
        $('#btnNuevoDocumento').click(function () {
            
            var numeroDeDocumentos = parseInt($('#txtDOCUMENTO_NUM').val()); 
            var listaIdentificadoresDocumentosYaAgregados;
            var blnContieneDatos;
            
            var id = '';
            var tipo = '';
            var titulo = '';
            var descripcion = '';
            var url = '';
            var fecha = '';
            
            if(numeroDeDocumentos < 0)
            { 
                
                var strEstructuraSeccionDocumento;
                
                
                $("#divContenedorDocumentos").html("");
                       
                strEstructuraSeccionDocumento = estructuraDocumento(0);
                strEstructuraSeccionDocumento = strEstructuraSeccionDocumento + '<input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="0">';
                strEstructuraSeccionDocumento = strEstructuraSeccionDocumento + '<div id="nuevosDocumentos"></div>'
                
                $("#divContenedorDocumentos").append(strEstructuraSeccionDocumento); 
                
                $("#listaDeIdentificadoresDeDocumentosARegistrar").val("0,");
            } 
            else
            { 
                listaIdentificadoresDocumentosYaAgregados = $("#listaDeIdentificadoresDeDocumentosARegistrar").val().split(",");
                blnContieneDatos = false;
                for(var i = 0; i < listaIdentificadoresDocumentosYaAgregados.length; i++)
                { 
                    if(Number.isInteger(parseInt(listaIdentificadoresDocumentosYaAgregados[i])))
                    { 
                        id = $('#txtDOCUMENTO_ID_' + listaIdentificadoresDocumentosYaAgregados[i]).val();
                        tipo = $('#txtDOCUMENTO_TIPO_' + listaIdentificadoresDocumentosYaAgregados[i]).val();
                        titulo = $('#txtDOCUMENTO_TITULO_' + listaIdentificadoresDocumentosYaAgregados[i]).val();
                        descripcion = $('#txtDOCUMENTO_DESCRIPCION_' + listaIdentificadoresDocumentosYaAgregados[i]).val();
                        url = $('#txtDOCUMENTO_URL_' + listaIdentificadoresDocumentosYaAgregados[i]).val();
                        fecha = $('#txtDOCUMENTO_FECHA_' + listaIdentificadoresDocumentosYaAgregados[i]).val();   
                        
                        if (id.length > 0)
                        { 
                            if (tipo.length >= 0 && titulo.length > 0 && descripcion.length > 0 && url.length > 0 && fecha.length > 0)
                            { 
                                blnContieneDatos = true;
                            } 
                            else
                            { 
                                blnContieneDatos = false;
                                break;
                            } 
                        } 
                        else
                        { 
                            blnContieneDatos = false;
                            break;
                        } 
                    } 
                } 
                
                if (blnContieneDatos)
                { 
                    var incremento = 0;
                    var documento = null;

                    incremento = numeroDeDocumentos + 1;
                    documento = estructuraDocumento(incremento);
                    $("#nuevosDocumentos").append(documento);
                    $("#txtDOCUMENTO_NUM").val(incremento);
                    
                    
                    $("#listaDeIdentificadoresDeDocumentosARegistrar").val($("#listaDeIdentificadoresDeDocumentosARegistrar").val() + incremento + ",");
                } 
                else
                { 
                    swal({
                        title: 'Error',
                        text: 'Para poder registrar un nuevo documento debes completar todos los campos requeridos de cada documento',
                        icon: 'error',
                        button: 'ok'
                    });                    
                } 
            } 
        });

        crearListaDocumentosPorDefecto();

        $('input[name=btnReset]').click(function (){ 
            
            $("#divContenedorDocumentos").html("");            
            for(i = 0; i < arregloCodigoDocumentosPorDefecto.length; i++)
            { 
                $("#divContenedorDocumentos").append(arregloCodigoDocumentosPorDefecto[i]);
            } 
            
            $("#divContenedorDocumentos").append('<input type="hidden" name ="txtDOCUMENTO_NUM" id="txtDOCUMENTO_NUM" value="' + (arregloCodigoDocumentosPorDefecto.length - 1) + '">');                
            
            $("#divContenedorDocumentos").append('<div id="nuevosDocumentos"></div>');
            
            $("#listaDeIdentificadoresDeDocumentosARegistrar").val($("#listaDeIdentificadoresDeDocumentosPorDefecto").val());
        
            validarPlazoInformacionPrevia(informacionPreviaPlazo, informacionPreviaUnidadPlazo, informacionPreviaFechaInicial, informacionPreviaFechaFinal);
            
        });
        
    });
  
    function asignarValorMinimoFechaFinal(fechaInicial)
    { 
        
        let txtPlazo;                                           
        let txtUnidad;                                          
        let txtFechaFinal;                                      
        let bolExisteInformacionPlazoUnidad;                    
        let arregloValorFechaInicial;                           
        let valorFechaFinal;                                    
        let anio;                                               
        let mes;                                                
        let dia;                                                
        let bolFechaInicialCorrecta;
        
        txtPlazo = document.getElementById("txtPLAZO");
        txtUnidad = document.getElementById("cmbUNIDAD_MEDIDA_PLAZO");
        txtFechaFinal = document.getElementById('txtFECHA_FINAL');
        bolExisteInformacionPlazoUnidad = false;
        bolFechaInicialCorrecta = false;
        
        if(fechaInicial !== null && fechaInicial.length > 0)
        { 
            if(fechaInicial.includes("-") === true)
            { 
                arregloValorFechaInicial = fechaInicial.split('-');
                if(arregloValorFechaInicial.length === 3)
                { 
                    bolFechaInicialCorrecta = true;
                } 
            } 
        } 
        
        if (txtPlazo.value !== null && txtPlazo.value.length > 0 && txtPlazo.value > 0)
        { 
            if (txtUnidad.value !== null && txtUnidad.value.length > 0)
            { 
                bolExisteInformacionPlazoUnidad = true;
            } 
        } 

        if(bolFechaInicialCorrecta === true)
        { 
            if(bolExisteInformacionPlazoUnidad === true)
            { 
            
                anio = arregloValorFechaInicial[0];
                mes = arregloValorFechaInicial[1] - 1;
                dia = arregloValorFechaInicial[2];

                valorFechaFinal = new Date(anio,mes,dia);

                switch(txtUnidad.value)
                { 
                    case 'DIAS':
                        valorFechaFinal.setDate(valorFechaFinal.getDate() + parseInt(txtPlazo.value,10));
                        break;
                    case 'SEMS': 
                        valorFechaFinal.setDate(valorFechaFinal.getDate() + (parseInt(txtPlazo.value,10) * 7));
                        break;
                    case 'MESS': 
                        valorFechaFinal.setMonth(valorFechaFinal.getMonth() + parseInt(txtPlazo.value,10));
                        break;
                    case 'ANIO': 
                        valorFechaFinal.setFullYear(valorFechaFinal.getFullYear() + parseInt(txtPlazo.value,10));
                        break;
                } 

                anio = valorFechaFinal.getFullYear();
                mes = ((valorFechaFinal.getMonth() + 1) < 10) ? '0' + (valorFechaFinal.getMonth() + 1) : valorFechaFinal.getMonth() + 1;
                dia = (valorFechaFinal.getDate() < 10) ? '0' + valorFechaFinal.getDate() : valorFechaFinal.getDate();

                txtFechaFinal.value = anio + '-' + mes + '-' + dia;
                txtFechaFinal.min = anio + '-' + mes + '-' + dia;
                txtFechaFinal.required = true;
            } 
            else
            { 
                txtFechaFinal.min = fechaInicial;
                txtFechaFinal.required = true;
            } 
            
        } 
        else
        { 
            txtFechaFinal.min = "";
            txtFechaFinal.required = false;
        } 
        
    } 
    
</script>