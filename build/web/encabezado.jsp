<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<!DOCTYPE html>
<html lang="es-Es">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>Plataforma Digital Estatal</title>

        <meta charset="ISO-8859-1" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <link rel="shortcut icon" href="framework/img/png/favicon.png" />
        <link rel="icon" href="framework/img/png/favicon_32.png" sizes="32x32" />
        <link rel="icon" href="framework/img/png/favicon_192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="framework/img/png/favicon_180.png">
        <meta name="msapplication-TileImage" content="framework/img/png/favicon_270.png">
        <link rel="stylesheet" href="style.min.css" />

    </head>

    <body>

        <div>
        </div>
        <div data-sticky-container id="barrasup" class="hide-for-print">
            <div class="title-bar" data-responsive-toggle="responsive-menu" data-hide-for="medium">
                <button class="menu-icon" type="button" data-toggle="responsive-menu"></button>
                <div class="title-bar-title">Men&uacute;</div>
            </div>
            <div class="top-bar sup" id="responsive-menu" data-sticky data-options="marginTop:0;">
                <div class="top-bar-left"> <img src="framework/img/svg/logo-pde-standar.svg" class="escudo" />
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li><a href="https://sesaemm.gob.mx/"></a></li>
                    </ul>
                </div>
                <div class="top-bar-right">
                    <a onclick="document.frmPrincipal.submit()"><img src="framework/img/svg/logo-standar.svg" class="logoPDE"></a>
                    <ul class="display-inline-block menu padding-left-1">
                        <li class="bienvenida display-inline-block padding-right-1">
                            <a data-toggle="dropdown-user-details">Bienvenid@ <strong>${usuario.nombre} ${usuario.primer_apellido} ${usuario.segundo_apellido}</strong></a>

                            <%@include file="/frmInfoUsuario.jsp" %>

                        </li>
                        <li class="display-inline-block">
                            <form name="frmSalir" action="/sistemaCaptura${initParam.versionIntranet}/intranet" method="post">
                                <input type="hidden" name="accion" value="cerrarSesion"/>
                                <input type="submit" class="button hollow" value="Salir"/>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
