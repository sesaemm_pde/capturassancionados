<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-queEs.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="cell large-12 display-top">

            <h1>&iquest;Qu&eacute; es la Plataforma Digital Estatal?</h1>

        </div>
        <div class="cell medium-4">
            <img src="framework/img/png/queEs.png" class="thumbnail" alt="Que es la PDE"/>
        </div>
        <div class="cell medium-8 grid-container">
            <p style="text-align: justify;">La Plataforma Digital Estatal es una fuente de inteligencia para construir integridad y combatir la corrupci&oacute;n, que crear&aacute; valor para el gobierno y la sociedad, a partir de grandes cantidades de datos.</p>
            <p style="text-align: justify;">La Plataforma es un medio para el intercambio de datos anticorrupci&oacute;n del Gobierno, que busca quitar barreras y romper silos de informaci&oacute;n para que los datos sean comparables, accesibles y utilizables, empezando con seis sistemas de
                datos prioritarios.</p>
        </div>
        <div class="cell large-12">

            <h1>Sistemas de la PDE</h1>

            <div class="grid-x grid-margin-x" data-equalizer="data-equalizer" data-equalize-on="medium" id="test-eq" style="display: flex; justify-content: center;">
                <c:set value="false" scope="request" var="estaAsignado"/>
                <c:forEach items="${usuario.sistemas}" var="sistema">
                    <c:if test="${sistema.sistema.id == 3}">
                        <c:set value="true" scope="request" var="estaAsignado"/>
                    </c:if>
                </c:forEach>
                <div class="cell medium-4">
                    <div class="cell">
                        <c:choose >
                            <c:when test="${estaAsignado}">
                                <div class="card" data-equalizer-watch="data-equalizer-watch">
                                    <a onclick="frmSancionados.submit()">
                                        <img src="framework/img/svg/s3.svg" width="200" class="display-top"/></a>
                                    <div class="card-section">
                                        <h4>
                                            <a onclick="frmSancionados.submit()">III.
                                                <small>Servidores p&uacute;blicos sancionados.</small>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="card" data-equalizer-watch="data-equalizer-watch">
                                    <a onclick="" class="deshabilitado">
                                        <img src="framework/img/svg/s3.svg" width="200" class="display-top"/></a>
                                    <div class="card-section">
                                        <h4>
                                            <a onclick="" class="deshabilitado">III.
                                                <small>Servidores p&uacute;blicos y particulares sancionados.</small>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>

            <h1>Objetivos</h1>

            <p>Usar nuevas tecnolog&iacute;as, metodolog&iacute;as de trabajo, ciencia de datos e inteligencia artificial como insumos y apoyo al trabajo de las autoridades del Sistema Anticorrupci&oacute;n del Estado de M&eacute;xico y Municipios para:</p>
            <ul>
                <li>Analizar, predecir y alertar a las autoridades sobre posibles riesgos de corrupci&oacute;n</li>
                <li>Automatizar procesos, evitar discrecionalidad, colusi&oacute;n y conflicto de inter&eacute;s</li>
                <li>Promover el uso de los datos para respaldar sanciones y como evidencia para combatir la impunidad</li>
                <li>Dar seguimiento, en tiempo real, a los procesos y proyectos de contrataci&oacute;n p&uacute;blica, asegurar el cumplimiento de sus objetivos y garantizar una mayor eficiencia en las compras p&uacute;blicas</li>
                <li>Apoyar la participaci&oacute;n ciudadana, poniendo al ciudadano al centro del combate a la corrupci&oacute;n</li>
                <li>Incorporar informaci&oacute;n sobre indicadores para evaluar la Pol&iacute;tica Nacional Anticorrupci&oacute;n y el fen&oacute;meno en M&eacute;xico</li>
                <li>Dar evidencia para generar recomendaciones de pol&iacute;tica p&uacute;blica a las autoridades del Sistema Anticorrupci&oacute;n del Estado de M&eacute;xico y Municipios</li>
            </ul>

            <h1>Plataforma de Interoperabilidad</h1>

            <p>Con la PDE, las instituciones del Gobierno continuar&aacute;n generando sus propios datos, que ahora deber&aacute;n ser estandarizados y distribuidos para ser consultados desde la Plataforma.</p>
            <p>Con esto, la PDE permitir&aacute; el intercambio y consulta de datos eficiente con autoridades y ciudadan&iacute;a, cuidando en todo momento la seguridad e integridad de la informaci&oacute;n.</p>

            <img src="framework/img/svg/PDE.svg" class="thumbnail" alt="PDE"/>

            <h1>&iquest;C&oacute;mo se construye la PDE?</h1>

        </div>
        <div class="cell medium-6">
            <h2>Principios</h2>

            <ul>
                <li>Dise&ntilde;o centrado en los y las usuarias y sus necesidades</li>
                <li>Construcci&oacute;n gradual, modular, escalable, &aacute;gil y flexible</li>
                <li>Datos interoperables y abiertos</li>
                <li>Seguridad de la informaci&oacute;n y protecci&oacute;n de datos personales</li>
                <li>Creaci&oacute;n de impacto y entrega de valor p&uacute;blico en el centro</li>
            </ul>
        </div>
        <div class="cell medium-6">
            <h2>Ejes de trabajo</h2>

            <img src="framework/img/svg/ejesTrabajo.svg" class="thumbnail" alt="Ejes de trabajo"/>
        </div>
        <div class="cell large-12">
            <h1>Impacto de los datos para combatir la corrupci&oacute;n</h1>

            <p>
                <a href="https://www.fuistetu.org/" target="_blank">Fuiste T&uacute;</a>
                usa datos de auditor&iacute;as para fomentar la trazabilidad en el uso de recursos p&uacute;blicos.</p>
            <p>
                <a href="https://imco.org.mx/articulo_es/indice-riesgos-corrupcion-sistema-mexicano-contrataciones-publicas/" target="_blank">Imco y OPI</a>
                utilizaron datos de contrataciones para identificar riesgos e impulsar una cultura de prevenci&oacute;n en contrataciones.</p>
            <p>
                <a href="https://1560000.org/explora" target="_blank">Data C&iacute;vica</a>
                utiliz&oacute; datos abiertos para reconstruir las declaraciones patrimoniales de servidores p&uacute;blicos del gobierno.</p>
            <p>
                <a href="https://www.animalpolitico.com/estafa-maestra/#datos" target="_blank">Animal Pol&iacute;tico</a>
                utiliz&oacute; datos de auditor&iacute;as y licitaciones para detectar desv&iacute;os de recursos, algo que ha sido utilizado como insumo en el proceso para sancionar a posibles involucrados.</p>

        </div>
    </div>
</div>

<%@include file="piePagina.jsp" %>