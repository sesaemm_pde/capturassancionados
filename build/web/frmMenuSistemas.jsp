<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-sis-min.png" alt="PDE"/>
        </li>
    </ul>
</div>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1 class="text-center">Plataforma Digital Estatal</h1>
            <h4 class="text-center">Sistema asignado</h4>
            <hr/>
            <div class="grid-margin-x grid-margin-y grid-x" style="display: flex; justify-content: center;">
                <c:forEach items="${sistemas}" var="sist">
                    <c:if test="${sist.sistema.id == 3}">
                        <c:choose>
                            <c:when test="${sist.sistema.activo == 1}">
                                <form name="frmSis${sist.sistema.id}" method="post" action="/sistemaCaptura${initParam.versionIntranet}${sist.sistema.contexto}">
                                    <input type="hidden" name="accion" value="ingresarSistema"/>
                                </form>
                                <a class="cell dashboard-nav-card medium-6" onclick="frmSis${sist.sistema.id}.submit()">
                                    <i class=" material-icons dashboard-nav-card-icon">settings_applications</i>
                                    <h4 class="dashboard-nav-card-title">
                                        Servidores p&uacute;blicos sancionados.
                                    </h4>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a class="cell dashboard-nav-card medium-6 deshabilitado" onclick="">
                                    <i class=" material-icons dashboard-nav-card-icon">settings_applications</i>
                                    <h3 class="dashboard-nav-card-title">Servidores p&uacute;blicos sancionados.</h3>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </c:forEach>
            </div>
        </div>
    </div>
</div>

<%@include file="piePagina.jsp" %>