<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-uso.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Aceptaci&oacute;n de t&eacute;rminos de uso de plataformadigitalnacional.org</h1>
            <p>El uso del plataformadigitalnacional.org o de cualquiera de sus componentes, implica la aceptaci&oacute;n expresa de los presentes &quot;T&eacute;rminos y Condiciones&quot;.</p>
            <h1>Sobre el uso de plataformadigitalnacional.org</h1>
            <p>Son obligaciones del usuario:</p>
            <ul>
                <li>
                    <p>No da&ntilde;ar, inutilizar o deteriorar los sistemas inform&aacute;ticos que puedan ser incorporados en este sitio, incluido el portal plataformadigitalnacional.org.</p>
                </li>
                <li>
                    <p>No modificar de ninguna manera los sistemas inform&aacute;ticos que puedan ser incorporados.</p>
                </li>
                <li>
                    <p>No utilizar versiones de sistemas modificados con el fin de obtener acceso no autorizado a cualquier sistema de informaci&oacute;n, contenido y/o servicios del portal.</p>
                </li>
                <li>
                    <p>No interferir ni interrumpir el acceso, funcionalidad y utilizaci&oacute;n del portal, servidores o redes conectados al mismo.</p>
                </li>
                <li>
                    <p>Los enlaces que son proporcionados en el sitio son s&oacute;lo con fines informativos, por lo que los contenidos o recursos de esos sitios de internet o p&aacute;ginas gubernamentales, ser&aacute; responsabilidad exclusiva de los entes p&uacute;blicos , por lo que la autor&iacute;a
                        y reconocimiento de la misma, es responsabilidad del propio ente p&uacute;blico. Lo anterior sin perjuicio de lo que establece la Ley Federal del Derecho de Autor y dem&aacute;s normatividad aplicable.</p>
                </li>
                <li>
                    <p>Este sitio contiene medidas de seguridad para proteger la informaci&oacute;n de cualquier alteraci&oacute;n realizada por terceros.</p>
                </li>
                <li>
                    <p>La Secretar&iacute;a Ejecutiva del Sistema Nacional Anticorrupci&oacute;n se deslinda de cualquier responsabilidad, perjuicio o da&ntilde;o que pueda generar el usuario por cualquier uso inadecuado del portal o la informaci&oacute;n contenida en plataformadigitalnacional.org.</p>
                </li>
                <li>
                    <p>El usuario se obliga a hacer buen uso del sitio, respetando la Ley General del Sistema Nacional Anticorrupci&oacute;n, la Ley Federal de Derechos de Autor y dem&aacute;s normatividad aplicable.</p>
                </li>
                <li>
                    <p>Debe contar con un equipo que cumpla con las caracter&iacute;sticas m&iacute;nimas necesarias para navegar en el sitio, recomendando su navegaci&oacute;n en las &uacute;ltimas versiones de los navegadores Google Chrome, Mozilla Firefox y Safari para obtener la mejor
                        experiencia.</p>
                </li>
                <li>
                    <p>Exime a la Secretar&iacute;a Ejecutiva del Sistema Nacional Anticorrupci&oacute;n de toda responsabilidad por los da&ntilde;os que el uso del sitio le pudieran ocasionar en forma incidental o consecuente con su equipo, informaci&oacute;n, patrimonio o persona, as&iacute; como
                        ninguna responsabilidad por la la alteraci&oacute;n o manipulaci&oacute;n de los datos una vez publicados en &eacute;l.</p>
                </li>
                <li>
                    <p>Acepta y se obliga a utilizar el sitio para fines l&iacute;citos y con apego a las disposiciones legales aplicables.</p>
                </li>
                <li>
                    <p>El destino y tratamiento de los datos que se obtengan de la plataforma, son responsabilidad exclusivamente del usuario, y de manera alguna se&ntilde;alar&aacute;n el posicionamiento de ning&uacute;n ente p&uacute;blico, salvo que expresamente se refiera.</p>
                </li>
            </ul>
            <h1>Del libre uso de los datos</h1>
            <p>Los presentes &quot;T&eacute;rminos de libre uso&quot; promueven el uso, re&uacute;so y redistribuci&oacute;n de los conjuntos de datos abiertos, de conformidad con lo siguiente:</p>
            <p>Usted puede:</p>
            <ul>
                <li>
                    <p>Hacer y distribuir copias del conjunto de datos y su contenido;</p>
                </li>
                <li>
                    <p>Difundir y publicar el conjunto de datos y su contenido;</p>
                </li>
                <li>
                    <p>Adaptar o reordenar el conjunto de datos y su contenido;</p>
                </li>
                <li>
                    <p>Extraer total o parcialmente el contenido del conjunto de datos;</p>
                </li>
                <li>
                    <p>Explotar comercialmente el conjunto de datos y su contenido, y;</p>
                </li>
                <li>
                    <p>Crear conjuntos de datos derivados del conjunto de datos o su contenido.</p>
                </li>
            </ul>
            <p>Condiciones:</p>
            <ul>
                <li>
                    <p>Citar la fuente de origen de donde obtuvo el conjunto de datos:</p>
                    <ul>
                        <li>
                            <p>&quot;Nombre del conjunto de datos&quot;, [Siglas de la instituci&oacute;n publicante]; Liga de internet de los datos descargados, y la fecha de la de consulta en formato num&eacute;rico [AAAA-MM-DD], puestos a disposici&oacute;n de tal manera que sean f&aacute;cilmente accesibles
                                para los usuarios, y en la forma que mejor se adecue al funcionamiento del bien o servicio;</p>
                        </li>
                    </ul>
                </li>
                <li>
                    <p>No utilizar la informaci&oacute;n con objeto de enga&ntilde;ar o confundir a la poblaci&oacute;n variando el sentido original de la informaci&oacute;n y su veracidad.</p>
                </li>
                <li>
                    <p>No aparentar que el uso que usted haga de los datos representa una postura.</p>
                </li>
                <li>
                    <p>Estos t&eacute;rminos de libre uso de datos no lo autorizan para utilizar el contenido de terceros como pueden ser obras en cualquier formato que se encuentren dentro de los conjuntos de datos. En caso de que requiera utilizar dicho contenido, deber&aacute;
                        buscar la autorizaci&oacute;n directamente del titular de los derechos correspondientes de conformidad con la Ley Federal de Derechos de Autor.</p>
                </li>
            </ul>
            <h1>Actualizaci&oacute;n de los t&eacute;rminos y condiciones</h1>
            <p>En cualquier momento estos t&eacute;rminos y condiciones pueden cambiar, por lo que te pedimos revises constantemente nuestro portal.</p>
            <h1>Glosario</h1>
            <p>
                <b>plataformadigitalnacional.org:</b>
                El sitio de internet que habilita la operaci&oacute;n de la Plataforma Digital Nacional establecida en el Art&iacute;culo 9 de la Ley General del Sistema Nacional Anticorrupci&oacute;n.<br/>
                <br/>
                <b>Sistemas inform&aacute;ticos:</b>
                componentes tecnol&oacute;gicos de infraestructura y software utilizados para la operaci&oacute;n de la Plataforma Digital Nacional y los establecidos en el art&iacute;culo 49 de la Ley del Sistemas que alimentan la informaci&oacute;n de los subsistemas, conjuntos de datos o
                proveedores, y que en su conjunto conforman la Plataforma Digital Nacional.<br/>
                <br/>
                <b>Subsistemas:</b>
                Conjuntos de datos e informaci&oacute;n concentrados, resguardados, administrados y actualizados por los encargados que alimentan a los sistemas, y que contendr&aacute;n la informaci&oacute;n que establezca la Secretar&iacute;a Ejecutiva para ser interconectada e integrada en
                los sistemas,<br/>
                <br/>
                <b>Usuario:</b>
                Las personas y entes con atribuciones y facultades para hacer uso de los sistemas de la Plataforma, y/o para ejercer derechos o acceder a la informaci&oacute;n, conforme a la normativa aplicable.</p>
            <h1>Propiedad intelectual</h1>
            <p>La informaci&oacute;n del portal plataformadigitalnacional.org es p&uacute;blica a menos que se indique lo contrario, en cuyo caso antes de reproducirla, deber&aacute;s observar si tiene derechos reservados y respetarlos en t&eacute;rminos de las normas relativas a derechos
                de autor y propiedad industrial. Adicionalmente el usuario que reproduzca o procese informaci&oacute;n contenida en gob.mx deber&aacute; referir la localizaci&oacute;n electr&oacute;nica y fecha en que se realiz&oacute; la consulta de la informaci&oacute;n. El usuario se compromete a
                respetar y dejar a salvo los derechos de terceros sobre los contenidos que se contengan en el portal gob.mx, en t&eacute;rminos de la Ley aplicable, por lo que para su uso, usted deber&aacute; de obtener las autorizaciones correspondientes directamente de los
                titulares de los derechos.</p>
        </div>
    </div>
</div>

<%@include file="piePagina.jsp" %>