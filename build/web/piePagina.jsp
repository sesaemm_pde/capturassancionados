<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix= "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix= "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<footer class="footer hide-for-print">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-4 large-4">
                <h1>Sistemas</h1>
                <ul class="menu vertical">
                    <c:forEach items="${usuario.sistemas}" var="sistema">
                        <c:choose>
                            <c:when test="${sistema.sistema.id == 3 && sistema.sistema.activo == 1}">
                                <li>
                                    <a onclick="frmSancionados.submit()" target="_self" >Servidores p&uacute;blicos sancionados</a>
                                </li>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </ul>
                <hr class="show-for-small-only" />
            </div>
            <div class="cell small-12 medium-4 large-4">
                <h1>Acerca del sitio</h1>
                <ul class="menu vertical">
                    <li><a onclick="frmQueEs.submit()" target="_self">&iquest;Qu&eacute; es la PDE?</a></li>
                    <li><a onclick="frmPreguntasFrecuentes.submit()" target="_self">Preguntas frecuentes</a></li>
                    <li><a onclick="frmTerminos.submit()" target="_self">T&eacute;rminos de uso</a></li>
                    <li><a onclick="frmPrivacidad.submit()" target="_self">Aviso de privacidad</a></li>
                </ul>
                <hr class="show-for-small-only" />
            </div>
            <div class="cell small-12 medium-4 large-4">
                <div class="grid-x grid-margin-x">
                    <div class="cell small-12 medium-12 large-12">
                        <p class="logofooter"> <a target="new"><img src="framework/img/svg/logo-white.svg" width="180px" alt="SESAEMM" /></a> </p>
                    </div>
                    <div class="cell small-12 medium-12 large-12">
                        <p class="about">S&iacute;guenos en:</p>
                        <ul class="inline-list social">
                            <a href="//twitter.com/SESAEMM_" target="_blank"><i class></i><img src="framework/img/svg/twitter.svg" alt="" /></a>
                            <a href="//www.youtube.com/channel/UC3-OvI6v3ENWEO89NsalMOg" target="_blank"><i class></i><img src="framework/img/svg/youtube.svg" alt="" /></a>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="cell small-12 display-top padding-1">
                <p class="about">${datosDeLaUltimaActualizacion}</p>
            </div>
        </div>
    </div>
</footer>

<form name="frmPrincipal" action="/sistemaCaptura${initParam.versionIntranet}/intranet" method="post">
    <input type="hidden" name="accion" value="mostrarMenuSistemas" />
</form>
<form name="frmSancionados" action="/sistemaCaptura${initParam.versionIntranet}/sanciones" method="post">
    <input type="hidden" name="accion" value="ingresarSistema"/>
</form>
<form name="frmQueEs" action="/sistemaCaptura${initParam.versionIntranet}/intranet" method="post">
    <input type="hidden" name="accion" value="mostrarPaginaQueEs" />
</form>
<form name="frmPreguntasFrecuentes" action="/sistemaCaptura${initParam.versionIntranet}/intranet" method="post">
    <input type="hidden" name="accion" value="mostrarPaginaPreguntasFrecuentes" />
</form>
<form name="frmTerminos" action="/sistemaCaptura${initParam.versionIntranet}/intranet" method="post">
    <input type="hidden" name="accion" value="mostrarPaginaTerminos" />
</form>
<form name="frmPrivacidad" action="/sistemaCaptura${initParam.versionIntranet}/intranet" method="post">
    <input type="hidden" name="accion" value="mostrarPaginaPrivacidad" />
</form>

<script src="framework/js/vendor/jquery.min.js"></script>
<script src="framework/js/vendor/what-input.min.js"></script>
<script src="framework/js/vendor/foundation.min.js"></script>
<script src="framework/js/app.js"></script>
<script src="framework/js/sweetalert.min.js"></script>
<script src="framework/js/chosen.jquery.min.js"></script>
<script>
                        function NoBack() {
                            history.go(1)
                        }
</script>

</body>

</html>
