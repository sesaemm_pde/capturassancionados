<%--
    Autor: Jessica Diaz jessica.diaz@sesaemm.org.mx, Ivan Cornejo ivan.cornejo@sesaemm.org.mx
    Colaboradores: Cristian Luna cristian.luna@sesaemm.org.mx, Ismael Ortiz ismael.ortiz@sesaemm.org.mx
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@include file="encabezado.jsp" %>

<div class="orbit" role="region" aria-label="Hero PDE" data-orbit="data-orbit">
    <ul class="orbit-container">
        <li class="orbit-slide">
            <img class="orbit-image" src="framework/img/png/banner-pde-faq.png" alt="PDE"/>
        </li>
    </ul>
</div>

<%@include file="migas.jsp" %>

<div class="grid-container display-top">
    <div class="grid-x">
        <div class="large-12 display-top">
            <h1>Preguntas frecuentes</h1>
            <dl>
                <dt>&iquest;La Plataforma Digital Estatal (PDE) va a generar informaci&oacute;n?</dt>
                <dd>No. El objetivo de la Plataforma es generar interoperabilidad entre los datos que ya generan actualmente los entes obligados, a trav&eacute;s del uso de est&aacute;ndares comunes.</dd>
                <hr/>
                <dt>&iquest;La PDE va a operar sistemas como CompraNet o Declaranet?</dt>
                <dd>No. Las Plataforma ser&aacute; una herramienta de interoperabilidad que a trav&eacute;s de las estandarizaci&oacute;n de la informaci&oacute;n ser&aacute; capaz de consultar los datos que contienen sistemas como CompraNet o Declaranet.<br/>La generaci&oacute;n de los datos desde sistemas como CompraNet o Declaranet seguir&aacute; siendo responsabilidad de los entes que tienen la atribuci&oacute;n actualmente.</dd>
                <hr/>
                <dt>&iquest;La PDE se van a quedar con los datos generados por las instituciones?</dt>
                <dd>No. Las Instituciones son las responsables de los datos que generan, y a partir de la publicaci&oacute;n de los lineamientos de cada Sistema, deber&aacute;n estandarizarlos de acuerdo a lo solicitado por la SESNA a trav&eacute;s del Comit&eacute; Coordinador del SNA.</dd>
                <hr/>
                <dt>&iquest;Se van a compartir los datos reservados o personales?</dt>
                <dd>No. Los est&aacute;ndares de datos que ser&aacute;n publicados por la SESNA permiten el control institucional para la consulta de datos reservados o personales.<br/>De acuerdo a lo mandatado por la Ley del SNA, el Comit&eacute; Coordinador del SNA, tendr&aacute; la responsabilidad de aprobar el cat&aacute;logo de perfiles de funcionarios que tendr&aacute;n acceso a los datos reservados.</dd>
                <hr/>
                <dt>&iquest;C&oacute;mo se va a trabajar en la seguridad e integridad de los datos?</dt>
                <dd>Se utilizar&aacute;n herramientas de autentificaci&oacute;n que contemplar&aacute;n los roles y permisos, por ejemplo: SSL, Firebase, OAuth, eFirma; estas herramientas permitir&aacute;n mantener la trazabilidad de las consultas de datos que se hagan dentro de las plataformas, garantizando su m&aacute;xima seguridad.</dd>
                <hr/>
                <dt>&iquest;Qui&eacute;n va a poder acceder a la PDE?</dt>
                <dd>El componente p&uacute;blico de la PDE ser&aacute; para consulta de cualquier ciudadano, y dar&aacute; acceso a los datos que tienen car&aacute;cter p&uacute;blico, de acuerdo a la Ley General de Transparencia y Acceso a la Informaci&oacute;n.<br/>El componente privado tendr&aacute; un acceso restringido que ser&aacute; determinado por el Comit&eacute; Coordinador del SNA, quien tendr&aacute; la responsabilidad de aprobar el cat&aacute;logo de perfiles y funcionarios que tendr&aacute;n acceso a los datos reservados.</dd>
                <hr/>
                <dt>&iquest;Qu&eacute; es interoperabilidad?</dt>
                <dd>Interoperabilidad se refiere a la posibilidad que tiene un sistema de obtener o transferir informaci&oacute;n con otros sistemas. Para lograr la interoperabilidad de los datos, es necesario estandarizarlos y ponerlos en un formato com&uacute;n.</dd>
                <hr/>
                <dt>&iquest;Qu&eacute; es un est&aacute;ndar de datos?</dt>
                <dd>Un est&aacute;ndar se refiere a las reglas y caracter&iacute;sticas con las que debe de contar un dato, como: tipo de dato, longitud, n&uacute;mero de veces que aparece, precisi&oacute;n, etc.</dd>
            </dl>
        </div>
    </div>
</div>

<%@include file="piePagina.jsp" %>
